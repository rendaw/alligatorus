# Roadmap

* Generate AST and intrinsic docs

* Mutable primitive data types, mutate operator
   * Const primitives have a "mut" function which returns a mutable value with same contents
   * Forking mut value produces const
* Loops, conditionals
   * Scope branch barrier; bind method
   * Seq names
   * Loop w/wo name
   * Branch (no name)
   * Exit w/wo name
* Primitive type operators
* JBC
   * Extern class, methods to get constructors, static fields
   * Define static field, instance field, method
   * Access, set fields
   * Visibility modifiers, abstract modifier
   * Define interface
   * Enums
* Actual hello world

* Test module system
* JBC wrap collections, string API
* Mortar collections (list, map)
* Read file, subprocess
* Byte operations, string operations
* Create JAR

* LLVM IR, hello world

* Mortar unions, case
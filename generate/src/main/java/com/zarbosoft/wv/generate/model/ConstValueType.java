package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;
import com.zarbosoft.rendaw.common.Assertion;

import static com.zarbosoft.wv.generate.model.Expressions.appPackage;
import static com.zarbosoft.wv.generate.model.Expressions.pgOp;
import static com.zarbosoft.wv.generate.model.Expressions.poetConstValue;

public class ConstValueType extends AutoType {
  @Override
  public CodeBlock buildSerializeModule(String name) {
    return CodeBlock.builder().build(); // not actually used
  }

  @Override
  public CodeBlock buildSerializeCache(String name) {
    return CodeBlock.builder()
        .add("$L.serializeCacheReference(context, cacheBase, writer);", name)
        .build();
  }

  @Override
  public CodeBlock buildStage(String name, int depth) {
    return CodeBlock.builder().add("$L", name).build();
  }

  @Override
  public CodeBlock buildStageCheck(String name, int depth) {
    return null;
  }

  @Override
  public CodeBlock buildDeserializeModule() {
    return pgOp(CodeBlock.builder().add("throw new $T();\n", Assertion.class).build());
  }

  @Override
  public CodeBlock buildDeserializeCache() {
    return CodeBlock.builder()
        .add("$T.deserializeNode", ClassName.get(appPackage + ".importing", "DeserializeCache"))
        .build();
  }

  @Override
  public CodeBlock buildUnbox(CodeBlock arg) {
    return CodeBlock.builder().add("($T)$L", poetConstValue, arg).build();
  }

  @Override
  public TypeName poet() {
    return poetConstValue;
  }
}

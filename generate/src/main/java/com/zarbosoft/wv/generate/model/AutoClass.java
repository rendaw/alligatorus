package com.zarbosoft.wv.generate.model;

import com.google.common.base.CaseFormat;
import com.google.common.collect.Streams;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LRecordCloseEvent;
import com.zarbosoft.luxem.events.LRecordOpenEvent;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.nodes.Set;

import javax.lang.model.element.Modifier;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.zarbosoft.wv.generate.GenerateAll.poetJoin;
import static com.zarbosoft.wv.generate.GenerateAll.write;
import static com.zarbosoft.wv.generate.model.Expressions.appPackage;
import static com.zarbosoft.wv.generate.model.Expressions.exprPackage;
import static com.zarbosoft.wv.generate.model.Expressions.pgColor;
import static com.zarbosoft.wv.generate.model.Expressions.pgOp;
import static com.zarbosoft.wv.generate.model.Expressions.pgSeq;
import static com.zarbosoft.wv.generate.model.Expressions.pgType;
import static com.zarbosoft.wv.generate.model.Expressions.poetContext;
import static com.zarbosoft.wv.generate.model.Expressions.poetExpr;
import static com.zarbosoft.wv.generate.model.Expressions.poetExprAddr;
import static com.zarbosoft.wv.generate.model.Expressions.poetExprEnum;
import static com.zarbosoft.wv.generate.model.Expressions.poetExprInner;
import static com.zarbosoft.wv.generate.model.Expressions.poetModule;
import static com.zarbosoft.wv.generate.model.Expressions.poetMortarEvaluate;
import static com.zarbosoft.wv.generate.model.Expressions.poetPath;

public class AutoClass extends AutoType {
  public final String name;
  public final String jtag;
  final String ltag;
  public final String lower;
  public final String upper;
  public final List<AutoField> fields = new ArrayList<>();
  final boolean polymorphic;
  public ClassName poetName;

  public AutoClass(Expressions context, String name, String className, boolean polymorphic) {
    context.subtypes.add(this);
    this.poetName = ClassName.get(Expressions.exprPackage, className);
    this.name = name;
    this.polymorphic = polymorphic;
    jtag = name.toUpperCase();
    lower = fixName(CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, name));
    ltag = lower;
    upper = name.toUpperCase();
  }

  private static String fixName(String to) {
    switch (to) {
      case "void":
        return to + "_";
      default:
        return to;
    }
  }

  public AutoClass addField(String name, AutoType t) {
    fields.add(new AutoField(name, t));
    return this;
  }

  public ClassName build(Path path) {
    TypeSpec.Builder mainType =
        TypeSpec.classBuilder(poetName)
            .addModifiers(Modifier.PUBLIC)
            .addSuperinterface(poetExprInner);
    MethodSpec.Builder constructor = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);

    CodeBlock.Builder serializeModuleExprInnerCode = CodeBlock.builder();
    CodeBlock.Builder serializeModuleCacheCode = CodeBlock.builder();
    if (polymorphic) {
      serializeModuleExprInnerCode.add("writer.type($S);\n", lower);
      serializeModuleCacheCode.add("writer.type($S);\n", lower);
    }
    serializeModuleExprInnerCode
        .add("writer.recordBegin();\n")
        .add("writer.key($S).primitive($T.toString(id.line));\n", "id", Integer.class);
    serializeModuleCacheCode
        .add("writer.recordBegin();\n")
        .add("writer.key($S);\n", "id")
        .add("id.serializeSimple(writer);\n");

    for (AutoField field : fields) {
      constructor.addParameter(field.inner.poet(), field.name);
      constructor.addCode("this.$L = $L;\n", field.name, field.name);
      serializeModuleExprInnerCode.add(field.buildSerializeModule());
      serializeModuleCacheCode.add(field.buildSerializeCache());
      mainType.addField(field.buildField());
    }
    write(
        path,
        poetName,
        mainType
            .addMethod(
                MethodSpec.methodBuilder("containsMatchingLower")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .returns(boolean.class)
                    .addCode(
                        CodeBlock.builder()
                            .add(
                                "Lower".equals(name)
                                    ? CodeBlock.builder()
                                        .add("if (depth == context.stageDepth) return true;\n")
                                        .build()
                                    : CodeBlock.builder().build())
                            .add(
                                poetJoin(
                                    "",
                                    fields.stream()
                                        .map(f -> containsMatchingLowerSub(f.name, f.inner, 0))))
                            .add("return false;\n")
                            .build())
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("stage")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .addParameter(poetExpr, "expr")
                    .returns(poetExpr)
                    .addCode(
                        CodeBlock.builder()
                            .add(
                                "Stage".equals(name)
                                    ? CodeBlock.builder()
                                        .add("context = context.pushStage();\n")
                                        .build()
                                    : "Lower".equals(name)
                                        ? CodeBlock.builder()
                                            .add("if (depth == context.stageDepth)\n")
                                            .add(
                                                "return new $T(expr.id, $T.$L, new $T($T.instance.lowerExpr(context, expr, this)));\n",
                                                poetExpr,
                                                poetExprEnum,
                                                jtag,
                                                ClassName.get(exprPackage, "ValueExpr"),
                                                poetMortarEvaluate)
                                            .add("else if (depth > context.stageDepth) {\n")
                                            .add(
                                                "context.errors.add(new $T(expr, new $T(context.stageDepth)));\n",
                                                ClassName.get(appPackage + ".error", "ExprError"),
                                                ClassName.get(
                                                    appPackage + ".error", "LowerUnreachable"))
                                            .add("return null;\n", poetMortarEvaluate)
                                            .add("}\n")
                                            .build()
                                        : CodeBlock.builder().build())
                            .add(
                                "$L",
                                poetJoin(
                                    "",
                                    fields.stream()
                                        .map(
                                            f ->
                                                CodeBlock.builder()
                                                    .add(
                                                        "$T $L_ = $L;\n",
                                                        f.inner.poet(),
                                                        f.name,
                                                        f.buildStage(0))
                                                    .build())))
                            .add(
                                "$L",
                                poetJoin(
                                    "",
                                    fields.stream()
                                        .map(
                                            f -> f.buildStageCheck(String.format("%s_", f.name), 0))
                                        .filter(f -> f != null)
                                        .map(f -> CodeBlock.of("if ($L) return null;\n", f))))
                            .add(
                                "return new $T(expr.id, $T.$L, new $T($L\n));\n",
                                poetExpr,
                                poetExprEnum,
                                jtag,
                                poetName,
                                poetJoin(
                                    "\n,", fields.stream().map(f -> CodeBlock.of("$L_", f.name))))
                            .build())
                    .build())
            .addMethod(constructor.build())
            .addMethod(
                MethodSpec.methodBuilder("serializeModule")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(Writer.class, "writer")
                    .addParameter(poetExprAddr, "id")
                    .addCode(
                        "Value".equals(name)
                            ? CodeBlock.builder()
                                .add("throw new $T();\n", RuntimeException.class)
                                .build()
                            : serializeModuleExprInnerCode.add("writer.recordEnd();\n").build())
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("serializeCache")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .addParameter(poetPath, "cacheBase")
                    .addParameter(Writer.class, "writer")
                    .addParameter(poetExprAddr, "id")
                    .addCode(serializeModuleCacheCode.add("writer.recordEnd();\n").build())
                    .build())
            .build());
    return poetName;
  }

  private static CodeBlock containsMatchingLowerSub(String name, AutoType t, int depth) {
    if (t instanceof ExprType) {
      return CodeBlock.builder()
          .add("if ($L.containsMatchingLower(context)) return true;\n", name)
          .build();
    } else if (t instanceof ListType) {
      return CodeBlock.builder()
          .add("for ($T x$L : $L) {\n", ((ListType) t).inner.poet(), depth, name)
          .add(
              containsMatchingLowerSub(
                  String.format("x%s", depth), ((ListType) t).inner, depth + 1))
          .add("}\n")
          .build();
    } else if (t instanceof AutoClass) {
      return poetJoin(
          "",
          ((AutoClass) t)
              .fields.stream()
                  .map(
                      f ->
                          containsMatchingLowerSub(
                              String.format("%s.%s", name, f.name), f.inner, depth + 1)));
    } else return CodeBlock.builder().build();
  }

  private CodeBlock deserializeWithKey(CodeBlock key) {
    return Expressions.pgOp(
        Expressions.pgSeq(
            Expressions.pgColor(ltag, pgType(ltag)),
            Expressions.pgDelim(LRecordOpenEvent.class),
            CodeBlock.builder()
                .add(
                    "new $T()$L",
                    Set.class,
                    poetJoin(
                        "",
                        Streams.concat(
                            Stream.of(key),
                            fields.stream()
                                .map(
                                    f ->
                                        CodeBlock.builder()
                                            .add(".add($L)", f.buildDeserialize())
                                            .build()))))
                .build(),
            Expressions.pgDelim(LRecordCloseEvent.class)),
        CodeBlock.builder()
            .add("$T kw = new $T($L);\n", Map.class, HashMap.class, fields.size())
            .add("store = store.popFixedMap($L, kw);\n", fields.size() + 1)
            .add(
                "store = store.pushStack(new $T(($T) kw.remove($S), $T.$L, new $T($L)));\n",
                poetExpr,
                poetExprAddr,
                "id",
                poetExprEnum,
                jtag,
                poetName,
                poetJoin(
                    ", ",
                    fields.stream()
                        .map(
                            f1 ->
                                f1.inner.buildUnbox(
                                    CodeBlock.builder().add("kw.get($S)", f1.name).build()))))
            .add("return store;\n")
            .build());
  }

  @Override
  public CodeBlock buildDeserializeModule() {
    return deserializeWithKey(
        CodeBlock.builder()
            .add(
                ".add($L)",
                pgSeq(
                    pgColor(
                        "id",
                        CodeBlock.builder()
                            .add(
                                "new $T(new $T($S))\n",
                                MatchingEventTerminal.class,
                                LKeyEvent.class,
                                "id")
                            .build()),
                    pgOp(
                        CodeBlock.builder()
                            .add("new $T($T.class)\n", ClassEqTerminal.class, LPrimitiveEvent.class)
                            .build(),
                        CodeBlock.builder()
                            .add(
                                "return store.pushStack(new $T((($T)store.getEnv($S)).addr, $T.parseInt((($T)store.top()).value))).stackFixedDoubleElement($S);\n",
                                poetExprAddr,
                                poetModule,
                                "module",
                                Integer.class,
                                LPrimitiveEvent.class,
                                "id")
                            .build())))
            .build());
  }

  @Override
  public CodeBlock buildDeserializeCache() {
    return deserializeWithKey(
        CodeBlock.builder()
            .add(
                ".add($L)",
                pgSeq(
                    pgColor(
                        "id",
                        CodeBlock.builder()
                            .add(
                                "new $T(new $T($S))\n",
                                MatchingEventTerminal.class,
                                LKeyEvent.class,
                                "id")
                            .build()),
                    pgOp(
                        CodeBlock.builder().add("$T.deserializeSimple\n", poetExprAddr).build(),
                        CodeBlock.builder()
                            .add("return store.stackFixedDoubleElement($S);\n", "id")
                            .build())))
            .build());
  }

  @Override
  public CodeBlock buildSerializeModule(String name) {
    return CodeBlock.builder().add("$L.serializeModule(writer);\n", name).build();
  }

  @Override
  public CodeBlock buildSerializeCache(String name) {
    return CodeBlock.builder().add("$L.serializeCache(context, writer);\n", name).build();
  }

  @Override
  public CodeBlock buildStage(String name, int depth) {
    return CodeBlock.builder().add("$L.stage(context, expr)\n", name).build();
  }

  @Override
  public CodeBlock buildStageCheck(String name, int depth) {
    return CodeBlock.builder().add("$L != null", name).build();
  }

  @Override
  public CodeBlock buildUnbox(CodeBlock arg) {
    return CodeBlock.builder().add("($T)$L", poetName, arg).build();
  }

  @Override
  public TypeName poet() {
    return poetName;
  }
}

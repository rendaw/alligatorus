package com.zarbosoft.wv.generate;

import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.wv.generate.model.AutoExprType;
import com.zarbosoft.wv.generate.model.ConstValueType;
import com.zarbosoft.wv.generate.model.Expressions;
import com.zarbosoft.wv.generate.model.IntType;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class GenerateAll extends com.zarbosoft.wv.generate.TaskBase {
  public static void main(final String[] args) {
    final GenerateAll t = new GenerateAll();
    t.setPath(args[0]);
    t.execute();
  }

  public static void write(Path path, ClassName name, TypeSpec spec) {
    System.out.format("Writing class %s\n", name);
    uncheck(
        () -> {
          Path resolved =
              path.resolve(
                  name.packageName().replace('.', '/') + "/" + name.simpleName() + ".java");
          try {
            Files.createDirectories(resolved.getParent());
          } catch (FileAlreadyExistsException ignored) {
          }
          String source = JavaFile.builder(name.packageName(), spec).build().toString();
          try {
            source = new Formatter().formatSourceAndFixImports(source);
          } catch (FormatterException e) {
            System.out.format(
                "Format failed:\n%s",
                source
                    .lines()
                    .map(new Common.Enumerator<>(1))
                    .map(p -> String.format("%04d: %s\n", p.first, p.second))
                    .collect(Collectors.joining("")));
            throw e;
          }
          try (OutputStream o = Files.newOutputStream(resolved)) {
            o.write(source.getBytes(StandardCharsets.UTF_8));
          }
        });
  }

  public static CodeBlock poetJoin(String infix, Stream<CodeBlock> blocks) {
    CodeBlock.Builder out = CodeBlock.builder();
    boolean first = true;
    for (CodeBlock b : iterable(blocks)) {
      if (first) {
        first = false;
      } else {
        out.add(infix);
      }
      out.add(b);
    }
    return out.build();
  }

  @Override
  public void run() {
    Expressions e = new Expressions();
    e.exprs.add(new AutoExprType(e, "Access").addExprField("target").addExprField("key"));
    e.exprs.add(new AutoExprType(e, "Bind").addExprField("key").addExprField("value"));
    e.exprs.add(new AutoExprType(e, "Call").addExprField("callable").addExprField("arg"));
    e.exprs.add(
        new AutoExprType(e, "MethodCall")
            .addExprField("base")
            .addExprField("key")
            .addExprField("arg"));
    e.exprs.add(new AutoExprType(e, "Intrinsic"));
    e.exprs.add(new AutoExprType(e, "Scope"));
    for (String name : new String[] {"String", "Int", "Long", "Byte", "Float", "Double", "Bool"}) {
      e.exprs.add(new AutoExprType(e, name + "Literal").addStringField("value"));
    }
    e.exprs.add(new AutoExprType(e, "VoidLiteral"));
    e.exprs.add(new AutoExprType(e, "SymbolLiteral").addStringField("hint"));
    e.exprs.add(
        new AutoExprType(e, "Lower").addExprField("value").addField("depth", new IntType()));
    e.exprs.add(new AutoExprType(e, "Stage").addExprField("value"));
    e.exprs.add(new AutoExprType(e, "Sequence").addExprListField("children"));
    e.exprs.add(new AutoExprType(e, "RecordPair").addExprField("key").addExprField("value"));
    e.exprs.add(new AutoExprType(e, "Record").addExprListField("fields"));
    e.exprs.add(new AutoExprType(e, "Tuple").addExprListField("fields"));
    e.exprs.add(new AutoExprType(e, "Value").addField("value", new ConstValueType()));
    e.build(path);
  }
}

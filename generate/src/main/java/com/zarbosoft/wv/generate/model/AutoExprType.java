package com.zarbosoft.wv.generate.model;

public class AutoExprType extends AutoClass {
  public AutoExprType(Expressions context, String name) {
    super(context, name, name + "Expr", true);
  }

  @Override
  public AutoExprType addField(String name, AutoType t) {
    return (AutoExprType) super.addField(name, t);
  }

  public AutoExprType addStringField(String name) {
    return addField(name, new StringType());
  }

  public AutoExprType addExprField(String name) {
    return addField(name, new ExprType());
  }

  public AutoExprType addExprListField(String name) {
    return addField(name, new ListType(new ExprType()));
  }
}

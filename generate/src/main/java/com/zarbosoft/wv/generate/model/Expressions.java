package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import com.zarbosoft.luxem.events.LArrayCloseEvent;
import com.zarbosoft.luxem.events.LArrayOpenEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LTypeEvent;
import com.zarbosoft.luxem.read.Parse;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Grammar;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Color;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Reference;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.rendaw.common.DeadCode;

import javax.lang.model.element.Modifier;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.wv.generate.GenerateAll.poetJoin;
import static com.zarbosoft.wv.generate.GenerateAll.write;

public class Expressions {
  public static final String appPackage = "com.zarbosoft.wv";
  public static final ClassName poetModule =
      ClassName.get(appPackage + ".importing", "ModuleSubContext");
  public static final String exprPackage = appPackage + ".expr";
  public static final String valueBasePackage = appPackage + ".valuebase";
  public static final ClassName poetExpr = ClassName.get(exprPackage, "Expr");
  public static final ClassName poetExprInner = ClassName.get(exprPackage, "ExprInner");
  public static final ClassName poetValue = ClassName.get(appPackage, "Value");
  public static final ClassName poetConstValue = ClassName.get(valueBasePackage, "ConstValue");
  public static final ClassName poetSimpleCacheConstValue =
      ClassName.get(valueBasePackage, "SimpleCacheConstValue");
  public static final ClassName poetMortarEvaluate = ClassName.get(appPackage, "Evaluate");
  public static final ClassName poetContext = ClassName.get(appPackage, "Context");
  public static final ClassName poetResult = ClassName.get(appPackage + ".mortar", "Result");
  public static final ClassName poetExprEnum = poetExpr.nestedClass("E");
  public static final ClassName poetExprAddr = ClassName.get(appPackage, "ExprAddr");
  public static final ClassName poetPath = ClassName.get(Path.class);
  public final List<AutoClass> subtypes = new ArrayList<>();

  public Expressions() {}

  public static CodeBlock pgOp(CodeBlock child, CodeBlock action) {
    return CodeBlock.builder()
        .add("new $T<$T>($L) {\n", Operator.class, StackStore.class, child)
        .add("@Override protected $T process($T store) {\n", StackStore.class, StackStore.class)
        .add(action)
        .add("}\n")
        .add("}\n")
        .build();
  }

  public static CodeBlock pgOp(CodeBlock action) {
    return CodeBlock.builder()
        .add("new $T<$T>() {\n", Operator.class, StackStore.class)
        .add("@Override protected $T process($T store) {\n", StackStore.class, StackStore.class)
        .add(action)
        .add("}\n")
        .add("}\n")
        .build();
  }

  public static CodeBlock pgSeq(CodeBlock... children) {
    return CodeBlock.builder()
        .add(
            "new $T()\n$L",
            Sequence.class,
            poetJoin(
                "", Stream.of(children).map(c -> CodeBlock.builder().add(".add($L)\n", c).build())))
        .build();
  }

  public static CodeBlock pgType(String name) {
    return CodeBlock.builder()
        .add("new $T(new $T($S))\n", MatchingEventTerminal.class, LTypeEvent.class, name)
        .build();
  }

  public static CodeBlock pgColor(String text, CodeBlock inner) {
    return CodeBlock.builder().add("new $T($S, $L)\n", Color.class, text, inner).build();
  }

  public static CodeBlock pgDelim(Class delim) {
    return CodeBlock.builder().add("new $T($T.instance)\n", ValEqTerminal.class, delim).build();
  }

  public final List<AutoExprType> exprs = new ArrayList<>();

  public void build(Path path) {
    for (AutoClass subtype : subtypes) {
      subtype.build(path);
    }
    TypeSpec.Builder tagsBuilder =
        TypeSpec.enumBuilder(poetExprEnum)
            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .addMethod(
                MethodSpec.methodBuilder("constructorName")
                    .returns(String.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addModifiers(Modifier.ABSTRACT)
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("errorDesc")
                    .returns(String.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addModifiers(Modifier.ABSTRACT)
                    .build());
    TypeSpec.Builder top =
        TypeSpec.classBuilder(poetExpr)
            .superclass(poetSimpleCacheConstValue)
            .addSuperinterface(ClassName.get(valueBasePackage, "SimpleObject"))
            .addModifiers(Modifier.PUBLIC)
            .addField(
                FieldSpec.builder(
                        Grammar.class, "grammar", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                    .initializer(
                        CodeBlock.builder()
                            .add("new $T()\n", Grammar.class)
                            .add(
                                ".add($S, $L)\n",
                                "root",
                                pgSeq(
                                    CodeBlock.builder()
                                        .add(
                                            "new $T(new $T($T.version))\n",
                                            MatchingEventTerminal.class,
                                            LPrimitiveEvent.class,
                                            ClassName.get(appPackage, "Global"))
                                        .build(),
                                    CodeBlock.builder()
                                        .add("new $T($S)\n", Reference.class, "expr")
                                        .build()))
                            .add(
                                ".add($S, $L)\n",
                                "expr",
                                CodeBlock.builder()
                                    .add(
                                        "new $T()\n$L",
                                        Union.class,
                                        poetJoin(
                                            "",
                                            exprs.stream()
                                                .map(
                                                    expr ->
                                                        CodeBlock.builder()
                                                            .add(
                                                                ".add($L)\n",
                                                                pgOp(
                                                                    expr.buildDeserializeModule(),
                                                                    CodeBlock.builder()
                                                                        .add(
                                                                            "return store.pushStack($T.$L);\n",
                                                                            poetExprEnum,
                                                                            expr.jtag)
                                                                        .build()))
                                                            .build())))
                                    .build())
                            .build())
                    .build())
            .addField(
                FieldSpec.builder(
                        Node.class,
                        "moduleCacheNode",
                        Modifier.PUBLIC,
                        Modifier.STATIC,
                        Modifier.FINAL)
                    .initializer(
                        pgSeq(
                            pgColor("expr", pgType("expr")),
                            pgDelim(LArrayOpenEvent.class),
                            CodeBlock.builder()
                                .add(
                                    "$T.deserializeSimple\n", ClassName.get(appPackage, "ExprAddr"))
                                .build(),
                            CodeBlock.builder()
                                .add(
                                    "new $T()\n$L",
                                    Union.class,
                                    poetJoin(
                                        "",
                                        exprs.stream()
                                            .map(
                                                expr ->
                                                    CodeBlock.builder()
                                                        .add(
                                                            ".add($L)\n",
                                                            pgOp(
                                                                expr.buildDeserializeCache(),
                                                                CodeBlock.builder()
                                                                    .add(
                                                                        "return store.pushStack($T.$L);\n",
                                                                        poetExprEnum,
                                                                        expr.jtag)
                                                                    .build()))
                                                        .build())))
                                .build(),
                            pgDelim(LArrayCloseEvent.class),
                            pgOp(
                                CodeBlock.builder()
                                    .add("$T tag = store.stackTop();\n", poetExprEnum)
                                    .add("store = store.popStack();\n")
                                    .add("$T inner = store.stackTop();\n", poetExprInner)
                                    .add("store = store.popStack();\n")
                                    .add("$T id = store.stackTop();\n", poetExprAddr)
                                    .add("store = store.popStack();\n")
                                    .add(
                                        "return store.pushStack(new $T(id, tag, inner));\n",
                                        Expressions.poetExpr)
                                    .build())))
                    .build())
            .addField(poetExprAddr, "id", Modifier.PUBLIC, Modifier.FINAL)
            .addField(poetExprEnum, "tag", Modifier.PUBLIC, Modifier.FINAL)
            .addField(poetExprInner, "inner", Modifier.PRIVATE, Modifier.FINAL);

    for (AutoExprType subexpr : exprs) {
      ClassName poetExpr = subexpr.build(path);
      tagsBuilder.addEnumConstant(
          subexpr.jtag,
          TypeSpec.anonymousClassBuilder("")
              .addMethod(
                  MethodSpec.methodBuilder("constructorName")
                      .addAnnotation(Override.class)
                      .returns(String.class)
                      .addModifiers(Modifier.PUBLIC)
                      .addCode("return $S;\n", subexpr.lower)
                      .build())
              .addMethod(
                  MethodSpec.methodBuilder("errorDesc")
                      .addAnnotation(Override.class)
                      .returns(String.class)
                      .addModifiers(Modifier.PUBLIC)
                      .addCode("return $S;\n", subexpr.name)
                      .build())
              .build());
      top.addMethod(
              MethodSpec.methodBuilder("is" + subexpr.name)
                  .addModifiers(Modifier.PUBLIC)
                  .returns(boolean.class)
                  .addCode(
                      CodeBlock.builder()
                          .add("return tag == $T.$L;\n", poetExprEnum, subexpr.jtag)
                          .build())
                  .build())
          .addMethod(
              MethodSpec.methodBuilder("get" + subexpr.name)
                  .addModifiers(Modifier.PUBLIC)
                  .returns(poetExpr)
                  .addCode(
                      CodeBlock.builder()
                          .add("assert tag == $T.$L;\n", poetExprEnum, subexpr.jtag)
                          .add("return ($T)inner;\n", poetExpr)
                          .build())
                  .build())
          .addMethod(
              MethodSpec.methodBuilder(subexpr.lower)
                  .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                  .returns(Expressions.poetExpr)
                  .addParameter(poetExprAddr, "id")
                  .addParameters(
                      iterable(
                          subexpr.fields.stream()
                              .map(f -> ParameterSpec.builder(f.inner.poet(), f.name).build())))
                  .addCode(
                      "return new $T(id, $T.$L, new $T($L));\n",
                      Expressions.poetExpr,
                      poetExprEnum,
                      subexpr.jtag,
                      subexpr.poetName,
                      poetJoin(",", subexpr.fields.stream().map(f -> CodeBlock.of("$L", f.name))))
                  .build());
    }
    write(
        path,
        poetExprInner,
        TypeSpec.interfaceBuilder(poetExprInner)
            .addMethod(
                MethodSpec.methodBuilder("containsMatchingLower")
                    .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                    .addParameter(poetContext, "context")
                    .returns(boolean.class)
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("stage")
                    .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                    .addParameter(poetContext, "context")
                    .addParameter(poetExpr, "expr")
                    .returns(poetExpr)
                    .build())
            .build());
    write(
        path,
        poetExpr,
        top.addType(tagsBuilder.build())
            .addMethod(
                MethodSpec.constructorBuilder()
                    .addParameter(poetExprAddr, "id")
                    .addParameter(poetExprEnum, "tag")
                    .addParameter(poetExprInner, "inner")
                    .addCode("this.id = id;\nthis.tag = tag;\nthis.inner = inner;\n")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("stage_")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .returns(poetExpr)
                    .addCode(
                        CodeBlock.builder()
                          .add("if (!containsMatchingLower(context)) return this;\n")
                            .add("return inner.stage(context, this);\n")
                            .build())
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("containsMatchingLower")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .returns(boolean.class)
                    .addCode("return inner.containsMatchingLower(context);\n")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("deserialize")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .addParameter(poetModule, "module")
                    .returns(ParameterizedTypeName.get(ClassName.get(Parse.class), poetExpr))
                    .addCode(
                        "return new $T<$T>().grammar(grammar).env($S, module);\n",
                        Parse.class,
                        poetExpr,
                        "module")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("serializeModuleTop")
                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                    .addParameter(Writer.class, "writer")
                    .addParameter(poetExpr, "expr")
                    .addCode("writer.primitive($T.version);\n", ClassName.get(appPackage, "Global"))
                    .addCode("expr.serializeModule(writer);\n")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("serializeModule")
                    .addComment(
                        "Discards module from expression addr: should only be used for re-serializing original source")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(Writer.class, "writer")
                    .addCode("writer.arrayBegin();\n")
                    .addCode(
                        "switch (tag) {\n$Ldefault: throw new $T();\n}\n",
                        poetJoin(
                            "",
                            exprs.stream()
                                .map(
                                    e ->
                                        CodeBlock.builder()
                                            .add(
                                                "case $L: (($T)inner).serializeModule(writer, id); break;\n",
                                                e.jtag,
                                                e.poetName)
                                            .build())),
                        DeadCode.class)
                    .addCode("writer.arrayEnd();\n")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("serializeCacheInner")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(poetContext, "context")
                    .addParameter(poetPath, "cacheBase")
                    .addParameter(Writer.class, "writer")
                    .addCode("writer.type($S);\n", "expr")
                    .addCode("writer.arrayBegin();\n")
                    .addCode(
                        "switch (tag) {\n$Ldefault: throw new $T();\n}\n",
                        poetJoin(
                            "",
                            exprs.stream()
                                .map(
                                    e ->
                                        CodeBlock.builder()
                                            .add(
                                                "case $L: (($T)inner).serializeCache(context, cacheBase, writer, id); break;\n",
                                                e.jtag,
                                                e.poetName)
                                            .build())),
                        DeadCode.class)
                    .addCode("writer.arrayEnd();\n")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("errorDesc")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(String.class)
                    .addCode("return $T.format($S, tag.errorDesc());\n", String.class, "expr %s")
                    .build())
            .addMethod(
                MethodSpec.methodBuilder("newCopy")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(poetConstValue)
                    .addCode("return new $T(id, tag, inner);\n", poetExpr)
                    .build())
            .build());
  }
}

package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;

public abstract class AutoType {
  public abstract CodeBlock buildDeserializeCache();

  public abstract CodeBlock buildDeserializeModule();

  public abstract CodeBlock buildSerializeModule(String name);

  public abstract CodeBlock buildUnbox(CodeBlock arg);

  public abstract TypeName poet();

  public abstract CodeBlock buildSerializeCache(String name);

  public abstract CodeBlock buildStage(String name, int depth);

  public abstract CodeBlock buildStageCheck(String name, int depth);
}

package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;

public class StringType extends AutoType {

  @Override
  public CodeBlock buildSerializeModule(String name) {
    return CodeBlock.builder().add("writer.primitive($L);", name).build();
  }

  @Override
  public CodeBlock buildSerializeCache(String name) {
    return CodeBlock.builder().add("writer.primitive($L);", name).build();
  }

  @Override
  public CodeBlock buildDeserializeModule() {
    return Expressions.pgOp(
        CodeBlock.builder()
            .add("new $T($T.class)", ClassEqTerminal.class, LPrimitiveEvent.class)
            .build(),
        CodeBlock.builder()
            .add("return store.pushStack((($T)store.stackTop()).value);", LPrimitiveEvent.class)
            .build());
  }

  @Override
  public CodeBlock buildDeserializeCache() {
    return buildDeserializeModule();
  }

  @Override
  public CodeBlock buildStage(String name, int depth) {
    return CodeBlock.builder().add("$L", name).build();
  }

  @Override
  public CodeBlock buildStageCheck(String name, int depth) {
    return null;
  }

  @Override
  public CodeBlock buildUnbox(CodeBlock arg) {
    return CodeBlock.builder().add("($T)$L", String.class, arg).build();
  }

  @Override
  public TypeName poet() {
    return ClassName.get(String.class);
  }
}

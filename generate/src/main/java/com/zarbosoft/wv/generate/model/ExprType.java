package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;
import com.zarbosoft.pidgoon.nodes.Reference;

import static com.zarbosoft.wv.generate.model.Expressions.poetExpr;

public class ExprType extends AutoType {
  @Override
  public CodeBlock buildSerializeModule(String name) {
    return CodeBlock.builder().add("$L.serializeModule(writer);", name).build();
  }

  @Override
  public CodeBlock buildSerializeCache(String name) {
    return CodeBlock.builder().add("$L.serializeCacheReference(context, cacheBase, writer);", name).build();
  }

  @Override
  public CodeBlock buildStage(String name, int depth) {
    return CodeBlock.builder().add("$L.stage_(context)\n", name).build();
  }

  @Override
  public CodeBlock buildStageCheck(String name, int depth) {
    return CodeBlock.builder().add("$L != null", name).build();
  }

  @Override
  public CodeBlock buildDeserializeModule() {
    return CodeBlock.builder().add("new $T($S)", Reference.class, "expr").build();
  }

  @Override
  public CodeBlock buildDeserializeCache() {
    return buildDeserializeModule();
  }

  @Override
  public CodeBlock buildUnbox(CodeBlock arg) {
    return CodeBlock.builder().add("($T)$L", poetExpr, arg).build();
  }

  @Override
  public TypeName poet() {
    return poetExpr;
  }
}

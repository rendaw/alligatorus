package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.zarbosoft.luxem.events.LArrayCloseEvent;
import com.zarbosoft.luxem.events.LArrayOpenEvent;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Reference;
import com.zarbosoft.pidgoon.nodes.Repeat;
import com.zarbosoft.pidgoon.nodes.Sequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListType extends AutoType {
  public final AutoType inner;

  public ListType(AutoType inner) {
    this.inner = inner;
  }

  @Override
  public TypeName poet() {
    return ParameterizedTypeName.get(ClassName.get(List.class), inner.poet());
  }

  @Override
  public CodeBlock buildSerializeModule(String name) {
    return CodeBlock.builder()
        .add("writer.arrayBegin();")
        .add("for ($T e : $L) e.serializeModule(writer);", inner.poet(), name)
        .add("writer.arrayEnd();")
        .build();
  }

  @Override
  public CodeBlock buildSerializeCache(String name) {
    return CodeBlock.builder()
        .add("writer.arrayBegin();")
        .add("for ($T e : $L) e.serializeCache(context, cacheBase);", inner.poet(), name)
        .add("writer.arrayEnd();")
        .build();
  }

  @Override
  public CodeBlock buildStage(String name, int depth) {
    return CodeBlock.builder()
        .add(
            "$L.stream().map(x$L -> $L).collect($T.toList())\n",
            name,
            depth,
            inner.buildStage(String.format("x%s", depth), depth + 1),
            Collectors.class)
        .build();
  }

  @Override
  public CodeBlock buildStageCheck(String name, int depth) {
    String name1 = String.format("x%s", name);
    CodeBlock innerCheck = inner.buildStageCheck(name1, depth + 1);
    if (innerCheck == null) return null;
    return CodeBlock.builder()
        .add("$L.stream().anyMatch($L -> $L)\n", name, name1, innerCheck)
        .build();
  }

  @Override
  public CodeBlock buildDeserializeCache() {
    return buildDeserializeModule();
  }

  @Override
  public CodeBlock buildDeserializeModule() {
    return Expressions.pgOp(
        CodeBlock.builder()
            .add(
                "new $T().add(new $T($T.instance)).add(new $T($L)).add(new $T($T.instance))",
                Sequence.class,
                ValEqTerminal.class,
                LArrayOpenEvent.class,
                Repeat.class,
                Expressions.pgOp(
                    CodeBlock.builder().add("new $T($S)", Reference.class, "expr").build(),
                    CodeBlock.builder().add("return store.stackSingleElement();").build()),
                ValEqTerminal.class,
                LArrayCloseEvent.class)
            .build(),
        CodeBlock.builder()
            .add(
                "$T out = new $T(($T)store.stackTop());",
                List.class,
                ArrayList.class,
                Integer.class)
            .add("store = store.popVarSingleList(out);", StackStore.class)
            .add("return store.pushStack(out);")
            .build());
  }

  @Override
  public CodeBlock buildUnbox(CodeBlock arg) {
    return CodeBlock.builder().add("($T)$L", poet(), arg).build();
  }
}

package com.zarbosoft.wv.generate.model;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;

import javax.lang.model.element.Modifier;

import static com.zarbosoft.wv.generate.model.Expressions.pgColor;
import static com.zarbosoft.wv.generate.model.Expressions.pgSeq;

public class AutoField {
  public final String name;
  public final AutoType inner;

  protected AutoField(String name, AutoType inner) {
    this.name = name;
    this.inner = inner;
  }

  public CodeBlock buildDeserialize() {
    return pgSeq(
        pgColor(
            name,
            CodeBlock.builder()
                .add("new $T(new $T($S))\n", MatchingEventTerminal.class, LKeyEvent.class, name)
                .build()),
        inner.buildDeserializeModule());
  }

  public CodeBlock buildSerializeModule() {
    return CodeBlock.builder().add("writer.key($S);\n", name).add(inner.buildSerializeModule(name)).build();
  }
  public CodeBlock buildSerializeCache() {
    return CodeBlock.builder().add("writer.key($S);\n", name).add(inner.buildSerializeCache(name)).build();
  }

  public FieldSpec buildField() {
    return FieldSpec.builder(inner.poet(), name, Modifier.PUBLIC, Modifier.FINAL).build();
  }

  public CodeBlock buildStage(int depth) {
    return inner.buildStage(name, depth);
  }

  public CodeBlock buildStageCheck(String name, int depth) {
    return inner.buildStageCheck(name, depth);
  }
}

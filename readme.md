**Current status:** Early development.

TODO awesome logo here

Alliga-torus is a programming language defined by three primary goals:

* **Universality:** Write programs for any environment: desktop, web, VM, bare metal, embedded, real time, etc.

   Environment characteristics are never hidden, but powerful low level abstractions are provided to make programming for any environment (or multiple environments) ergonomic. DSLs are an anti-pattern.

* **Portability:** Code reuse should be trivial and natural, requiring no specific efforts: no packaging steps, no publishing steps.
* **Transformability:** From the start, the language should be designed so that programs can be easily and safetly queried and transformed by other software.

   Transformability should also make the language more accessable.

The language will also support a number of other goals:

* **Small core:** Ideally someone should be able to write a complete implementation in a couple days.
* **No boilerplate:** A single file can be a complete, distributable program.
* **Default allow:** No features or use cases will forbidden "because they're dangerous" or "because it's bad practice".

   Users are responsible for their own code -- of course, Alliga-torus will make it easy for users to protect themselves as necessary.

* **Great errors:** Compiler error reporting and handling is a development focus from the start.

   Errors should indicate what was expected, what was encountered, and include as much relevant related information as possible.

* **Orthogonality:** Language constructs should be maximally orthogonal.
* **Reproducibility:** Programs should be deterministic unless users take specific steps to make them not so.

Alliga-torus also has a number of other cool features:

* **No text syntax:** Alliga-torus's interchange format is the AST.  It's up to the editor how to represent the source, which in practice means you can tweak how you represent the source code and add sugars as you see fit.
* **Expression ids:** Rather than line numbers, each expression in a source file has a file-unique id which can be qualified into a globally unique id.  This has a number of add on benefits: pinpoint accuracy errors, breakpoints, etc., robust hyperlinks to specific expressions, the additional information makes high quality diff algorithms trivial to implement.

TODO add a hello world example here (how?)

# Getting started

#### If you want to write code

Get [Alliga-torus-IDE](https://TODO).

#### If you want to compile code

1. Clone this repository
2. In the repository root, run `mvn package`
3. Maybe make an alias or wrapper script for `java alliga-torus.jar $ROOT` named `at`.
4. Compile the source code with `at myprogram.at`

# Learning Alliga-torus

* The [guide](https://TODO), using the official [default syntax](https://TODO) with [Alliga-torus-IDE](https://TODO)
* An overview of [concepts](concepts.md)
* The AST [reference](https://TODO)
* The intrinsic [reference](https://TODO)
* A [directory](https://TODO) of published modules
* A rough road map, internal notes [here](notes.md)

# AQ

* Are there other projects with similar goals or implementations?

seanbaxter circle
nim
zig
manifold (java)
javapoet + java

* What is the logo?

The logo is the alliga-torus.

* What is the alliga-torus?

The alliga-torus is an ancient symbol representing recursive abstraction.

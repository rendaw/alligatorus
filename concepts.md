# Language overview

Alliga-torus is a multi-stage language.  Unlike C and Java, the compilation pipeline has no fixed structure and can be freely programmed.  Compilation happens roughly in two steps:

1. Mortar (non-target-specific interpreter) generates target-specific code
2. The target (Java Bytecode, LLVM IR, etc) takes the target-specific code and generates machine code

Mortar and the target interpretation environments use the same AST but interpret it uniquely.  Data can be shared from mortar to the target environment.

Code generation happens by way of `stage` and `lower` operations.  The `stage` operation turns a subtree of the AST into a value which can be stored, passed to functions, etc.  The `lower` operation reverses the `stage` operation, evaluating a staged expression or suspending the staging and substituting the subtree evaluation result into the staged code.  You can think of this like templating, where `stage` is used to define the template and `lower` does template interpolation.

Compilation takes no interpretation-affecting arguments so that a source file itself contains 100% of the required information to compile it (does this violate default allow?) -- the compilation for any given source file is deterministic.

# Module system

Importing, sharing code, etc.

Every source file is a module.  There are two types of modules: local and remote.

## The root module

The root module is where compilation starts.  

Otherwise the root module essentially behaves like a local module.

## Local modules

Local modules are any module imported according to a filesystem path.  Local modules are assumed to be managed by the software maintainer/builder somehow, perhaps by version control or sheer force of gut.

The compilation root is the where compilation starts.  It imports and is never imported.  While it may evaluate to a value, the value is discarded.  Additionally, calls originating while evaluating the root module directly (and not an import) are unrestricted in what they can do - they can read and write files, open sockets, run programs, etc.

All local modules are cached except the root.  Cached local modules are stored nearby the compilation root.

## Remote modules

Remote imports are identified by a uri and a hash.  The hash is recursively constructed from the uri contents as well as transitive imports and other files read by the module.  Currently only http remote modules are supported.

Imports are relative to the importing module, so a module imported _locally_ from a remote module will also be a remote module.

The target of a _remote_ import will be a remote root.  The result of evaluating a remote root is cached, however the modules imported _locally_ from the root are only cached if they form part of the root result, as part of the root result.  Cached remote modules are stored in a system wide directory.

Any module can add remote module overrides to deal with conflicting dependency specifications or to unify multiply depended-on modules with similar versions, or perhaps just for fun.  The overrides only apply to the import subtree of code of the module that defines the override starting from where the override is defined.  Overrides added earlier have precedence over overrides added later.


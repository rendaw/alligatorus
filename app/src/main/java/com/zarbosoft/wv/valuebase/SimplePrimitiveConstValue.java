package com.zarbosoft.wv.valuebase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class SimplePrimitiveConstValue extends ValueBase implements SimpleConstValue {

  @Override
  public final void serializeCache(Context context, Path cacheBase) {
    uncheck(
        () -> {
          try (OutputStream s = Files.newOutputStream(cacheBase)) {
            serializeCacheReference(context, cacheBase, new Writer(s));
          }
        });
  }
}

package com.zarbosoft.wv.valuebase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.importing.CacheAddr;
import com.zarbosoft.wv.value.VoidValue;

import java.nio.file.Path;

public abstract class IntrinsicConstValue extends ValueBase implements SimpleConstValue {
  public CacheAddr cacheAddr;
  public TargetState pre;
  public TargetState post;

  @Override
  public void serializeCacheReference(Context context, Path basePath, Writer writer) {
    cacheAddr.serializeSimple(writer);
  }

  @Override
  public void serializeCache(Context context, Path cacheBase) {}
}

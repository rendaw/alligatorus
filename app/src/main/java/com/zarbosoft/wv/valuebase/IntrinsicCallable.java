package com.zarbosoft.wv.valuebase;

public abstract class IntrinsicCallable extends IntrinsicConstValue
    implements Callable, SimpleConstValue, SimpleObject {}

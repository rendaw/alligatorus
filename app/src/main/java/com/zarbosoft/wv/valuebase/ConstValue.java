package com.zarbosoft.wv.valuebase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Value;

import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

/**
 * Immutable, serializable -- serialization contains references to module paths.
 *
 * This indicates possibility of being const, not guarantee.
 * */
public interface ConstValue extends Value {
  /**
   * Serialize as a new standalone cache object
   *
   * @param context
   * @param cacheBase
   */
  public abstract void serializeCache(Context context, Path cacheBase);

  /**
   * Serialize a reference from within another cache object
   *
   * @param context
   * @param writer
   */
  public abstract void serializeCacheReference(Context context, Path cacheBase, Writer writer);

  @Override
  public default boolean isConst() {
    return true;
  }

}

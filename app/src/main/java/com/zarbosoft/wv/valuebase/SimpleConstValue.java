package com.zarbosoft.wv.valuebase;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.value.VoidValue;
import org.pcollections.TreePVector;

import java.util.List;

import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;

public interface SimpleConstValue extends ConstValue {
  @Override
  public default Value realize(Context context) {
    return this;
  }

  @Override
  public default Value access(Context context, Key key) {
    context.suberrors.add(new MissingField(TreePVector.empty(), key));
    return null;
  }

  @Override
  public default Value drop(Context context) {
    return new VoidValue().addPre(context, pre()).addPost(context, post());
  }

  @Override
  public default Pair<TargetState, Value> bind(Context context) {
    return new Pair<>(gatherTargetState(context, this), this);
  }

  @Override
  public default Value realizeAccess(Context context, List<Value> keys) {
    if (keys.isEmpty()) return this;
    throw new AssertionError();
  }

  @Override
  public default Value realizeAccessFork(Context context, List<Value> keys) {
    if (keys.isEmpty()) return newCopy();
    throw new AssertionError();
  }

  Value newCopy();
}

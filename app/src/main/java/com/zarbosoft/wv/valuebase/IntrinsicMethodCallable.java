package com.zarbosoft.wv.valuebase;

public abstract class IntrinsicMethodCallable extends IntrinsicConstValue
    implements MethodCallable, SimpleConstValue, SimpleObject {}

package com.zarbosoft.wv.valuebase;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingMethod;
import com.zarbosoft.wv.value.StringValue;

import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public interface SimpleObject extends Value {
  @Override
  default Value accessMethod(Context context, Key key) {
    MethodCallable found;
    if (!(key instanceof StringValue)
        || (found = accessMethod(((StringValue) key).value)) == null) {
      context.suberrors.add(new MissingMethod(key));
      return err;
    }
    return found;
  }

  default MethodCallable accessMethod(String key) {
    return null;
  }
}

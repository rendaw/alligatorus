package com.zarbosoft.wv.valuebase;

import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;

import java.util.List;

public class ErrorValue implements Value {
  public static final ErrorValue err = new ErrorValue();

  private ErrorValue() {}

  @Override
  public boolean isConst() {
    return true;
  }

  @Override
  public String errorDesc() {
    throw new Assertion();
  }

  @Override
  public Value access(Context context, Key key) {
    throw new Assertion();
  }

  @Override
  public Value drop(Context context) {
    throw new Assertion();
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    return new Pair<>(null, this);
  }

  @Override
  public Value accessMethod(Context context, Key key) {
    throw new Assertion();
  }

  @Override
  public TargetState pre() {
    return null;
  }

  @Override
  public Value addPre(Context context, TargetState... pre) {
    return this;
  }

  @Override
  public TargetState post() {
    return null;
  }

  @Override
  public Value addPost(Context context, TargetState... post) {
    return this;
  }

  @Override
  public Value realize(Context context) {
    return this;
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    throw new Assertion();
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    throw new Assertion();
  }

  @Override
  public TargetState takePre() {
    return null;
  }

  @Override
  public TargetState takePost() {
    return null;
  }

  @Override
  public String toSurveyJSON() {
    return "{\"type\":\"error\"}";
  }
}

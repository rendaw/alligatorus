package com.zarbosoft.wv.valuebase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.importing.CacheAddr;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.importing.RemoteModuleAddr;
import com.zarbosoft.wv.importing.RemoteModuleSubContext;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class SimpleCacheConstValue extends ValueBase implements SimpleConstValue {
  public CacheAddr cacheAddr;

  @Override
  public void serializeCacheReference(com.zarbosoft.wv.Context context, Path cacheBase, Writer writer) {
    serializeCache(context, cacheBase);
    cacheAddr.serializeSimple(writer);
  }

  @Override
  public void serializeCache(com.zarbosoft.wv.Context context, Path cacheBase) {
    if (cacheAddr != null) return;
    RemoteModuleSubContext inModule = (RemoteModuleSubContext) context.module;
    int part = inModule.partIds++;
    this.cacheAddr = inModule.createCacheAddr(context.importContextHash, part);
    uncheck(
        () -> {
          try (OutputStream s = cacheAddr.createOutputStream(cacheBase)) {
            Writer writer = new Writer(s);
            serializeCacheInner(context, cacheBase, writer);
          }
        });
  }

  protected abstract void serializeCacheInner(Context context, Path cacheBase, Writer writer);
}

package com.zarbosoft.wv.valuebase;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import org.pcollections.PVector;

public interface MethodCallable extends ConstValue {
  String errorDesc = "method callable";
  TypeCheck check =
      new TypeCheck() {
        @Override
        protected boolean checkInternal(Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof MethodCallable)) {
            context.suberrors.add(new TypeMismatch(path, Callable.errorDesc, arg.errorDesc()));
            return false;
          }
          return true;
        }
      };

  /**
   * Callee must drop this_, arg
   *
   * @param context
   * @param this_
   * @param arg
   * @return
   */
  public abstract Value call(Context context, Value this_, Value arg);

  @Override
  default String errorDesc() {
    return errorDesc;
  };
}

package com.zarbosoft.wv.valuebase;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;

public abstract class ValueBase implements Value {
  private TargetState pre;
  private TargetState post;

  @Override
  public TargetState pre() {
    return pre;
  }

  @Override
  public TargetState post() {
    return post;
  }

  @Override
  public TargetState takePre() {
    TargetState out = pre;
    pre = null;
    return out;
  }

  @Override
  public TargetState takePost() {
    TargetState out = post;
    post = null;
    return out;
  }

  @Override
  public Value addPre(Context context, TargetState... pre) {
    TargetState start = null;
    for (TargetState e : pre) {
      if (e == null) continue;
      if (start == null) {
        start = e;
        continue;
      }
      start.combine(context, e);
    }
    if (start == null) start = this.pre;
    else start.combine(context, this.pre);
    this.pre = start;
    return this;
  }

  @Override
  public Value addPost(Context context, TargetState... post) {
    TargetState start = this.post;
    for (TargetState e : post) {
      if (e == null) continue;
      if (start == null) {
        start = e;
      }
      start.combine(context, e);
    }
    if (this.post == null) this.post = start;
    return this;
  }
}

package com.zarbosoft.wv;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.rendaw.common.DeadCode;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.error.DuplicateField;
import com.zarbosoft.wv.error.ExprError;
import com.zarbosoft.wv.error.InvalidLiteral;
import com.zarbosoft.wv.error.LowerUnreachable;
import com.zarbosoft.wv.error.NotConst;
import com.zarbosoft.wv.error.RecordPairMustBeInRecord;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.expr.AccessExpr;
import com.zarbosoft.wv.expr.BindExpr;
import com.zarbosoft.wv.expr.CallExpr;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.expr.LowerExpr;
import com.zarbosoft.wv.expr.MethodCallExpr;
import com.zarbosoft.wv.expr.RecordExpr;
import com.zarbosoft.wv.expr.RecordPairExpr;
import com.zarbosoft.wv.expr.SequenceExpr;
import com.zarbosoft.wv.expr.StageExpr;
import com.zarbosoft.wv.expr.TupleExpr;
import com.zarbosoft.wv.help.KeyMap;
import com.zarbosoft.wv.value.AccessValue;
import com.zarbosoft.wv.value.BoolValue;
import com.zarbosoft.wv.value.ByteValue;
import com.zarbosoft.wv.value.DoubleValue;
import com.zarbosoft.wv.value.FloatValue;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.LongValue;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.ScopeValue;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.value.SymbolValue;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.Callable;
import com.zarbosoft.wv.valuebase.ConstValue;
import com.zarbosoft.wv.valuebase.MethodCallable;
import org.pcollections.TreePVector;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.wv.Intrinsic.intrinsic;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class Evaluate {
  public static final Evaluate instance = new Evaluate();

  public Value evaluate(Context context, Expr expr) {
    context = context.pushExpr();
    Value out = evaluateInner(context, expr);
    if (!context.suberrors.isEmpty()) context.errors.add(new ExprError(expr, context.suberrors));
    if (out != err) {
      context.supercontext.doInDb(
          connection -> {
            try (PreparedStatement s =
                connection.prepareStatement("insert into survey(addr, data) values (?, ?)")) {
              s.setString(1, expr.id.toSurveyAddr());
              s.setString(2, out.toSurveyJSON());
              s.execute();
            }
          });
    }
    return out;
  }

  private Value evaluateInner(Context context, Expr expr) {
    switch (expr.tag) {
      case ACCESS:
        return evaluateAccessExpr(context, expr.getAccess());
      case BIND:
        return evaluateBindExpr(context, expr.getBind());
      case CALL:
        return evaluateCallExpr(context, expr.getCall());
      case METHODCALL:
        return evaluateMethodCallExpr(context, expr.getMethodCall());
      case INTRINSIC:
        return intrinsic.realizeAccessFork(context, ImmutableList.of());
      case SCOPE:
        return new ScopeValue(context.scope);
      case SYMBOLLITERAL:
        return new SymbolValue(
            context.module.addr, context.module.symbolIds++, expr.getSymbolLiteral().hint);
      case STRINGLITERAL:
        return new StringValue(expr.getStringLiteral().value);
      case INTLITERAL:
        {
          int value;
          try {
            value = Integer.parseInt(expr.getIntLiteral().value);
          } catch (NumberFormatException e) {
            context.suberrors.add(
                new InvalidLiteral(expr.getIntLiteral().value, IntValue.errorDesc));
            return err;
          }
          return new IntValue(value);
        }
      case LONGLITERAL:
        {
          long value;
          try {
            value = Long.parseLong(expr.getLongLiteral().value);
          } catch (NumberFormatException e) {
            context.suberrors.add(
                new InvalidLiteral(expr.getLongLiteral().value, LongValue.errorDesc));
            return err;
          }
          return new LongValue(value);
        }
      case BYTELITERAL:
        {
          byte value;
          try {
            value = Byte.parseByte(expr.getByteLiteral().value);
          } catch (NumberFormatException e) {
            context.suberrors.add(
                new InvalidLiteral(expr.getByteLiteral().value, ByteValue.errorDesc));
            return err;
          }
          return new ByteValue(value);
        }
      case FLOATLITERAL:
        {
          float value;
          try {
            value = Float.parseFloat(expr.getFloatLiteral().value);
          } catch (NumberFormatException e) {
            context.suberrors.add(
                new InvalidLiteral(expr.getFloatLiteral().value, FloatValue.errorDesc));
            return err;
          }
          return new FloatValue(value);
        }
      case DOUBLELITERAL:
        {
          double value;
          try {
            value = Double.parseDouble(expr.getDoubleLiteral().value);
          } catch (NumberFormatException e) {
            context.suberrors.add(
                new InvalidLiteral(expr.getDoubleLiteral().value, DoubleValue.errorDesc));
            return err;
          }
          return new DoubleValue(value);
        }
      case BOOLLITERAL:
        {
          boolean value;
          String literal = expr.getBoolLiteral().value;
          if ("true".equals(literal)) value = true;
          else if ("false".equals(literal)) value = false;
          else {
            context.suberrors.add(new InvalidLiteral(literal, BoolValue.errorDesc));
            return err;
          }
          return new BoolValue(value);
        }
      case VOIDLITERAL:
        return new VoidValue();
      case LOWER:
        return evaluateLowerExpr(context, expr, expr.getLower());
      case STAGE:
        return evaluateStage(context, expr, expr.getStage());
      case SEQUENCE:
        return evaluateSequenceExpr(context, expr.getSequence());
      case RECORDPAIR:
        {
          context.suberrors.add(RecordPairMustBeInRecord.instance);
          return err;
        }
      case RECORD:
        return evaluateRecordExpr(context, expr.getRecord());
      case TUPLE:
        return evaluateTupleExpr(context, expr, expr.getTuple());
      case VALUE:
        return expr.getValue().value;
      default:
        throw new DeadCode();
    }
  }

  protected Value evaluateTupleExpr(Context context, Expr expr, TupleExpr inner) {
    List<Value> out = new ArrayList<>(inner.fields.size());
    for (int i = 0; i < inner.fields.size(); ++i) {
      out.add(evaluate(context, inner.fields.get(i)).realize(context));
    }
    return new Tuple(out);
  }

  protected Value evaluateLowerExpr(Context context, Expr expr, LowerExpr inner) {
    LowerExpr lower = expr.getLower();
    if (lower.depth != 0) {
      context.suberrors.add(new LowerUnreachable(context.stageDepth));
      return err;
    }
    Expr res = lowerExpr(context, expr, lower);
    if (res == null) return err;
    return evaluate(context, res);
  }

  public Expr lowerExpr(Context context, Expr expr, LowerExpr inner) {
    Value res = evaluate(context, inner.value);
    if (res == err) return null;
    if (res instanceof Expr) {
      return (Expr) res;
    } else if (res.isConst()) {
      return Expr.value(expr.id, (ConstValue) res);
    } else {
      context.suberrors.add(new NotConst(TreePVector.empty(), res.errorDesc()));
      return null;
    }
  }

  protected Value evaluateStage(Context context, Expr expr, StageExpr inner) {
    context = context.pushStage();
    Expr res = inner.value.stage_(context);
    if (res == null) return err;
    return res;
  }

  private Value evaluateRecordExpr(Context context, RecordExpr inner) {
    HashMap<Key, Integer> indexes = new HashMap<>();
    KeyMap<Value> values = new KeyMap<>(new LinkedHashMap<>());
    boolean errored = false;
    for (int i = 0; i < inner.fields.size(); ++i) {
      Expr child0 = inner.fields.get(i);
      IntValue iKey = new IntValue(i);
      if (!child0.isRecordPair()) {
        context.suberrors.add(
            new TypeMismatch(TreePVector.singleton(iKey), "record pair", child0.errorDesc()));
        errored = true;
        continue;
      }
      RecordPairExpr child = child0.getRecordPair();
      Value key = evaluate(context, child.key).realize(context);
      Value value = evaluate(context, child.value).realize(context);
      if (!Key.check.check(context, TreePVector.<Key>singleton(iKey).plus(Intern.key), key))
        return err;
      value.addPre(context, key.pre(), key.post());
      Key key1 = (Key) key;
      Integer existing = indexes.get(key1);
      if (existing != null) {
        context.suberrors.add(new DuplicateField(TreePVector.singleton(iKey), key1));
        continue;
      }
      indexes.put(key1, i);
      values.put(key1, value);
    }
    if (errored) return err;
    return new Record(values);
  }

  private Value evaluateCallExpr(Context context, CallExpr inner) {
    /*
    Callable is responsible for consuming argument targetstate and including in output
     */
    Value callable0 = evaluate(context, inner.callable).realize(context);
    Value arg = evaluate(context, inner.arg).realize(context);
    if (!Callable.check.check(context, TreePVector.singleton(Intern.callable), callable0)
        || arg == err) return err;
    Callable callable = (Callable) callable0;
    // Omit drop soft store, drop callable because callables are always const
    return callable.call(context, arg).addPre(context, callable0.pre(), callable0.post());
  }

  private Value evaluateMethodCallExpr(Context context, MethodCallExpr inner) {
    Value base = evaluate(context, inner.base).realize(context);
    Value key = evaluate(context, inner.key).realize(context);
    Value callable0;
    if (base == err || !Key.check.check(context, TreePVector.singleton(Intern.key), key)) {
      callable0 = err;
    } else {
      callable0 = base.accessMethod(context, (Key) key);
    }
    Value arg = evaluate(context, inner.arg).realize(context);
    if (callable0 == err
        || !MethodCallable.check.check(context, TreePVector.singleton(Intern.key), callable0))
      return err;
    MethodCallable callable = (MethodCallable) callable0;
    Value out = callable.call(context, base, arg);
    out.addPre(context, key.pre(), key.post());
    return out;
  }

  private Value evaluateAccessExpr(Context context, AccessExpr inner) {
    Value target = evaluate(context, inner.target);
    Value key = evaluate(context, inner.key);
    if (target == err || !Key.check.check(context, TreePVector.singleton(Intern.key), key))
      return err;
    Value standin = target.access(context, (Key) key);
    if (standin == err) return err;
    return new AccessValue(target, key, standin);
  }

  private Value evaluateSequenceExpr(Context context, SequenceExpr inner) {
    if (inner.children.isEmpty()) return new VoidValue();
    Value last = null;
    {
      TargetState[] dropPre = new TargetState[(inner.children.size() - 1) * 2];
      context = context.pushScope();
      for (int i = 0; i < inner.children.size(); ++i) {
        Expr child = inner.children.get(i);
        if (last != null) {
          Value dropped = drop(context, last);
          dropPre[(i - 1) * 2 + 0] = dropped.pre();
          dropPre[(i - 1) * 2 + 1] = dropped.post();
        }
        last = evaluate(context, child);
      }
      last.addPre(context, dropPre);
    }
    {
      int i = 0;
      TargetState[] dropPost = new TargetState[context.scope.keyed.size() * 2];
      for (Pair<Key, Binding> e : iterable(context.scope.keyed.stream())) {
        Value dropped = drop(context, e.second.value);
        dropPost[i * 2 + 0] = dropped.pre();
        dropPost[i * 2 + 1] = dropped.post();
      }
      last.addPost(context, dropPost);
    }
    return last;
  }

  public Value drop(Context context, Value result) {
    if (result == err) return err;
    result = result.realize(context);
    if (result == err) return err;
    result = result.drop(context);
    if (!(result instanceof VoidValue)) result = drop(context, result);
    return result;
  }

  /**
   * @param context
   * @param child
   * @param inner
   * @return true if scope left in a recognizable state, false if scope is now in shamblambles
   *     (second ignored)
   */
  private Value evaluateBindExpr(Context context, BindExpr inner) {
    Value key0 = evaluate(context, inner.key);
    if (!Key.check.check(context, Intern.key, key0)) return err;
    List<Value> targetState = new ArrayList<>(2);
    targetState.add(key0);
    Key key = (Key) key0;
    Binding old = context.scope.unbind(key);
    if (old != null && old.value != err) {
      targetState.add(drop(context, old.value));
    }
    Value value = evaluate(context, inner.value).realize(context);
    if (value == err) {
      context.scope.bind(key, new Binding(err));
      return err;
    }
    Pair<TargetState, Value> bindValue = value.bind(context);
    if (bindValue == null) {
      context.scope.bind(key, new Binding(err));
      return err;
    }

    context.scope.bind(key, new Binding(bindValue.second));

    return bindValue.second.addPre(context, bindValue.first).addPreFromValues(context, targetState);
  }
}

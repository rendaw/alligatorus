package com.zarbosoft.wv.help;

import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Key;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class KeyMap<T> {
  public final Map<Comparable, Pair<Key, T>> inner;

  public KeyMap(KeyMap<T> other) {
    this.inner = new HashMap<>(other.inner);
  }

  public KeyMap() {
    inner = new HashMap<>();
  }

  public KeyMap(int pre) {
    inner = new HashMap<>(pre);
  }

  public KeyMap(Map inner) {
    if (!inner.isEmpty()) throw new Assertion();
    this.inner = inner;
  }

  public T get(Key key) {
    Pair<Key, T> out = inner.get(key.pieces());
    if (out == null) return null;
    return out.second;
  }

  public void put(Key key, T value) {
    inner.put(key.pieces(), new Pair<>(key, value));
  }

  public T remove(Key key) {
    Pair<Key, T> out = inner.remove(key.pieces());
    if (out == null) return null;
    return out.second;
  }

  public boolean contains(Key key) {
    return inner.containsKey(key.pieces());
  }

  public Stream<Pair<Key, T>> stream() {
    return inner.values().stream();
  }

  public int size() {
    return inner.size();
  }
}

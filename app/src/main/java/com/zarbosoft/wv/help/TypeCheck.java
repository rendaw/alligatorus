package com.zarbosoft.wv.help;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.expr.Expr;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public abstract class TypeCheck {
  public static TypeCheck checkExpr =
      new TypeCheck() {
        @Override
        public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof Expr)) {
            context.suberrors.add(new TypeMismatch(path, "expr", arg.errorDesc()));
            return false;
          }
          return true;
        }
      };

  public boolean check(Context context, Value arg) {
    return check(context, TreePVector.empty(), arg);
  }

  public boolean check(Context context, Key path, Value arg) {
    return check(context, TreePVector.singleton(path), arg);
  }

  public boolean check(Context context, PVector<Key> path, Value arg) {
    if (arg == err) return false;
    return checkInternal(context, path, arg);
  }

  protected abstract boolean checkInternal(Context context, PVector<Key> path, Value arg);
}

package com.zarbosoft.wv.help;

import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LRecordCloseEvent;
import com.zarbosoft.luxem.events.LRecordOpenEvent;
import com.zarbosoft.luxem.events.LTypeEvent;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Color;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Sequence;

import java.util.HashMap;
import java.util.Map;

public class Serialize {
  public static final ValEqTerminal recOpen = new ValEqTerminal(LRecordOpenEvent.instance);
  public static final ValEqTerminal recClose = new ValEqTerminal(LRecordCloseEvent.instance);

  public static Node type(String name, Node inner) {
    return new Sequence()
        .add(new Color(name, new MatchingEventTerminal(new LTypeEvent(name))))
        .add(inner);
  }

  public static class DeserializeRecordBuilder {
    private final Map<String, Node> data = new HashMap<>();

    public DeserializeRecordBuilder addString(String key) {
      data.put(
          key,
          new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
            @Override
            protected StackStore process(StackStore store) {
              return store.pushStack(((LPrimitiveEvent) store.top()).value);
            }
          });
      return this;
    }

    public DeserializeRecordBuilder addInt(String key) {
      data.put(
          key,
          new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
            @Override
            protected StackStore process(StackStore store) {
              return store.pushStack(Integer.parseInt(((LPrimitiveEvent) store.top()).value));
            }
          });
      return this;
    }

    public DeserializeRecordBuilder add(String key, Node node) {
      data.put(key, node);
      return this;
    }

    public Node build() {
      Sequence out = new Sequence();
      out.add(recOpen);
      for (Map.Entry<String, Node> e : data.entrySet()) {
        out.add(new Color(e.getKey(), new MatchingEventTerminal(new LKeyEvent(e.getKey()))));
        out.add(
            new Operator<StackStore>(e.getValue()) {
              @Override
              protected StackStore process(StackStore store) {
                return store.stackFixedDoubleElement(e.getKey());
              }
            });
      }
      out.add(recClose);
      return new Operator<StackStore>(out) {
        @Override
        protected StackStore process(StackStore store) {
          Map<String, Object> map = new HashMap<>();
          store = store.popFixedMap(data.size(), map);
          return store.pushStack(map);
        }
      };
    }
  }
}

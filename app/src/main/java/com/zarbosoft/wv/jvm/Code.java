package com.zarbosoft.wv.jvm;

import org.objectweb.asm.MethodVisitor;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.util.List;

public abstract class Code {
  private static class EmptyCode extends Code {
    @Override
    protected void render(Scope scope, MethodVisitor out) {}
  }

  public static final EmptyCode empty = new EmptyCode();

  protected static class Scope {
    final Scope parent;
    PVector<Object> indexes;

    public Scope(List<Object> indexes) {
      parent = null;
      this.indexes = TreePVector.from(indexes);
    }

    public Scope(Scope parent) {
      this.parent = parent;
      if (parent == null) indexes = TreePVector.empty();
      else indexes = parent.indexes;
    }
  }

  public void render(MethodVisitor out, List<Object> initialIndexes) {
    render(new Scope(initialIndexes), out);
  }

  protected abstract void render(Scope scope, MethodVisitor out);
}

package com.zarbosoft.wv.jvm;

import com.zarbosoft.rendaw.common.Common;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static com.zarbosoft.wv.jvm.JCodeHelper.jvmInternalName;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;
import static org.objectweb.asm.Opcodes.ASM8;

public class ASMClass {
  public final ClassWriter cw;
  public final Set<String> seenFunctions = new HashSet<>();

  public ASMClass(String classId) {
    this.cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
    cw.visit(52, ACC_PUBLIC + ACC_SUPER, jvmInternalName(classId), null, "java/lang/Object", null);
  }

  public ASMClass setMetaSource(String address) {
    cw.visitSource(address, null);
    return this;
  }

  public ASMClass defineFunction(
      String methodId, String desc, com.zarbosoft.wv.jvm.Code code, List<Object> initialIndexes) {
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, methodId, desc, null, null);
    mv.visitCode();
    code.render(mv, initialIndexes);
    mv.visitMaxs(-1, -1);
    mv.visitEnd();
    return this;
  }

  public byte[] render() {
    cw.visitEnd();

    uncheck(
        () -> {
          try (OutputStream os = Files.newOutputStream(Common.workingDir().resolve("dump.class"))) {
            os.write(cw.toByteArray());
          }
        });
    System.out.println("JVM CHECK");
    CheckClassAdapter.verify(new ClassReader(cw.toByteArray()), true, new PrintWriter(System.out));
    System.out.println("END JVM CHECK");
    return cw.toByteArray();
  }

  public ASMClass defineStaticField(String name, Class t) {
    cw.visitField(
        ACC_PUBLIC + ACC_STATIC, name, Type.getDescriptor(t), Type.getDescriptor(t), null);
    return this;
  }

  public boolean isDefined(String name) {
    return seenFunctions.contains(name);
  }
}

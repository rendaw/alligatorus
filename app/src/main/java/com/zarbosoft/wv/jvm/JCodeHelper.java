package com.zarbosoft.wv.jvm;

import com.zarbosoft.rendaw.common.Assertion;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static org.objectweb.asm.Opcodes.ACONST_NULL;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.BIPUSH;
import static org.objectweb.asm.Opcodes.CHECKCAST;
import static org.objectweb.asm.Opcodes.DRETURN;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.FCONST_0;
import static org.objectweb.asm.Opcodes.FCONST_1;
import static org.objectweb.asm.Opcodes.FCONST_2;
import static org.objectweb.asm.Opcodes.FRETURN;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.ICONST_0;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.ICONST_2;
import static org.objectweb.asm.Opcodes.ICONST_3;
import static org.objectweb.asm.Opcodes.ICONST_4;
import static org.objectweb.asm.Opcodes.ICONST_5;
import static org.objectweb.asm.Opcodes.ICONST_M1;
import static org.objectweb.asm.Opcodes.INVOKEINTERFACE;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.LCONST_0;
import static org.objectweb.asm.Opcodes.LCONST_1;
import static org.objectweb.asm.Opcodes.LRETURN;
import static org.objectweb.asm.Opcodes.NEW;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.SIPUSH;

public class JCodeHelper {
  public static final Code jcodeReturnIntCode = new CodeBuilder().add(IRETURN);
  public static final Code jcodeReturnLongCode = new CodeBuilder().add(LRETURN);
  public static final Code jcodeReturnFloatCode = new CodeBuilder().add(FRETURN);
  public static final Code jcodeReturnDoubleCode = new CodeBuilder().add(DRETURN);
  public static final Code jcodeReturnObjCode = new CodeBuilder().add(ARETURN);
  public static final Code jcodeReturnVoidCode = new CodeBuilder().add(RETURN);
  public static final Code jvmReturnNullCode = new CodeBuilder().add(ACONST_NULL);

  public static String jcodeDesc(Class klass, String methodName) {
    return Type.getMethodDescriptor(findMethod(klass, methodName));
  }

  public static Method findMethod(Class klass, String name) {
    return Arrays.stream(klass.getMethods())
        .filter(m -> name.equals(m.getName()))
        .findFirst()
        .get();
  }

  public static CodeBuilder jcodeLoadString(String s) {
    return new CodeBuilder().add(m -> m.add(new LdcInsnNode(s)));
  }

  private static final Code asmLoadIntM1 = new CodeBuilder().add(ICONST_M1);
  private static final Code asmLoadInt0 = new CodeBuilder().add(ICONST_0);
  private static final Code asmLoadInt1 = new CodeBuilder().add(ICONST_1);
  private static final Code asmLoadInt2 = new CodeBuilder().add(ICONST_2);
  private static final Code asmLoadInt3 = new CodeBuilder().add(ICONST_3);
  private static final Code asmLoadInt4 = new CodeBuilder().add(ICONST_4);
  private static final Code asmLoadInt5 = new CodeBuilder().add(ICONST_5);

  public static Code jcodeLoadInt(int value) {
    if (value == -1) return asmLoadIntM1;
    else if (value == 0) return asmLoadInt0;
    else if (value == 1) return asmLoadInt1;
    else if (value == 2) return asmLoadInt2;
    else if (value == 3) return asmLoadInt3;
    else if (value == 4) return asmLoadInt4;
    else if (value == 5) return asmLoadInt5;
    else if (value >= Byte.MIN_VALUE || value <= Byte.MAX_VALUE)
      return new CodeBuilder().add(m -> m.add(new IntInsnNode(BIPUSH, value)));
    else if (value >= Short.MIN_VALUE || value <= Short.MAX_VALUE)
      return new CodeBuilder().add(m -> m.add(new IntInsnNode(SIPUSH, value)));
    else return new CodeBuilder().add(m -> m.add(new LdcInsnNode(value)));
  }

  private static final Code asmLoadLong0 = new CodeBuilder().add(LCONST_0);
  private static final Code asmLoadLong1 = new CodeBuilder().add(LCONST_1);

  public static Code jcodeLoadLong(long value) {
    if (value == 0) return asmLoadLong0;
    else if (value == 1) return asmLoadLong1;
    else return new CodeBuilder().add(m -> m.add(new LdcInsnNode(value)));
  }

  public static Code jcodeLoadBool(boolean value) {
    if (value) return asmLoadInt1;
    else return asmLoadInt0;
  }

  private static final Code asmLoadFloat0 = new CodeBuilder().add(FCONST_0);
  private static final Code asmLoadFloat1 = new CodeBuilder().add(FCONST_1);
  private static final Code asmLoadFloat2 = new CodeBuilder().add(FCONST_2);

  public static Code jcodeLoadFloat(float value) {
    if (value == 0) return asmLoadFloat0;
    else if (value == 1) return asmLoadFloat1;
    else if (value == 2) return asmLoadFloat2;
    else return new CodeBuilder().add(m -> m.add(new LdcInsnNode(value)));
  }

  private static final Code asmLoadDouble0 = new CodeBuilder().add(FCONST_0);
  private static final Code asmLoadDouble1 = new CodeBuilder().add(FCONST_1);

  public static Code jcodeLoadDouble(double value) {
    if (value == 0) return asmLoadDouble0;
    else if (value == 1) return asmLoadDouble1;
    else return new CodeBuilder().add(m -> m.add(new LdcInsnNode(value)));
  }

  public static String jvmInternalName(String classId) {
    return classId.replace('.', '/');
  }

  public static class JCodeStaticMethod {
    private final String className;
    private final String name;
    private final String desc;

    public JCodeStaticMethod(Class klass, String name) {
      this.className = Type.getInternalName(klass);
      this.name = name;
      this.desc = jcodeDesc(klass, name);
    }

    public void buildCall(CodeBuilder c) {
      c.m().add(new MethodInsnNode(INVOKESTATIC, className, name, desc, false));
    }
  }

  public static class JVMStaticField {
    protected final String desc;
    private final Code loadCode;

    public JVMStaticField(Class klass, String name) {
      this.desc = Type.getDescriptor(uncheck(() -> klass.getField(name)).getType());
      CodeBuilder c = new CodeBuilder();
      c.m().add(new FieldInsnNode(GETSTATIC, Type.getInternalName(klass), name, desc));
      loadCode = c;
    }

    public Code load() {
      return loadCode;
    }
  }

  public static class JVMInterfaceMethod {
    private final String className;
    private final String name;
    private final String desc;

    public JVMInterfaceMethod(Class klass, String name, Class arg1, Class... args2) {
      this.className = Type.getInternalName(klass);
      this.name = name;
      Class[] args = new Class[1 + args2.length];
      args[0] = arg1;
      System.arraycopy(args2, 0, args, 1, args2.length);
      this.desc = Type.getMethodDescriptor(uncheck(() -> klass.getMethod(name, args)));
    }

    public JVMInterfaceMethod(Class klass, String name) {
      this.className = Type.getInternalName(klass);
      this.name = name;
      this.desc = jcodeDesc(klass, name);
    }

    public Code buildCall(Code... args) {
      CodeBuilder out = new CodeBuilder();
      for (Code arg : args) {
        out.add(arg);
      }
      out.m().add(new MethodInsnNode(INVOKEINTERFACE, className, name, desc, true));
      return out;
    }
  }

  public static class JVMVirtualMethod {
    private final String className;
    private final String name;
    private final String desc;

    public JVMVirtualMethod(Class klass, String name, Class arg1, Class... args2) {
      this.className = Type.getInternalName(klass);
      this.name = name;
      Class[] args = new Class[1 + args2.length];
      args[0] = arg1;
      System.arraycopy(args2, 0, args, 1, args2.length);
      this.desc = Type.getMethodDescriptor(uncheck(() -> klass.getMethod(name, args)));
    }

    public JVMVirtualMethod(Class klass, String name) {
      this.className = Type.getInternalName(klass);
      this.name = name;
      this.desc = jcodeDesc(klass, name);
    }

    public Code buildCall(Code... args) {
      return buildCallLine(null, args);
    }

    public Code buildCallLine(Integer line, Code... args) {
      CodeBuilder out = new CodeBuilder();
      for (Code arg : args) {
        out.add(arg);
      }
      out.line(line);
      out.m().add(new MethodInsnNode(INVOKEVIRTUAL, className, name, desc, false));
      return out;
    }
  }

  public static class JVMInstance {
    private final String className;
    private final String sig;
    private final int expNumArgs;

    public static JVMInstance first(Class klass) {
      String className = Type.getInternalName(klass);
      Constructor constructor =
          Arrays.stream(klass.getConstructors())
              .filter(m -> m.getParameterCount() > 0)
              .findFirst()
              .get();
      return new JVMInstance(className, constructor);
    }

    public static JVMInstance specific(Class klass, Class... args) {
      String className = Type.getInternalName(klass);
      Constructor constructor = uncheck(() -> klass.getConstructor(args));
      return new JVMInstance(className, constructor);
    }

    private JVMInstance(String className, Constructor c) {
      this.className = className;
      this.sig = Type.getConstructorDescriptor(c);
      this.expNumArgs = c.getParameterCount();
    }

    public Code buildNew(Code... arguments) {
      return buildNewLine(null, arguments);
    }

    public Code buildNewLine(Integer line, Code... arguments) {
      CodeBuilder c = new CodeBuilder();
      if (arguments.length != expNumArgs) throw new Assertion();
      c.m().add(new TypeInsnNode(NEW, className));
      c.m().add(new InsnNode(DUP));
      for (Code argument : arguments) {
        c.add(argument);
      }
      c.line(line);
      c.m().add(new MethodInsnNode(INVOKESPECIAL, className, "<init>", sig, false));
      return c;
    }
  }

  public static final JCodeStaticMethod boxInt = new JCodeStaticMethod(Integer.class, "valueOf");
  public static final JCodeStaticMethod boxLong = new JCodeStaticMethod(Long.class, "valueOf");
  public static final JCodeStaticMethod boxFloat = new JCodeStaticMethod(Float.class, "valueOf");
  public static final JCodeStaticMethod boxDouble = new JCodeStaticMethod(Double.class, "valueOf");
  public static final JCodeStaticMethod boxBoolean =
      new JCodeStaticMethod(Boolean.class, "valueOf");

  public static class JVMField {
    private final JVMWrapper wrapper;
    private final String name;
    private final String desc;

    public JVMField(JVMWrapper wrapper, String name) {
      this.wrapper = wrapper;
      this.name = name;
      this.desc = Type.getDescriptor(uncheck(() -> wrapper.klass.getField(name)).getType());
    }

    public Code buildLoad(Code value) {
      return new CodeBuilder()
          .add(value)
          .add(
              m -> {
                m.add(new FieldInsnNode(GETFIELD, wrapper.className, name, desc));
              });
    }
  }

  public static class JVMWrapper {
    // TODO deduplicate more classes to use this wrapper as a core
    private final Class klass;
    private final String className;

    public JVMWrapper(Class klass) {
      this.klass = klass;
      this.className = Type.getInternalName(klass);
    }

    public Code buildCast(Code value) {
      return new CodeBuilder().add(value).add(m -> m.add(new TypeInsnNode(CHECKCAST, className)));
    }

    public JVMField field(String name) {
      return new JVMField(this, name);
    }
  }
}

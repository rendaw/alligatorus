package com.zarbosoft.wv.jvm;

import com.zarbosoft.wv.Value;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.ACC_INTERFACE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ACC_SUPER;

public class ASMValueInterface {
  public final ClassWriter cw;

  public ASMValueInterface(String classId) {
    this.cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
    cw.visit(
        52,
        ACC_PUBLIC + ACC_SUPER + ACC_INTERFACE,
        classId,
        null,
        "java/lang/Object",
        new String[] {Type.getInternalName(Value.class)});
  }

  public ASMValueInterface defineFunction(String methodId, String desc) {
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, methodId, desc, null, null);
    mv.visitEnd();
    return this;
  }

  public byte[] render() {
    cw.visitEnd();
    return cw.toByteArray();
  }
}

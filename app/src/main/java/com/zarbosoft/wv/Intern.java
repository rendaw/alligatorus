package com.zarbosoft.wv;

import com.zarbosoft.wv.value.StringValue;

public class Intern {
  public static final StringValue class_ = new StringValue("class");
  public static final StringValue function = new StringValue("function");
  public static final StringValue implementFunction = new StringValue("implementFunction");
  public static final StringValue bytecode = new StringValue("bytecode");
  public static final StringValue in = new StringValue("in");
  public static final StringValue out = new StringValue("out");
  public static final StringValue arg = new StringValue("arg");
  public static final StringValue importLocal = new StringValue("importLocal");
  public static final StringValue importRemote = new StringValue("importRemote");
  public static final StringValue importBlock = new StringValue("importBlock");
  public static final StringValue externFunction = new StringValue("externFunction");
  public static final StringValue type = new StringValue("type");
  public static final StringValue int_ = new StringValue("int");
  public static final StringValue byte_ = new StringValue("byte");
  public static final StringValue long_ = new StringValue("long");
  public static final StringValue float_ = new StringValue("float");
  public static final StringValue double_ = new StringValue("double");
  public static final StringValue bool = new StringValue("bool");
  public static final StringValue string = new StringValue("string");
  public static final StringValue array = new StringValue("array");
  public static final StringValue write = new StringValue("write");
  public static final StringValue key = new StringValue("key");
  public static final StringValue callable = new StringValue("callable");
  public static final StringValue mortar = new StringValue("mortar");
  public static final StringValue jbc = new StringValue("jbc");
  public static final StringValue addressKey = new StringValue("address");
  public static final StringValue hashTypeKey = new StringValue("hashType");
  public static final StringValue hashKey = new StringValue("hash");
}

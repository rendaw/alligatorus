package com.zarbosoft.wv.mortar;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.DuplicateField;
import com.zarbosoft.wv.error.MultiErrorException;
import com.zarbosoft.wv.error.OtherError;
import com.zarbosoft.wv.errorbase.ExprSubError;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.help.KeyMap;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.importing.Importing;
import com.zarbosoft.wv.importing.ModuleAddr;
import com.zarbosoft.wv.importing.RemoteModuleAddr;
import com.zarbosoft.wv.importing.RemoteModuleSubContext;
import com.zarbosoft.wv.typecheck.CheckRecord;
import com.zarbosoft.wv.typecheck.CheckTuple;
import com.zarbosoft.wv.typecheck.CheckUniformRecord;
import com.zarbosoft.wv.value.ByteArrayValue;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.Record.Builder;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.ConstValue;
import com.zarbosoft.wv.valuebase.IntrinsicCallable;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.rendaw.common.Common.uncheck;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class Intrinsic {
  private static final CheckRecord checkImportRemote =
      new CheckRecord()
          .put(Intern.addressKey, StringValue.check)
          .put(Intern.hashKey, StringValue.check);

  private static Pair<Value, Hash> waitFuture(
      Context context, CompletableFuture<Pair<ConstValue, Hash>> f) {
    try {
      return (Pair<Value, Hash>) (Pair) uncheck(() -> f.get());
    } catch (MultiErrorException e) {
      context.suberrors.addAll(e.inner);
      return new Pair<>(err, null);
    } catch (ExprSubError e) {
      context.suberrors.add(e);
      return new Pair<>(err, null);
    } catch (Exception e) {
      context.suberrors.add(new OtherError(e));
      return new Pair<>(err, null);
    }
  }

  public static final Record intrinsic =
      new Builder()
          .put(Intern.importLocal, new DoImportLocal())
          .put(Intern.importRemote, new DoImportRemote())
          .put(Intern.importBlock, new DoImportBlock())
          .put(Intern.write, new DoWrite())
          .build();

  private static class DoImportLocal extends IntrinsicCallable {
    @Override
    public Value call(Context context, Value arg) {
      if (!StringValue.check.check(context, new StringValue("arg"), arg)) return err;
      ModuleAddr addr = context.module.addr.resolveLocal(((StringValue) arg).value);
      Pair<Value, Hash> result = waitFuture(context, context.module.importLocal(context, addr));
      if (context.module instanceof RemoteModuleSubContext)
        ((RemoteModuleSubContext) context.module)
            .hash.put(new Key.Tuple(addr.pieces(), result.second.pieces()));
      return result.first.addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoImportLocal();
    }
  }

  private static class DoImportRemote extends IntrinsicCallable {
    @Override
    public Value call(Context context, Value arg0) {
      if (!checkImportRemote.check(context, com.zarbosoft.wv.Intrinsic.argBasePath, arg0))
        return err;
      Record arg = (Record) arg0;
      RemoteModuleAddr addr =
          new RemoteModuleAddr(
              ((StringValue) arg.data.get(Intern.addressKey)).value,
              ((StringValue) arg.data.get(Intern.hashKey)).value);
      Pair<Value, Hash> result = waitFuture(context, Importing.importRemoteEntry(context, addr));
      if (context.module instanceof RemoteModuleSubContext)
        ((RemoteModuleSubContext) context.module)
            .hash.put(new Key.Tuple(addr.pieces(), result.second.pieces()));
      return result.first.addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoImportRemote();
    }
  }

  private static class DoImportBlock extends IntrinsicCallable {
    final TypeCheck check =
        new CheckRecord()
            .put(new StringValue("remote"), new CheckUniformRecord(checkImportRemote))
            .put(new StringValue("local"), new CheckUniformRecord(StringValue.check));

    @Override
    public Value call(Context context, Value arg0) {
      if (!check.check(context, new StringValue("arg"), arg0)) return err;
      Record arg = (Record) arg0;
      KeyMap<Pair<ModuleAddr, CompletableFuture<Pair<ConstValue, Hash>>>> futures = new KeyMap<>();
      for (Pair<Key, Value> e :
          iterable(((Record) arg.data.get(new StringValue("remote"))).data.stream())) {
        Record spec = (Record) e.second;
        RemoteModuleAddr addr =
            new RemoteModuleAddr(
                ((StringValue) spec.data.get(Intern.addressKey)).value,
                ((StringValue) spec.data.get(Intern.hashKey)).value);
        futures.put(e.first, new Pair<>(addr, Importing.importRemoteEntry(context, addr)));
      }
      for (Pair<Key, Value> e :
          iterable(((Record) arg.data.get(new StringValue("local"))).data.stream())) {
        if (futures.contains(e.first)) {
          context.suberrors.add(
              new DuplicateField(
                  com.zarbosoft.wv.Intrinsic.argBasePath.plus(new StringValue("local")), e.first));
          continue;
        }
        ModuleAddr addr = context.module.addr.resolveLocal(((StringValue) e.second).value);
        futures.put(e.first, new Pair<>(addr, context.module.importLocal(context, addr)));
      }

      KeyMap<Value> out = new KeyMap<>(new LinkedHashMap<>());
      for (Pair<Key, Pair<ModuleAddr, CompletableFuture<Pair<ConstValue, Hash>>>> x :
          iterable(futures.stream())) {
        Pair<Value, Hash> got = waitFuture(context, x.second.second);
        if (context.module instanceof RemoteModuleSubContext)
          ((RemoteModuleSubContext) context.module)
              .hash.put(new Key.Tuple(x.second.first.pieces(), got.second.pieces()));
        out.put(x.first, got.first);
      }

      return new Record(out).addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoImportBlock();
    }
  }

  public static TargetState gatherTargetState(Context context, Value value) {
    TargetState out = value.takePre();
    if (value instanceof Record) {
      for (Pair<Key, Value> e : iterable(((Record) value).data.stream())) {
        if (out == null) out = gatherTargetState(context, e.second);
        else out.combine(context, gatherTargetState(context, e.second));
      }
    } else if (value instanceof Tuple) {
      for (Value e : ((Tuple) value).values) {
        if (out == null) out = gatherTargetState(context, e);
        else out.combine(context, gatherTargetState(context, e));
      }
      return out;
    } else {
      // nop
    }
    if (out == null) return value.takePost();
    else {
      out.combine(context, value.takePost());
      return out;
    }
  }

  private static class DoWrite extends IntrinsicCallable {
    final TypeCheck check =
        new CheckTuple().put("path", StringValue.check).put("bytes", ByteArrayValue.check);

    @Override
    public Value call(Context context, Value arg) {
      if (!check.check(context, arg)) return err;
      List<Value> values = ((Tuple) arg).values;
      try {
        Path path = Paths.get(((StringValue) values.get(0)).value);
        Files.createDirectories(path.getParent());
        try (OutputStream os = Files.newOutputStream(path)) {
          os.write(((ByteArrayValue) values.get(1)).bytes);
        }
      } catch (Exception e) {
        context.suberrors.add(new OtherError(e));
        return err;
      }
      return new VoidValue().addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoWrite();
    }
  }
}

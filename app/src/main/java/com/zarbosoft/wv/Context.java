package com.zarbosoft.wv;

import com.zarbosoft.wv.error.ExprError;
import com.zarbosoft.wv.errorbase.ExprSubError;
import com.zarbosoft.wv.hash.StructuredSHA256;
import com.zarbosoft.wv.importing.ModuleAddr;
import com.zarbosoft.wv.importing.ModuleSubContext;
import com.zarbosoft.wv.importing.RemoteModuleAddr;
import org.pcollections.HashTreePSet;
import org.pcollections.PSet;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.wv.Key.keyComparator;

public class Context {
  public final Supercontext supercontext;
  public final Scope scope;
  public final ConcurrentLinkedQueue<ExprError> errors;
  public final ConcurrentLinkedQueue<ExprSubError> suberrors;
  public final ModuleSubContext module;
  public final PSet<ModuleAddr> importLoopDetect;
  public final Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> moduleImportOverride;
  public final Map<RemoteModuleAddr, RemoteModuleAddr> generalImportOverride;
  public final String importContextHash;
  public final int stageDepth;

  public Context(
      Supercontext supercontext,
      Scope scope,
      ConcurrentLinkedQueue<ExprError> errors,
      ConcurrentLinkedQueue<ExprSubError> suberrors,
      ModuleSubContext module,
      PSet<ModuleAddr> importLoopDetect,
      Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> moduleImportOverride,
      Map<RemoteModuleAddr, RemoteModuleAddr> generalImportOverride,
      String importContextHash,
      int stageDepth) {
    this.supercontext = supercontext;
    this.scope = scope;
    this.errors = errors;
    this.suberrors = suberrors;
    this.module = module;
    this.importLoopDetect = importLoopDetect;
    this.moduleImportOverride = moduleImportOverride;
    this.generalImportOverride = generalImportOverride;
    this.importContextHash = importContextHash;
    this.stageDepth = stageDepth;
  }

  private static final Comparator<Map.Entry<? extends Key, ?>> mapKeyComparator =
      new Comparator<Map.Entry<? extends Key, ?>>() {
        @Override
        public int compare(Map.Entry<? extends Key, ?> a, Map.Entry<? extends Key, ?> b) {
          return keyComparator.compare(a.getKey(), b.getKey());
        }
      };

  protected static String computeImportContextHash(
      Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> moduleImportOverride,
      Map<RemoteModuleAddr, RemoteModuleAddr> generalImportOverride) {
    StructuredSHA256 out = new StructuredSHA256();
    for (Map.Entry<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> x :
        iterable(moduleImportOverride.entrySet().stream().sorted(mapKeyComparator))) {
      for (Map.Entry<RemoteModuleAddr, RemoteModuleAddr> y :
          iterable(x.getValue().entrySet().stream().sorted(mapKeyComparator))) {
        out.put(new Key.Tuple(x.getKey().pieces(), y.getKey().pieces(), y.getValue().pieces()));
      }
    }
    for (Map.Entry<RemoteModuleAddr, RemoteModuleAddr> x :
        iterable(generalImportOverride.entrySet().stream().sorted(mapKeyComparator))) {
      out.put(new Key.Tuple(x.getKey().pieces(), x.getValue().pieces()));
    }
    return out.serialize();
  }

  public static Context createSeparateModule(
      com.zarbosoft.wv.Context source, ModuleSubContext module, PSet<ModuleAddr> importLoopDetect) {
    return new Context(
        source.supercontext,
        new Scope(null),
        source.errors,
        new ConcurrentLinkedQueue<>(),
        module,
        importLoopDetect,
        source.moduleImportOverride,
        source.generalImportOverride,
        source.importContextHash,
        0);
  }

  public static Context createStart(
      Supercontext supercontext, ModuleSubContext module, ConcurrentLinkedQueue<ExprError> errors) {
    Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> moduleImportOverride =
        new HashMap<>();
    Map<RemoteModuleAddr, RemoteModuleAddr> generalImportOverride = new HashMap<>();
    return new Context(
        supercontext,
        new Scope(null),
        errors,
        new ConcurrentLinkedQueue<>(),
        module,
        HashTreePSet.empty(),
        moduleImportOverride,
        generalImportOverride,
        computeImportContextHash(moduleImportOverride, generalImportOverride),
        0);
  }

  public Context pushScope() {
    return new Context(
        supercontext,
        new Scope(scope),
        errors,
        suberrors,
        module,
        importLoopDetect,
        moduleImportOverride,
        generalImportOverride,
        importContextHash,
        stageDepth);
  }

  public Context pushImportOverrides(
      Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> moduleImportOverride,
      Map<RemoteModuleAddr, RemoteModuleAddr> generalImportOverride) {
    Map<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> newModuleImportOverride =
        new HashMap<>(this.moduleImportOverride);
    for (Map.Entry<RemoteModuleAddr, Map<RemoteModuleAddr, RemoteModuleAddr>> e :
        moduleImportOverride.entrySet()) {
      Map<RemoteModuleAddr, RemoteModuleAddr> subMap =
          newModuleImportOverride.compute(
              e.getKey(),
              (k, existing) -> existing == null ? new HashMap<>() : new HashMap<>(existing));
      for (Map.Entry<RemoteModuleAddr, RemoteModuleAddr> e2 : e.getValue().entrySet()) {
        if (subMap.containsKey(e2.getKey())) continue;
        subMap.put(e2.getKey(), e2.getValue());
      }
    }
    Map<RemoteModuleAddr, RemoteModuleAddr> newGeneralImportOverride =
        new HashMap<>(this.generalImportOverride);
    for (Map.Entry<RemoteModuleAddr, RemoteModuleAddr> e : generalImportOverride.entrySet()) {
      if (newGeneralImportOverride.containsKey(e.getKey())) continue;
      newGeneralImportOverride.put(e.getKey(), e.getValue());
    }
    return new Context(
        supercontext,
        scope,
        errors,
        suberrors,
        module,
        importLoopDetect,
        newModuleImportOverride,
        newGeneralImportOverride,
        computeImportContextHash(newModuleImportOverride, newGeneralImportOverride),
        stageDepth);
  }

  public Context pushStage() {
    return new Context(
        supercontext,
        scope,
        errors,
        suberrors,
        module,
        importLoopDetect,
        moduleImportOverride,
        generalImportOverride,
        importContextHash,
        stageDepth + 1);
  }

  public Context pushExpr() {
    return new Context(
        supercontext,
        scope,
        errors,
        new ConcurrentLinkedQueue<>(),
        module,
        importLoopDetect,
        moduleImportOverride,
        generalImportOverride,
        importContextHash,
        stageDepth);
  }
}

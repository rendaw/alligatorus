package com.zarbosoft.wv;

public class Binding {
  public Scope scope;
  public final Value value;

  public Binding(Value value) {
    this.value = value;
  }
}

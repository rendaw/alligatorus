package com.zarbosoft.wv;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.importing.IntrinsicCacheAddr;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.valuebase.ConstValue;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.lang.reflect.Field;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public class Intrinsic {
  public static final Record intrinsic;

  static {
    intrinsic =
        new Record.Builder()
            .put(Intern.mortar, com.zarbosoft.wv.mortar.Intrinsic.intrinsic)
            .put(Intern.jbc, com.zarbosoft.wv.target.jbc.Intrinsic.intrinsic)
            .build();
    assignIntrinsicAddr(TreePVector.empty(), intrinsic);
  }

  public static final PVector<Key> argBasePath = TreePVector.singleton(new StringValue("arg"));

  private static void assignIntrinsicAddr(PVector<String> path, Value value) {
    uncheck(
        () -> {
          Field f = value.getClass().getField("cacheAddr");
          if (f != null) {
            IntrinsicCacheAddr intrinsicAddr = new IntrinsicCacheAddr(path);
            Global.intrinsicLookup.put(intrinsicAddr, (ConstValue) value);
            f.set(value, intrinsicAddr);
          }
        });
    if (value instanceof Record) {
      for (Pair<Key, Value> e : iterable(((Record) value).data.stream())) {
        assignIntrinsicAddr(path.plus(((StringValue) e.first).value), e.second);
      }
    }
  }
}

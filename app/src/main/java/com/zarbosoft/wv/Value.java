package com.zarbosoft.wv;

import com.zarbosoft.rendaw.common.Pair;

import java.util.List;

public interface Value {
  boolean isConst();

  /**
   * Machine-usable (humanish) description of this value for type error messages
   *
   * @return
   */
  String errorDesc();

  /**
   * Returns unforked accessed value - forking will be done at realize
   *
   * @param context
   * @param key
   * @return
   */
  Value access(Context context, Key key);

  /**
   * Do anything required to drop
   *
   * @param context
   * @return
   */
  Value drop(Context context);

  /**
   * Returns value to bind or error
   *
   * @param context
   * @return null == error; first is result of binding process (to store); second is token to
   *     retrieve from scope later
   */
  Pair<TargetState, Value> bind(Context context);

  /**
   * Never has targetstate since always done in (internal) purely mortar
   *
   * @param context
   * @param key
   * @return
   */
  Value accessMethod(Context context, Key key);

  TargetState pre();

  Value addPre(Context context, TargetState... pre);

  TargetState post();

  Value addPost(Context context, TargetState... post);

  Value realize(Context context);

  default Value addPreFromValues(Context context, List<Value> values) {
    TargetState[] targetState = new TargetState[values.size() * 2];
    for (int i = 0; i < values.size(); ++i) {
      Value value = values.get(i);
      targetState[i * 2 + 0] = value.pre();
      targetState[i * 2 + 1] = value.post();
    }
    return this;
  }

  /**
   * Access a free value - any unaccessed subvalues should be dropped and
   * target state collected
   * @param context
   * @param keys
   * @return
   */
  Value realizeAccess(Context context, List<Value> keys);

  /**
   * Access a bound value - Only the outer value, keys, and the end fork state
   * should end up in the output value
   * @param context
   * @param keys
   * @return
   */
  Value realizeAccessFork(Context context, List<Value> keys);

  TargetState takePre();

  TargetState takePost();

  default String toSurveyJSON() {
    return errorDesc();
  }
}

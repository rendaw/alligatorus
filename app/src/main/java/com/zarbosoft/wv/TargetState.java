package com.zarbosoft.wv;

public interface TargetState {
  boolean combine(Context context, TargetState other);

  TargetState createMutable();

  String errorDesc();
}

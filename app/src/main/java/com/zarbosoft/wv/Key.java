package com.zarbosoft.wv;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.DeadCode;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import org.pcollections.PVector;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Stream;

import static com.zarbosoft.wv.valuebase.ErrorValue.err;

/** Implementing values must have rs0_key trait */
public interface Key {
  String errorDesc = "key type";
  TypeCheck check = new TypeCheck() {
    @Override
    public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
      if (!(arg instanceof Key)) {
        context.suberrors.add(new TypeMismatch(path,errorDesc,arg.errorDesc()));
        return false;
      }
      return true;
    }
  };

  static Stream<Comparable> recursiveFlattenKeyPieces(Stream<Comparable> source) {
    return source.flatMap(e -> {
      if (e instanceof Tuple) {
        return recursiveFlattenKeyPieces(Arrays.stream(((Tuple) e).v));
      } else return Stream.of(e);
    });
  }

  String toStringKey();

  public static final class Tuple implements Comparable<Tuple> {
    public final Comparable[] v;

    public Tuple(Comparable... v) {
      this.v = v.clone();
    }

    public Tuple(Collection<Comparable> v) {
      this.v = v.toArray(Comparable[]::new);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Tuple tuple = (Tuple) o;
      return Arrays.equals(v, tuple.v);
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(v);
    }

    @Override
    public int compareTo(Tuple tuple) {
      for (int i = 0; i < Math.min(v.length, tuple.v.length); ++i) {
        int d = compareInternal(v[i], tuple.v[i]);
        if (d != 0) return d;
      }
      return Integer.compare(v.length, tuple.v.length);
    }

    public void roughSerialize(Writer writer) {
      writer.arrayBegin();
      for (Comparable e : v) {
        roughSerializeInternal(writer, e);
      }
      writer.arrayEnd();
    }
  }

  private static int typeOrder(Class klass) {
    if (klass == Integer.class) return 0;
    if (klass == Long.class) return 1;
    if (klass == Boolean.class) return 2;
    if (klass == String.class) return 3;
    if (klass == Tuple.class) return 4;
    throw new Assertion();
  }

  static void roughSerializeInternal(Writer writer, Comparable e) {
    Class klass = e.getClass();
    if (klass == Tuple.class) ((Tuple) e).roughSerialize(writer);
    else if (klass == Integer.class) writer.primitive(Integer.toString((Integer) e));
    else if (klass == Long.class) writer.primitive(Long.toString((Long) e));
    else if (klass == Boolean.class) writer.primitive(((Boolean) e) ? "true" : "false");
    else if (klass == String.class) writer.primitive((String) e);
    else throw new DeadCode();
  }

  default void roughSerialize(Writer writer) {
    roughSerializeInternal(writer, pieces());
  }

  private static int compareInternal(Comparable x1, Comparable x2) {
    if (x1 instanceof Key) x1 = ((Key) x1).pieces();
    if (x2 instanceof Key) x2 = ((Key) x2).pieces();
    int diff1 = typeOrder(x1.getClass()) - typeOrder(x2.getClass());
    if (diff1 != 0) return diff1;
    int diff2 = x1.compareTo(x2);
    if (diff2 != 0) return diff2;
    return 0;
  }

  public static Comparator<Key> keyComparator = new Comparator<Key>() {
    @Override
    public int compare(Key a, Key b) {
      return keyPieceComparator.compare(a.pieces(),b.pieces());
    }
  };

  public static Comparator<Comparable> keyPieceComparator = new Comparator<Comparable>() {
    @Override
    public int compare(Comparable a, Comparable b) {
      return compareInternal(a,b);
    }
  };

  /** @return int, long, bool, string, Tuple, or Key */
  public Comparable pieces();
}

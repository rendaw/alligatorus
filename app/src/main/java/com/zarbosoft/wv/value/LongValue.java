package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;

import java.nio.file.Path;

public class LongValue extends SimplePrimitiveConstValue implements Key, SimpleObject {
  public static final String errorDesc = "long";
  public final long value;

  public LongValue(long value) {
    this.value = value;
  }

  @Override
  public String toStringKey() {
    return "l$" + value;
  }

  @Override
  public Comparable pieces() {
    return value;
  }

  @Override
  public void serializeCacheReference(Context context, Path cacheBase, Writer writer) {
    writer.type("long").primitive(Long.toString(value));
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new LongValue(value);
  }
}

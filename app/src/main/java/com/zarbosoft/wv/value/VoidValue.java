package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;
import org.pcollections.PVector;

import java.nio.file.Path;

public class VoidValue extends SimplePrimitiveConstValue implements SimpleObject {
  public static final String errorDesc = "void";
  public static final TypeCheck check =
      new TypeCheck() {
        @Override
        public boolean checkInternal(com.zarbosoft.wv.Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof VoidValue)) {
            context.suberrors.add(new TypeMismatch(path, VoidValue.errorDesc, arg.errorDesc()));
            return false;
          }
          return true;
        }
      };

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public void serializeCacheReference(Context context, Path cacheBase, Writer writer) {
    writer.type("void").recordBegin().recordEnd();
  }

  @Override
  public Value newCopy() {
    return new VoidValue();
  }
}

package com.zarbosoft.wv.value;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Evaluate;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.help.KeyMap;
import com.zarbosoft.wv.valuebase.ConstValue;
import com.zarbosoft.wv.valuebase.SimpleCacheConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.pcollections.TreePVector;

import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.rendaw.common.Common.sublist;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public final class Record extends SimpleCacheConstValue implements SimpleObject {
  public static final String errorDesc = "record";
  public final KeyMap<Value> data;
  private final boolean isConst;

  @Override
  protected void serializeCacheInner(Context context, Path cacheBase, Writer writer) {
    if (!isConst) throw new Assertion();
    writer.type("record").arrayBegin();
    for (Pair<Key, Value> e : iterable(data.stream())) {
      writer.arrayBegin();
      ((ConstValue) e.first).serializeCacheReference(context, cacheBase, writer);
      ((ConstValue) e.second).serializeCacheReference(context, cacheBase, writer);
      writer.arrayEnd();
    }
    writer.arrayEnd();
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public String toSurveyJSON() {
    return String.format(
        "{\"type\":\"record\",\"fields\":{%s}}",
        data.stream()
            .map(e -> String.format("\"%s\":%s", e.first.toStringKey(), e.second.toSurveyJSON()))
            .collect(Collectors.joining(",")));
  }

  @Override
  public Value access(Context context, Key key) {
    Value found = data.get(key);
    if (found == null) {
      context.suberrors.add(new MissingField(TreePVector.empty(), key));
      return err;
    }
    return found;
  }

  @Override
  public Value drop(Context context) {
    TargetState targetState = null;
    for (Value value : iterable(data.stream().map(e -> e.second))) {
      TargetState t = gatherTargetState(context, Evaluate.instance.drop(context, value));
      if (targetState == null) targetState = t;
      else targetState.combine(context, t);
    }
    return new VoidValue().addPre(context, targetState);
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    boolean failed = false;
    TargetState targetState = null;
    KeyMap<Value> newValues = new KeyMap<>(new LinkedHashMap<>(data.size()));
    for (Pair<Key, Value> e : iterable(data.stream())) {
      Pair<TargetState, Value> res = e.second.bind(context);
      if (res == null) failed = true;
      if (res.first != null) {
        if (targetState == null) targetState = res.first;
        else targetState.combine(context, res.first);
      }
      newValues.put(e.first, res.second);
    }
    if (failed) return null;
    return new Pair<>(targetState, new Record(newValues));
  }

  @Override
  public Value realize(Context context) {
    return this;
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    if (keys.isEmpty()) return this;
    Value out = null;
    TargetState pre = null;
    TargetState post = null;
    Value key = keys.get(0);
    for (Pair<Key, Value> e : iterable(data.stream())) {
      if (((Key) key).pieces().equals(e.first.pieces())) {
        out = e.second;
      } else {
        TargetState dropped = gatherTargetState(context, Evaluate.instance.drop(context, e.second));
        if (out == null) {
          if (pre == null) pre = dropped;
          else pre.combine(context, dropped);
        } else {
          if (post == null) post = dropped;
          else post.combine(context, dropped);
        }
      }
    }
    return out.addPre(context, pre(), key.pre(), key.post(), pre)
        .addPost(context, post, post())
        .realizeAccess(context, sublist(keys, 1));
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    if (keys.isEmpty()) {
      KeyMap<Value> newValues = new KeyMap<>(new LinkedHashMap<>(data.size()));
      for (Pair<Key, Value> e : iterable(data.stream())) {
        newValues.put(e.first, e.second.realizeAccessFork(context, ImmutableList.of()));
      }
      return new Record(newValues).addPre(context, pre()).addPost(context, post());
    }
    IntValue key = (IntValue) keys.get(0);
    Value value =
        data.get(key)
            .addPre(context, pre(), key.pre(), key.post())
            .addPost(context, post())
            .realizeAccessFork(context, sublist(keys, 1));
    return value;
  }

  @Override
  public Value newCopy() {
    throw new Assertion();
  }

  public static final class Builder {
    KeyMap<Value> data = new KeyMap<>(new LinkedHashMap<>());

    public Builder put(Key key, Value value) {
      if (data.contains(key)) throw new Assertion();
      data.put(key, value);
      return this;
    }

    public Record build() {
      return new Record(data);
    }
  }

  public Record(KeyMap<Value> data) {
    if (!(data.inner instanceof LinkedHashMap)) throw new Assertion();
    this.data = data;
    this.isConst = data.stream().allMatch(v -> v.second.isConst());
  }

  @Override
  public boolean isConst() {
    return isConst;
  }
}

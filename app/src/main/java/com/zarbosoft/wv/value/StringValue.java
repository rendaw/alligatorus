package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.jvm.JCodeHelper;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;
import org.pcollections.PVector;

import java.nio.file.Path;

public class StringValue extends SimplePrimitiveConstValue implements Key, SimpleObject {
  public static final JCodeHelper.JVMInstance jvmInstance =
      JCodeHelper.JVMInstance.first(StringValue.class);
  public static final String errorDesc = "string";
  public static TypeCheck check =
      new TypeCheck() {
        @Override
        public boolean checkInternal(com.zarbosoft.wv.Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof StringValue)) {
            context.suberrors.add(new TypeMismatch(path, StringValue.errorDesc, arg.errorDesc()));
            return false;
          }
          return true;
        }
      };
  public final String value;

  public StringValue(String value) {
    this.value = value;
  }

  @Override
  public String toStringKey() {
    return "s$" + value;
  }

  @Override
  public Comparable pieces() {
    return value;
  }

  @Override
  public void serializeCacheReference(
    Context context, Path cacheBase, Writer writer
  ) {
    writer.type("string").primitive(value);
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new StringValue(value);
  }
}

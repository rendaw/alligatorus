package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.importing.ModuleAddr;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;

import java.nio.file.Path;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.wv.Key.recursiveFlattenKeyPieces;

public class SymbolValue extends SimplePrimitiveConstValue implements Key, SimpleObject {
  public final ModuleAddr module;
  public final int id;
  public final String hint;

  public SymbolValue(ModuleAddr module, int id, String hint) {
    this.module = module;
    this.id = id;
    this.hint = hint;
  }

  @Override
  public String toStringKey() {
    return recursiveFlattenKeyPieces(Stream.of("sym", module.toSurveyAddrTuple(), id))
        .map(x -> x.toString())
        .collect(Collectors.joining("$"));
  }

  @Override
  public Comparable pieces() {
    return new Tuple("sym", module.pieces(), id);
  }

  @Override
  public void serializeCacheReference(Context context, Path cacheBase, Writer writer) {
    writer.type("symbol").recordBegin();
    writer.key("module");
    module.serializeSimple(writer);
    writer.key("id").primitive(Integer.toString(id));
    writer.key("hint").primitive(hint);
    writer.recordEnd();
  }

  @Override
  public String errorDesc() {
    return "symbol";
  }

  @Override
  public Value newCopy() {
    return new SymbolValue(module, id, hint);
  }
}

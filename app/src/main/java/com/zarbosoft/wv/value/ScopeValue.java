package com.zarbosoft.wv.value;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Binding;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Scope;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.ValueBase;
import org.pcollections.TreePVector;

import java.util.List;

import static com.zarbosoft.rendaw.common.Common.sublist;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class ScopeValue extends ValueBase implements Value, SimpleObject {
  public static final String errorDesc = "scope";
  private final Scope scope;

  public ScopeValue(Scope scope) {
    this.scope = scope;
  }

  @Override
  public boolean isConst() {
    return false;
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value access(Context context, Key key) {
    Binding got = scope.get(key);
    if (got == null) {
      context.suberrors.add(new MissingField(TreePVector.empty(), key));
      return err;
    }
    return got.value;
  }

  @Override
  public Value drop(Context context) {
    return new VoidValue().addPre(context, pre()).addPost(context, post());
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    return new Pair<>(gatherTargetState(context, this), new ScopeValue(scope));
  }

  @Override
  public Value realize(Context context) {
    return this;
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    Value key = keys.get(0);
    return scope
        .get((Key) key)
        .value
        .realizeAccessFork(context, sublist(keys, 1))
        .addPre(context, pre(), key.pre(), key.post()).addPost(context,post());
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    throw new AssertionError();
  }
}

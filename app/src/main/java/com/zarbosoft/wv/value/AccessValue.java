package com.zarbosoft.wv.value;

import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Evaluate;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.valuebase.ValueBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AccessValue extends ValueBase implements Value {
  private final Value standin;
  private final Value key;
  public final Value target;

  public AccessValue(Value target, Value key, Value standin) {
    this.target = target;
    this.key = key;
    this.standin = standin;
  }

  @Override
  public boolean isConst() {
    return false;
  }

  @Override
  public String errorDesc() {
    throw new Assertion();
  }

  @Override
  public String toSurveyJSON() {
    return standin.toSurveyJSON();
  }

  @Override
  public Value access(Context context, Key key) {
    return standin.access(context, key);
  }

  @Override
  public Value drop(Context context) {
    return Evaluate.instance
        .drop(context, target)
        .addPre(context, pre())
        .addPost(context, key.pre(), key.post(), post());
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    throw new AssertionError(); // should be realized
  }

  @Override
  public Value accessMethod(Context context, Key key) {
    throw new AssertionError(); // should be realized
  }

  @Override
  public Value realize(Context context) {
    List<Value> keys = new ArrayList<>();
    Value at = this;
    while (at instanceof AccessValue) {
      keys.add(((AccessValue) at).key);
      at = ((AccessValue) at).target;
    }
    Collections.reverse(keys);
    return at.realizeAccess(context, keys).addPre(context, pre()).addPost(context, post());
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    throw new AssertionError();
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    throw new AssertionError();
  }
}

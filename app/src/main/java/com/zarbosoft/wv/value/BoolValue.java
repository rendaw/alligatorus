package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;

import java.nio.file.Path;

public class BoolValue extends SimplePrimitiveConstValue implements Key, SimpleObject {
  public static final String errorDesc = "bool";
  public final boolean value;

  public BoolValue(boolean value) {
    this.value = value;
  }

  @Override
  public String toStringKey() {
    return "b$" + (value ? "true":"false");
  }

  @Override
  public Comparable pieces() {
    return value;
  }

  @Override
  public void serializeCacheReference(Context context, Path cacheBase, Writer writer) {
    writer.type("bool").primitive(value ? "true" : "false");
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new BoolValue(value);
  }
}

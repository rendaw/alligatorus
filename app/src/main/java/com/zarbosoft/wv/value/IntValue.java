package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;

import java.nio.file.Path;

public class IntValue extends SimplePrimitiveConstValue implements Key, SimpleObject {
  public static final String errorDesc = "int";
  public final int value;

  public IntValue(int value) {
    this.value = value;
  }

  @Override
  public String toStringKey() {
    return "i$" + value;
  }

  @Override
  public Comparable pieces() {
    return value;
  }

  @Override
  public void serializeCacheReference(Context context, Path path, Writer writer) {
    writer.type("int").primitive(Integer.toString(value));
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new IntValue(value);
  }
}

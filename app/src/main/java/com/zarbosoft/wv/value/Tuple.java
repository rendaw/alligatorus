package com.zarbosoft.wv.value;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Evaluate;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.valuebase.ConstValue;
import com.zarbosoft.wv.valuebase.SimpleCacheConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.pcollections.TreePVector;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.sublist;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class Tuple extends SimpleCacheConstValue implements SimpleObject {
  public static final String errorDesc = "tuple";
  public final List<Value> values;
  private final boolean isConst;

  public Tuple(List<Value> values) {
    this.values = values;
    this.isConst = values.stream().allMatch(v -> v.isConst());
  }

  @Override
  public Value newCopy() {
    throw new Assertion();
  }

  @Override
  protected void serializeCacheInner(Context context, Path cacheBase, Writer writer) {
    if (!isConst) throw new Assertion();
    writer.type("tuple").arrayBegin();
    for (Value value : values) {
      ((ConstValue) value).serializeCacheReference(context, cacheBase, writer);
    }
    writer.arrayEnd();
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public String toSurveyJSON() {
    return String.format(
        "{\"type\":\"tuple\",\"fields\":[%s]}",
        values.stream().map(e -> e.toSurveyJSON()).collect(Collectors.joining(",")));
  }

  @Override
  public Value realize(Context context) {
    return this;
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    if (keys.isEmpty()) return this;
    Value out = null;
    TargetState pre = null;
    TargetState post = null;
    IntValue key = (IntValue) keys.get(0);
    for (int i = 0; i < values.size(); ++i) {
      Value value = values.get(i);
      if (i == key.value) {
        out = value;
      } else {
        TargetState dropped = gatherTargetState(context, Evaluate.instance.drop(context, value));
        if (out == null) {
          if (pre == null) pre = dropped;
          else pre.combine(context, dropped);
        } else {
          if (post == null) post = dropped;
          else post.combine(context, dropped);
        }
      }
    }
    return out.addPre(context, pre(), key.pre(), key.post(), pre)
        .addPost(context, post, post())
        .realizeAccess(context, sublist(keys, 1));
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    if (keys.isEmpty()) {
      List<Value> newValues = new ArrayList<>(values.size());
      for (Value value : values) {
        newValues.add(value.realizeAccessFork(context, ImmutableList.of()));
      }
      return new com.zarbosoft.wv.value.Tuple(newValues)
          .addPre(context, pre())
          .addPost(context, post());
    }
    IntValue key = (IntValue) keys.get(0);
    Value value =
        values
            .get(key.value)
            .addPre(context, pre(), key.pre(), key.post())
            .addPost(context, post())
            .realizeAccessFork(context, sublist(keys, 1));
    return value;
  }

  @Override
  public Value access(Context context, Key key) {
    if (!(key instanceof IntValue)) {
      context.suberrors.add(
          new TypeMismatch(TreePVector.empty(), IntValue.errorDesc, ((Value) key).errorDesc()));
      return err;
    }
    if (((IntValue) key).value < 0 || ((IntValue) key).value >= values.size()) {
      context.suberrors.add(new MissingField(TreePVector.empty(), key));
      return err;
    }
    return values.get(((IntValue) key).value);
  }

  @Override
  public Value drop(Context context) {
    TargetState targetState = null;
    for (Value value : values) {
      TargetState t = gatherTargetState(context, Evaluate.instance.drop(context, value));
      if (targetState == null) targetState = t;
      else targetState.combine(context, t);
    }
    return new VoidValue().addPre(context, targetState);
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    boolean failed = false;
    TargetState targetState = null;
    List<Value> newValues = new ArrayList<>(values.size());
    for (Value value : values) {
      Pair<TargetState, Value> res = value.bind(context);
      if (res == null) failed = true;
      if (res.first != null) {
        if (targetState == null) targetState = res.first;
        else targetState.combine(context, res.first);
      }
      newValues.add(res.second);
    }
    if (failed) return null;
    return new Pair<>(targetState, new com.zarbosoft.wv.value.Tuple(newValues));
  }

  @Override
  public boolean isConst() {
    return isConst;
  }
}

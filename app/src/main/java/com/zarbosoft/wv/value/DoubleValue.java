package com.zarbosoft.wv.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.valuebase.SimpleObject;
import com.zarbosoft.wv.valuebase.SimplePrimitiveConstValue;

import java.nio.file.Path;

public class DoubleValue extends SimplePrimitiveConstValue implements SimpleObject {
  public static final String errorDesc = "double";
  public final double value;

  public DoubleValue(double value) {
    this.value = value;
  }

  @Override
  public void serializeCacheReference(Context context, Path cacheBase, Writer writer) {
    writer.type("double").primitive(Double.toHexString(value));
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new DoubleValue(value);
  }
}

package com.zarbosoft.wv.value;

import com.google.common.io.BaseEncoding;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.valuebase.SimpleCacheConstValue;
import com.zarbosoft.wv.valuebase.SimpleConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.pcollections.PVector;

import java.nio.file.Path;

public class ByteArrayValue extends SimpleCacheConstValue
    implements SimpleConstValue, SimpleObject {
  public static final String errorDesc = "byte array";
  public static TypeCheck check =
      new TypeCheck() {
        @Override
        public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof ByteArrayValue)) {
            context.suberrors.add(new TypeMismatch(path, arg.errorDesc(), errorDesc));
            return false;
          }
          return true;
        }
      };
  public final byte[] bytes;

  public ByteArrayValue(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  protected void serializeCacheInner(Context context, Path cacheBase, Writer writer) {
    writer.primitive(BaseEncoding.base64Url().encode(bytes));
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value newCopy() {
    return new ByteArrayValue(bytes);
  }
}

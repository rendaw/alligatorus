package com.zarbosoft.wv.errorbase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.wv.Key;
import org.pcollections.PVector;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class SubpathExprSubError extends ExprSubError {
  public final PVector<Key> path;

  public SubpathExprSubError(PVector<Key> path) {
    this.path = path;
  }

  protected static String pathStr(PVector<Key> path) {
    return path.stream().map(v -> v.toString()).collect(Collectors.joining("/"));
  }

  public void serializePath(Writer writer) {
    writer.key("path").arrayBegin();
    for (Key e : path) {
      e.roughSerialize(writer);
    }
    writer.arrayEnd();
  }
}

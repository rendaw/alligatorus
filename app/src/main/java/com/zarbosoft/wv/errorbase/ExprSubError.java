package com.zarbosoft.wv.errorbase;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.LongValue;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.value.SymbolValue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class ExprSubError extends RuntimeException {
  public static void serializeInternalFields(Writer writer, Object o) {
    writer.type(o.getClass().getSimpleName()).recordBegin();
    for (Field field : o.getClass().getFields()) {
      Object value = uncheck(() -> field.get(o));
      if (value == null) continue;
      if (value instanceof List && ((List) value).isEmpty()) continue;
      writer.key(field.getName());
      serializeInternal(writer, value);
    }
    writer.recordEnd();
  }

  public static void serializeInternal(Writer writer, Object o) {
    if (o instanceof List) {
      writer.arrayBegin();
      for (Object e : (List) o) {
        serializeInternal(writer, e);
      }
      writer.arrayEnd();
    } else if (Arrays.stream(o.getClass().getMethods())
        .filter(m -> m.getName().equals("serializeSimple"))
        .findAny()
        .map(
            m ->
                uncheck(
                    () -> {
                      m.invoke(o, writer);
                      return true;
                    }))
        .isPresent()) {
      // nop
    } else if (o instanceof String) {
      writer.primitive(((String) o));
    } else if (o instanceof Integer) {
      writer.primitive(o.toString());
    } else if (o instanceof Long) {
      writer.primitive(o.toString());
    } else if (o instanceof Boolean) {
      writer.primitive(((Boolean) o) ? "true" : "false");
    } else if (o instanceof IntValue) {
      writer.primitive(Integer.toString(((IntValue) o).value));
    } else if (o instanceof LongValue) {
      writer.primitive(Long.toString(((LongValue) o).value));
    } else if (o instanceof StringValue) {
      writer.primitive(((StringValue) o).value);
    } else if (o instanceof SymbolValue) {
      ((SymbolValue) o).roughSerialize(writer);
    } else throw new Assertion("Unsupported error field type " + o.getClass().getName().toString());
  }

  public final void serialize(Writer writer) {
    serializeInternalFields(writer, this);
  }
}

package com.zarbosoft.wv.typecheck;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.Tuple;
import org.pcollections.PVector;

public class CheckTupleTree extends TypeCheck {
  public final TypeCheck inner;

  public CheckTupleTree(TypeCheck inner) {
    this.inner = inner;
  }

  @Override
  public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof Tuple) {
      boolean succeeded = false;
      for (int i = 0; i < ((Tuple) arg).values.size(); ++i) {
        succeeded &= check(context, path.plus(new IntValue(i)), ((Tuple) arg).values.get(i));
      }
      return succeeded;
    } else return inner.check(context, path, arg);
  }
}

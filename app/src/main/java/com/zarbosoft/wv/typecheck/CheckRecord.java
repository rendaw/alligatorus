package com.zarbosoft.wv.typecheck;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.KeyMap;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.value.Record;
import org.pcollections.PVector;

import static com.zarbosoft.rendaw.common.Common.iterable;

public class CheckRecord extends TypeCheck {
  private final KeyMap<TypeCheck> spec = new KeyMap<>();

  @Override
  public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
    if (!(arg instanceof Record)) {
      context.suberrors.add(new TypeMismatch(path, Record.errorDesc, arg.errorDesc()));
      return false;
    }
    boolean succeeded = true;
    for (Pair<Key, TypeCheck> e : iterable(spec.stream())) {
      Value got = ((Record) arg).data.get(e.first);
      if (got == null) {
        succeeded = false;
        context.suberrors.add(new MissingField(path, e.first));
        continue;
      }
      succeeded &= e.second.check(context, path.plus(e.first), ((Record) arg).data.get(e.first));
    }
    return succeeded;
  }

  public CheckRecord put(Key key, TypeCheck spec) {
    this.spec.put(key, spec);
    return this;
  }
}

package com.zarbosoft.wv.typecheck;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.value.Record;
import org.pcollections.PVector;

import java.util.Map;

import static com.zarbosoft.rendaw.common.Common.iterable;

public class CheckUniformRecord extends TypeCheck {
  private final TypeCheck value;

  public CheckUniformRecord(TypeCheck value) {
    this.value = value;
  }

  @Override
  public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
    if (!(arg instanceof Record)) {
      context.suberrors.add(new TypeMismatch(path, Record.errorDesc, arg.errorDesc()));
      return false;
    }
    boolean succeeded = true;
    for (Pair<Key, Value> e : iterable(((Record) arg).data.stream())) {
      succeeded &= value.check(context, path.plus(e.first), e.second);
    }
    return succeeded;
  }
}

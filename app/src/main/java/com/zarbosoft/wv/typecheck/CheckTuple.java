package com.zarbosoft.wv.typecheck;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.ExtraField;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.Tuple;
import org.pcollections.PVector;

import java.util.ArrayList;
import java.util.List;

public class CheckTuple extends TypeCheck {
  List<Pair<String, TypeCheck>> values = new ArrayList<>();

  public CheckTuple() {}

  public CheckTuple put(String name, TypeCheck element) {
    values.add(new Pair<>(name, element));
    return this;
  }

  @Override
  public boolean checkInternal(Context context, PVector<Key> path, Value arg) {
    if (!(arg instanceof Tuple)) {
      context.suberrors.add(new TypeMismatch(path, Tuple.errorDesc, arg.errorDesc()));
      return false;
    }
    Tuple arg1 = (Tuple) arg;
    boolean succeeded = true;
    if (arg1.values.size() < values.size()) {
      succeeded = false;
      for (int i = arg1.values.size(); i < values.size(); ++i)
        context.suberrors.add(new MissingField(path, new IntValue(i), values.get(i).first));
    } else if (arg1.values.size() > values.size()) {
      succeeded = false;
      for (int i = values.size(); i < arg1.values.size(); ++i)
        context.suberrors.add(new ExtraField(path.plus(new IntValue(i))));
    }
    for (int i = 0; i < Math.min(values.size(), ((Tuple) arg).values.size()); ++i) {
      succeeded &=
          values.get(i).second.check(context, path.plus(new IntValue(i)), arg1.values.get(i));
    }
    return succeeded;
  }
}

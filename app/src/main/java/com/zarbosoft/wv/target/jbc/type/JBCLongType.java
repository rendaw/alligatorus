package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.JBCPrimitiveType;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.LongValue;
import org.pcollections.PVector;

import static com.zarbosoft.wv.jvm.JCodeHelper.jcodeLoadLong;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static org.objectweb.asm.Opcodes.LLOAD;
import static org.objectweb.asm.Opcodes.LRETURN;
import static org.objectweb.asm.Opcodes.LSTORE;

public class JBCLongType extends JBCPrimitiveType {
  public JBCLongType() {
    super("jbc long", LSTORE, LLOAD, LRETURN);
  }

  @Override
  public String jbcDescriptor() {
    return "J";
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof LongValue) {
      return new JBCValue(this)
          .addPre(
              context,
              gatherTargetState(context, arg),
              new JBCTargetState().add(jcodeLoadLong(((LongValue) arg).value)));
    }
    return super.accept(context, path, arg);
  }

  @Override
  public Value newCopy() {
    return new JBCLongType();
  }
}

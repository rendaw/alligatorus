package com.zarbosoft.wv.target.jbc.value;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.valuebase.ValueBase;

import java.util.List;

public class JBCBoundValue extends ValueBase implements Value {
  public final Type type;

  public JBCBoundValue(Type type) {
    this.type = type;
  }

  @Override
  public boolean isConst() {
    return false;
  }

  @Override
  public String errorDesc() {
    return type.errorDesc();
  }

  @Override
  public Value access(Context context, Key key) {
    return type.valueAccess(context, key);
  }

  @Override
  public Value drop(Context context) {
    return type.boundValueDrop(this, context);
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    throw new AssertionError();
  }

  @Override
  public Value accessMethod(Context context, Key key) {
    return type.valueAccessMethod(context, key);
  }

  @Override
  public Value realize(Context context) {
    return type.boundValueFork(context, this);
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    throw new AssertionError();
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    return type.valueRealizeAccessFork(context,keys);
  }
}

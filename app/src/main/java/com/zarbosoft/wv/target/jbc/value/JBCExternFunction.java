package com.zarbosoft.wv.target.jbc.value;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.Callable;
import com.zarbosoft.wv.valuebase.SimpleCacheConstValue;
import com.zarbosoft.wv.valuebase.SimpleConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.objectweb.asm.tree.MethodInsnNode;
import org.pcollections.TreePVector;

import java.nio.file.Path;

import static com.zarbosoft.wv.target.jbc.CallableHelp.computeDesc;
import static com.zarbosoft.wv.target.jbc.CallableHelp.jbcCheckAndLoadInputs;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;

public class JBCExternFunction extends SimpleCacheConstValue
    implements Callable, SimpleObject, SimpleConstValue {
  private static final String errorDesc = "jbc extern function";
  protected final String jbcInternalClass;
  protected final String name;
  protected final Record spec;
  protected final String jbcDesc;
  private static final String cacheType = "jbc_extern_function";
  protected final String qualifiedClass;

  public JBCExternFunction(String qualifiedClass, String name, Record spec) {
    this(qualifiedClass, qualifiedClass.replace(".", "/"), name, spec, computeDesc(spec));
  }

  protected JBCExternFunction(
      String qualifiedClass, String jbcInternalClass, String name, Record spec, String jbcDesc) {
    this.jbcInternalClass = jbcInternalClass;
    this.name = name;
    this.spec = spec;
    this.jbcDesc = jbcDesc;
    this.qualifiedClass = qualifiedClass;
  }

  @Override
  public Value call(Context context, Value arg) {
    JBCTargetState code = new JBCTargetState();
    if (!jbcCheckAndLoadInputs(
        context, code.code, TreePVector.singleton(Intern.arg), spec.data.get(Intern.in), arg))
      return err;
    code.code.m().add(new MethodInsnNode(INVOKESTATIC, jbcInternalClass, name, jbcDesc, false));
    Value out = spec.data.get(Intern.out);
    Value res;
    if (out instanceof VoidValue) res = new VoidValue();
    else res = new JBCValue((Type) out);
    return res.addPre(context, code);
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  protected void serializeCacheInner(Context context, Path cacheBase, Writer writer) {
    writer
        .type(cacheType)
        .recordBegin()
        .key("class")
        .primitive(qualifiedClass)
        .key("name")
        .primitive("name")
        .key("spec");
    spec.serializeCacheReference(context, cacheBase, writer);
    writer.recordEnd();
  }

  @Override
  public Value newCopy() {
    return new JBCExternFunction(qualifiedClass, jbcInternalClass, name, spec, jbcDesc);
  }
}

package com.zarbosoft.wv.target.jbc.typebase;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.jvm.Code;
import com.zarbosoft.wv.target.jbc.value.JBCBoundValue;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.VoidValue;
import org.pcollections.PVector;

import java.util.List;

public interface Type extends Value {
  public static final String errorDesc = "jbc type";
  public static TypeCheck check =
      new TypeCheck() {
        @Override
        protected boolean checkInternal(Context context, PVector<Key> path, Value arg) {
          if (!(arg instanceof Type || arg instanceof VoidValue)) {
            context.suberrors.add(new TypeMismatch(path, Type.errorDesc, arg.errorDesc()));
            return false;
          }
          return true;
        }
      };

  Value boundValueFork(Context context, JBCBoundValue this_);

  Value valueAccess(Context context, Key key);

  Value valueDrop(Context context, JBCValue this_);

  Pair<TargetState, Value> freeValueBind(Context context, JBCValue this_);

  Value valueAccessMethod(Context context, Key key);

  String jbcDescriptor();

  Value accept(Context context, PVector<Key> path, Value arg);

  Value boundValueDrop(JBCBoundValue this_, Context context);

  Value valueRealizeAccess(Context context, List<Value> keys);

  Value valueRealizeAccessFork(Context context, List<Value> keys);

  int jbcRet();
}

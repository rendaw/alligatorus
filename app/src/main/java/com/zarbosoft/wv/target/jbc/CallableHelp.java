package com.zarbosoft.wv.target.jbc;

import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.ExtraField;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.jvm.CodeBuilder;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.value.VoidValue;
import org.pcollections.PVector;

import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.enumerate;
import static com.zarbosoft.rendaw.common.Common.zip;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class CallableHelp {
  public static String computeDesc(Record spec) {
    Value out = spec.data.get(Intern.out);
    return String.format(
        "(%s)%s",
        computeInDesc(spec.data.get(Intern.in)),
        out instanceof VoidValue ? "V" : ((Type) out).jbcDescriptor());
  }

  public static String computeInDesc(Value spec) {
    if (spec instanceof Tuple) {
      return ((Tuple) spec)
          .values.stream().map(v -> computeInDesc(v)).collect(Collectors.joining());
    } else if (spec instanceof VoidValue) {
      return "";
    } else if (spec instanceof Type) {
      return ((Type) spec).jbcDescriptor();
    } else throw new Assertion();
  }

  public static boolean jbcCheckAndLoadInputs(
      Context context, CodeBuilder code, PVector<Key> path, Value spec, Value arg) {
    if (spec instanceof Tuple) {
      if (!(arg instanceof Tuple)) {
        context.suberrors.add(new TypeMismatch(path, Tuple.errorDesc, arg.errorDesc()));
        return false;
      }
      code.add(((JBCTargetState) arg.pre()).code);
      Tuple in1 = (Tuple) spec;
      Tuple arg1 = (Tuple) arg;
      boolean failed = false;
      if (arg1.values.size() < in1.values.size()) {
        for (int i = arg1.values.size(); i < in1.values.size(); ++i) {
          failed |= context.suberrors.add(new MissingField(path, new IntValue(i)));
        }
      } else if (arg1.values.size() > in1.values.size()) {
        for (int i = in1.values.size(); i < arg1.values.size(); ++i)
          failed |= context.suberrors.add(new ExtraField(path.plus(new IntValue(i))));
      }
      failed =
          failed
              || enumerate(zip(in1.values.stream(), arg1.values.stream()))
                      .mapToInt(
                          p ->
                              jbcCheckAndLoadInputs(
                                      context,
                                      code,
                                      path.plus(new IntValue(p.first)),
                                      p.second.first,
                                      p.second.second)
                                  ? 1
                                  : 0)
                      .sum()
                  != 0;
      code.add(((JBCTargetState) arg.post()).code);
      return failed;
    } else if (spec instanceof VoidValue) {
      code.add(((JBCTargetState) arg.pre()).code);
      code.add(((JBCTargetState) arg.post()).code);
    } else if (spec instanceof Type) {
      Value res = ((Type) spec).accept(context, path, arg);
      if (res == err) return false;
      code.add(((JBCTargetState) gatherTargetState(context, res)).code);
    } else throw new Assertion();
    return true;
  }
}

package com.zarbosoft.wv.target.jbc.value;

import com.google.common.collect.ImmutableMap;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.wv.Binding;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Evaluate;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.AlreadyImplemented;
import com.zarbosoft.wv.error.ExprError;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.jvm.ASMClass;
import com.zarbosoft.wv.jvm.Code;
import com.zarbosoft.wv.jvm.CodeBuilder;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.Callable;
import com.zarbosoft.wv.valuebase.IntrinsicMethodCallable;
import com.zarbosoft.wv.valuebase.MethodCallable;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.objectweb.asm.tree.InsnNode;
import org.pcollections.TreePVector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;
import static org.objectweb.asm.Opcodes.RETURN;

public class JBCFunction extends JBCExternFunction implements Callable, SimpleObject {
  public static String errorDesc = "jbc function";
  public final State state;
  private static final Map<String, MethodCallable> methods =
      ImmutableMap.<String, MethodCallable>builder().put("implement", new DoImplement()).build();

  public JBCFunction(JBCClass class_, String name, Record spec) {
    super(String.format("%s.%s", class_.inner.package_, class_.inner.name), name, spec);
    this.state = new State();
  }

  private JBCFunction(
      String qualifiedClass,
      String jbcInternalClass,
      String name,
      Record spec,
      String jbcDesc,
      State state) {
    super(qualifiedClass, jbcInternalClass, name, spec, jbcDesc);
    this.state = state;
  }

  @Override
  public Value newCopy() {
    return new JBCFunction(qualifiedClass, jbcInternalClass, name, spec, jbcDesc, state);
  }

  @Override
  public boolean isConst() {
    return state.implementation != null;
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public MethodCallable accessMethod(String key) {
    return methods.get(key);
  }

  public boolean build(Context source, ASMClass class_) {
    Context context = Context.createSeparateModule(source, source.module, source.importLoopDetect);
    List<Object> initialIndexes = new ArrayList<>();
    context.scope.bind(
        Intern.arg, new Binding(jbcImplInitialInputs(initialIndexes, spec.data.get(Intern.in))));

    Value out = spec.data.get(Intern.out);
    Value res = Evaluate.instance.evaluate(context, state.implementation).realize(context);
    res =
        out instanceof VoidValue
            ? Evaluate.instance.drop(context, res)
            : ((Type) out).accept(context, TreePVector.empty(), res);
    CodeBuilder code = new CodeBuilder();
    if (res != err) {
      Code code1 = ((JBCTargetState) gatherTargetState(context, res)).code;
      if (code1 != null) code.add(code1);
    }
    if (!context.suberrors.isEmpty()) {
      context.errors.add(new ExprError(state.implementation, context.suberrors));
      return false;
    }

    if (out instanceof VoidValue) {
      code.m().add(new InsnNode(RETURN));
    } else {
      code.add(((Type) out).jbcRet());
    }

    class_.defineFunction(name, jbcDesc, code, initialIndexes);
    return true;
  }

  private Value jbcImplInitialInputs(List<Object> initialIndexes, Value spec) {
    if (spec instanceof Tuple) {
      return new Tuple(
          ((Tuple) spec)
              .values.stream()
                  .map(v -> jbcImplInitialInputs(initialIndexes, v))
                  .collect(Collectors.toList()));
    } else if (spec instanceof VoidValue) {
      return new VoidValue();
    } else if (spec instanceof Type) {
      JBCBoundValue v = new JBCBoundValue((Type) spec);
      initialIndexes.add(v);
      return v;
    } else throw new Assertion();
  }

  private static class DoImplement extends IntrinsicMethodCallable {
    private final TypeCheck check = TypeCheck.checkExpr;

    @Override
    public Value call(Context context, Value this_, Value arg) {
      if (!check.check(context, arg)) return err;
      JBCFunction this_1 = (JBCFunction) this_;
      if (this_1.state.implementation != null) {
        context.suberrors.add(new AlreadyImplemented());
        return err;
      }
      this_1.state.implementation = (Expr) arg;
      return new VoidValue().addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoImplement();
    }
  }

  public static class State {
    public Expr implementation;

    public State() {}
  }
}

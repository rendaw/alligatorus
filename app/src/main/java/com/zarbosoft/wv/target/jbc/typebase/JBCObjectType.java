package com.zarbosoft.wv.target.jbc.typebase;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.ASTORE;

public abstract class JBCObjectType extends JBCPrimitiveType {
  private final String internalName;

  public JBCObjectType(String errorDesc, String internalName) {
    super(errorDesc, ASTORE, ALOAD, ARETURN);
    this.internalName = internalName;
  }

  @Override
  public String jbcDescriptor() {
    return String.format("L%s;", internalName);
  }
}

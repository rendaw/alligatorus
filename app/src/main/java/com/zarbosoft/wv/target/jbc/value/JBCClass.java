package com.zarbosoft.wv.target.jbc.value;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.BaseEncoding;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.importing.ModuleSubContext;
import com.zarbosoft.wv.jvm.ASMClass;
import com.zarbosoft.wv.typecheck.CheckTuple;
import com.zarbosoft.wv.value.ByteArrayValue;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.IntrinsicMethodCallable;
import com.zarbosoft.wv.valuebase.MethodCallable;
import com.zarbosoft.wv.valuebase.SimpleCacheConstValue;
import com.zarbosoft.wv.valuebase.SimpleConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.target.jbc.Intrinsic.checkSignature;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class JBCClass extends SimpleCacheConstValue
    implements SimpleObject, ModuleSubContext.Incomplete, SimpleConstValue {
  public static final String errorDesc = "jbc class";
  public final Inner inner;
  private static final Map<String, MethodCallable> methods =
      ImmutableMap.<String, MethodCallable>builder()
          .put("function", new DoFunction())
          .put("bytes", new DoBytes())
          .build();

  public JBCClass(String package_, String name) {
    this.inner = new Inner(package_, name);
  }

  private JBCClass(Inner inner) {
    this.inner = inner;
  }

  public void build(Context context) {
    if (inner.bytes != null || inner.failed) return;
    ASMClass class_ = new ASMClass(inner.package_ + "." + inner.name);
    for (JBCFunction f : inner.pendingFunctions) {
      if (!f.build(context, class_)) inner.failed = true;
    }
    if (!inner.failed) this.inner.bytes = class_.render();
  }

  public String slashClassId() {
    return inner.package_.replace(".", "/") + "/" + inner.name;
  }

  @Override
  public MethodCallable accessMethod(String key) {
    return methods.get(key);
  }

  @Override
  protected void serializeCacheInner(Context context, Path cacheBase, Writer writer) {
    if (inner.failed) throw new Assertion();
    writer
        .type("jbcclass")
        .recordBegin()
        .key("package")
        .primitive(inner.package_)
        .key("name")
        .primitive(inner.name)
        .key("bytes")
        .primitive(BaseEncoding.base64().encode(inner.bytes))
        .recordEnd();
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public boolean complete(Context context) {
    build(context);
    return !inner.failed;
  }

  @Override
  public Value newCopy() {
    return new JBCClass(inner);
  }

  private static class DoFunction extends IntrinsicMethodCallable {
    private static final TypeCheck check =
        new CheckTuple().put("name", StringValue.check).put("signature", checkSignature);

    @Override
    public Value call(Context context, Value this_, Value arg) {
      JBCClass this_1 = (JBCClass) this_;
      if (!check.check(context, Intern.arg, arg)) {
        this_1.inner.failed = true;
        return err;
      }
      List<Value> values = ((Tuple) arg).values;
      JBCFunction out =
          new JBCFunction(
              (JBCClass) this_, ((StringValue) values.get(0)).value, (Record) values.get(1));
      this_1.inner.pendingFunctions.add(out);
      return out.addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoFunction();
    }
  }

  private static class DoBytes extends IntrinsicMethodCallable {
    @Override
    public Value call(Context context, Value this_, Value arg) {
      if (!VoidValue.check.check(context, Intern.arg, arg)) return err;
      JBCClass this_1 = (JBCClass) this_;
      if (!this_1.inner.failed && this_1.inner.bytes == null) {
        this_1.build(context);
      }
      if (this_1.inner.failed) return err;
      return new ByteArrayValue(this_1.inner.bytes)
          .addPre(context, gatherTargetState(context, arg));
    }

    @Override
    public Value newCopy() {
      return new DoBytes();
    }
  }

  public static class Inner {
    public final String package_;
    public final String name;
    final List<JBCFunction> pendingFunctions = new ArrayList<JBCFunction>();
    byte[] bytes = null;
    boolean failed = false;

    public Inner(String package_, String name) {
      this.package_ = package_;
      this.name = name;
    }
  }
}

package com.zarbosoft.wv.target.jbc;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.error.IncompatibleTargetStates;
import com.zarbosoft.wv.jvm.Code;
import com.zarbosoft.wv.jvm.CodeBuilder;

public class JBCTargetState implements TargetState {
  public final String errorDesc = "jbc";
  public final CodeBuilder code = new CodeBuilder();

  @Override
  public boolean combine(Context context, TargetState other) {
    if (other == null) return true;
    if (!(other instanceof JBCTargetState)) {
      context.suberrors.add(new IncompatibleTargetStates(errorDesc(), other.errorDesc()));
      return false;
    }
    code.add(((JBCTargetState) other).code);
    return true;
  }

  public final JBCTargetState add(Code code) {
    this.code.add(code);
    return this;
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public TargetState createMutable() {
    return new JBCTargetState();
  }
}

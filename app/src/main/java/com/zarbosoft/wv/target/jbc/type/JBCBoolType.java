package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.JBCPrimitiveType;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.BoolValue;
import org.pcollections.PVector;

import static com.zarbosoft.wv.jvm.JCodeHelper.jcodeLoadBool;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.ISTORE;

public class JBCBoolType extends JBCPrimitiveType {
  public JBCBoolType() {
    super("jbc byte", ISTORE, ILOAD, IRETURN);
  }

  @Override
  public String jbcDescriptor() {
    return "B";
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof BoolValue) {
      return new JBCValue(this)
          .addPre(
              context,
              gatherTargetState(context, arg),
              new JBCTargetState().add(jcodeLoadBool(((BoolValue) arg).value)));
    }
    return super.accept(context, path, arg);
  }

  @Override
  public Value newCopy() {
    return new JBCBoolType();
  }
}

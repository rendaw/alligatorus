package com.zarbosoft.wv.target.jbc;

import com.zarbosoft.wv.target.jbc.type.JBCBoolType;
import com.zarbosoft.wv.target.jbc.type.JBCByteType;
import com.zarbosoft.wv.target.jbc.type.JBCDoubleType;
import com.zarbosoft.wv.target.jbc.type.JBCFloatType;
import com.zarbosoft.wv.target.jbc.type.JBCIntType;
import com.zarbosoft.wv.target.jbc.type.JBCLongType;
import com.zarbosoft.wv.target.jbc.type.JBCStringType;
import com.zarbosoft.wv.target.jbc.typebase.Type;

public class Types {
  public static final Type t_int = new JBCIntType();
  public static final Type t_byte = new JBCByteType();
  public static final Type t_long = new JBCLongType();
  public static final Type t_float = new JBCFloatType();
  public static final Type t_double = new JBCDoubleType();
  public static final Type t_bool = new JBCBoolType();
  public static final Type t_string = new JBCStringType();

}

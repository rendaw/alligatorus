package com.zarbosoft.wv.target.jbc.typebase;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.MissingField;
import com.zarbosoft.wv.error.MissingMethod;
import com.zarbosoft.wv.error.TypeMismatch;
import com.zarbosoft.wv.jvm.Code;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.value.JBCBoundValue;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.VoidValue;
import com.zarbosoft.wv.valuebase.IntrinsicConstValue;
import com.zarbosoft.wv.valuebase.SimpleObject;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.util.List;

import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public abstract class JBCPrimitiveType extends IntrinsicConstValue implements SimpleObject, Type {
  public final String valueErrorDesc;
  private final int storeOpcode;
  private final int loadOpcode;
  private final int retOpcode;

  protected JBCPrimitiveType(String errorDesc, int storeOpcode, int loadOpcode, int retOpcode) {
    this.valueErrorDesc = errorDesc;
    this.storeOpcode = storeOpcode;
    this.loadOpcode = loadOpcode;
    this.retOpcode = retOpcode;
  }

  @Override
  public Value boundValueFork(Context context, JBCBoundValue this_) {
    JBCTargetState out = new JBCTargetState();
    out.code.addVarInsn(loadOpcode, this_);
    return new JBCValue(this).addPre(context, out);
  }

  @Override
  public Value boundValueDrop(JBCBoundValue this_, Context context) {
    return new VoidValue();
  }

  @Override
  public Value valueAccess(Context context, Key key) {
    context.suberrors.add(new MissingField(TreePVector.empty(), key));
    return null;
  }

  @Override
  public Value valueDrop(Context context, JBCValue this_) {
    return new VoidValue().addPre(context, gatherTargetState(context, this_));
  }

  @Override
  public Pair<TargetState, Value> freeValueBind(Context context, JBCValue this_) {
    JBCTargetState out = new JBCTargetState();
    out.add(((JBCTargetState) gatherTargetState(context, this_)).code);
    JBCBoundValue anchor = new JBCBoundValue(this);
    out.code.addVarInsn(storeOpcode, anchor);
    return new Pair<>(out, anchor);
  }

  @Override
  public Value valueAccessMethod(Context context, Key key) {
    context.suberrors.add(new MissingMethod(key));
    return err;
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (!(arg instanceof JBCValue) || ((JBCValue) arg).type.getClass() != this.getClass()) {
      context.suberrors.add(new TypeMismatch(path, arg.errorDesc(), errorDesc));
      return err;
    }
    return arg;
  }

  @Override
  public String errorDesc() {
    return errorDesc;
  }

  @Override
  public Value valueRealizeAccess(Context context, List<Value> keys) {
    throw new AssertionError();
  }

  @Override
  public Value valueRealizeAccessFork(Context context, List<Value> keys) {
    throw new AssertionError();
  }

  @Override
  public int jbcRet() {
    return retOpcode;
  }
}

package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.JBCPrimitiveType;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.IntValue;
import org.pcollections.PVector;

import static com.zarbosoft.wv.jvm.JCodeHelper.jcodeLoadInt;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.ISTORE;

public class JBCIntType extends JBCPrimitiveType {
  public JBCIntType() {
    super("jbc int", ISTORE, ILOAD, IRETURN);
  }

  @Override
  public String jbcDescriptor() {
    return "I";
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof IntValue) {
      return new JBCValue(this)
          .addPre(
              context,
              gatherTargetState(context, arg),
              new JBCTargetState().add(jcodeLoadInt(((IntValue) arg).value)));
    }
    return super.accept(context, path, arg);
  }

  @Override
  public Value newCopy() {
    return new JBCIntType();
  }
}

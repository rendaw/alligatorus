package com.zarbosoft.wv.target.jbc;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Intern;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.help.TypeCheck;
import com.zarbosoft.wv.target.jbc.type.JBCArrayType;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.target.jbc.value.JBCClass;
import com.zarbosoft.wv.target.jbc.value.JBCExternFunction;
import com.zarbosoft.wv.typecheck.CheckRecord;
import com.zarbosoft.wv.typecheck.CheckTuple;
import com.zarbosoft.wv.typecheck.CheckTupleTree;
import com.zarbosoft.wv.value.Record;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.value.Tuple;
import com.zarbosoft.wv.valuebase.IntrinsicCallable;
import org.pcollections.TreePVector;

import java.util.List;

import static com.zarbosoft.wv.target.jbc.Types.t_bool;
import static com.zarbosoft.wv.target.jbc.Types.t_byte;
import static com.zarbosoft.wv.target.jbc.Types.t_double;
import static com.zarbosoft.wv.target.jbc.Types.t_float;
import static com.zarbosoft.wv.target.jbc.Types.t_int;
import static com.zarbosoft.wv.target.jbc.Types.t_long;
import static com.zarbosoft.wv.target.jbc.Types.t_string;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class Intrinsic {
  public static final TypeCheck checkSignature =
      new CheckRecord().put(Intern.in, new CheckTupleTree(Type.check)).put(Intern.out, Type.check);
  public static final Record intrinsic =
      new Record.Builder()
          .put(
              Intern.type,
              new Record.Builder()
                  .put(Intern.int_, t_int)
                  .put(Intern.byte_, t_byte)
                  .put(Intern.long_, t_long)
                  .put(Intern.float_, t_float)
                  .put(Intern.double_, t_double)
                  .put(Intern.bool, t_bool)
                  .put(Intern.string, t_string)
                  .put(Intern.array, new DoArray())
                  .build())
          .put(Intern.class_, new DoClass())
          .put(Intern.externFunction, new DoExternFunction())
          .build();

  private static class DoArray extends IntrinsicCallable {
    @Override
    public Value call(Context context, Value arg) {
      if (!Type.check.check(context, TreePVector.singleton(Intern.arg), arg)) return err;
      return new JBCArrayType((Type) arg);
    }

    @Override
    public Value newCopy() {
      return new DoArray();
    }
  }

  private static class DoClass extends IntrinsicCallable {
    private final TypeCheck check =
        new CheckTuple().put("package", StringValue.check).put("name", StringValue.check);

    @Override
    public Value call(Context context, Value arg) {
      if (!check.check(context, TreePVector.singleton(Intern.arg), arg)) return err;
      List<Value> values = ((Tuple) arg).values;
      JBCClass jbcClass =
          new JBCClass(((StringValue) values.get(0)).value, ((StringValue) values.get(1)).value);
      context.module.incomplete.add(jbcClass);
      return jbcClass;
    }

    @Override
    public Value newCopy() {
      return new DoClass();
    }
  }

  private static class DoExternFunction extends IntrinsicCallable {
    private final TypeCheck check =
        new CheckTuple()
            .put("qualified class", StringValue.check)
            .put("name", StringValue.check)
            .put("signature", checkSignature);

    @Override
    public Value call(Context context, Value arg) {
      if (!check.check(context, TreePVector.singleton(Intern.arg), arg)) return err;
      List<Value> values = ((Tuple) arg).values;
      return new JBCExternFunction(
          ((StringValue) values.get(0)).value,
          ((StringValue) values.get(1)).value,
          ((Record) values.get(2)));
    }

    @Override
    public Value newCopy() {
      return new DoExternFunction();
    }
  }
}

package com.zarbosoft.wv.target.jbc.value;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.TargetState;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.typebase.Type;
import com.zarbosoft.wv.valuebase.ValueBase;

import java.util.List;

public class JBCValue extends ValueBase implements Value {
  public final Type type;

  public JBCValue(Type type) {
    this.type = type;
  }

  @Override
  public boolean isConst() {
    return false;
  }

  @Override
  public String errorDesc() {
    return type.errorDesc();
  }

  @Override
  public Value access(Context context, Key key) {
    return type.valueAccess(context, key);
  }

  @Override
  public Value drop(Context context) {
    return type.valueDrop(context, this);
  }

  @Override
  public Pair<TargetState, Value> bind(Context context) {
    return type.freeValueBind(context, this);
  }

  @Override
  public Value accessMethod(Context context, Key key) {
    return type.valueAccessMethod(context, key);
  }

  @Override
  public Value realize(Context context) {
    return this;
  }

  @Override
  public Value realizeAccess(Context context, List<Value> keys) {
    return type.valueRealizeAccess(context, keys);
  }

  @Override
  public Value realizeAccessFork(Context context, List<Value> keys) {
    throw new AssertionError();
  }
}

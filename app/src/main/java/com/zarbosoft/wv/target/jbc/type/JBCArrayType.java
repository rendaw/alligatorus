package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.typebase.JBCPrimitiveType;
import com.zarbosoft.wv.target.jbc.typebase.Type;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.ASTORE;

public class JBCArrayType extends JBCPrimitiveType {
  private final Type inner;

  public JBCArrayType(Type inner) {
    super(String.format("jbc array of %s", inner.errorDesc()), ASTORE, ALOAD, ARETURN);
    this.inner = inner;
  }

  @Override
  public String jbcDescriptor() {
    return "[" + inner.jbcDescriptor();
  }

  @Override
  public Value newCopy() {
    return new JBCArrayType(inner);
  }
}

package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.JBCPrimitiveType;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.FloatValue;
import org.pcollections.PVector;

import static com.zarbosoft.wv.jvm.JCodeHelper.jcodeLoadFloat;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;
import static org.objectweb.asm.Opcodes.FLOAD;
import static org.objectweb.asm.Opcodes.FRETURN;
import static org.objectweb.asm.Opcodes.FSTORE;

public class JBCFloatType extends JBCPrimitiveType {
  public JBCFloatType() {
    super("jbc float", FSTORE, FLOAD, FRETURN);
  }

  @Override
  public String jbcDescriptor() {
    return "F";
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof FloatValue) {
      return new JBCValue(this)
          .addPre(
              context,
              gatherTargetState(context, arg),
              new JBCTargetState().add(jcodeLoadFloat(((FloatValue) arg).value)));
    }
    return super.accept(context, path, arg);
  }

  @Override
  public Value newCopy() {
    return new JBCFloatType();
  }
}

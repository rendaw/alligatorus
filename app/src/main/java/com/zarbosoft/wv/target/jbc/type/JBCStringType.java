package com.zarbosoft.wv.target.jbc.type;

import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.target.jbc.JBCTargetState;
import com.zarbosoft.wv.target.jbc.typebase.JBCObjectType;
import com.zarbosoft.wv.target.jbc.value.JBCValue;
import com.zarbosoft.wv.value.StringValue;
import org.pcollections.PVector;

import static com.zarbosoft.wv.jvm.JCodeHelper.jcodeLoadString;
import static com.zarbosoft.wv.mortar.Intrinsic.gatherTargetState;

public class JBCStringType extends JBCObjectType {
  public JBCStringType() {
    super("jbc string", String.class.getName().replace(".","/"));
  }

  @Override
  public Value accept(Context context, PVector<Key> path, Value arg) {
    if (arg instanceof StringValue) {
      return new JBCValue(this)
          .addPre(
              context,
              gatherTargetState(context, arg),
              new JBCTargetState().add(jcodeLoadString(((StringValue) arg).value)));
    }
    return super.accept(context, path, arg);
  }

  @Override
  public Value newCopy() {
    return new JBCStringType();
  }
}

package com.zarbosoft.wv.importing;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Global;
import com.zarbosoft.wv.error.ModuleResultPartMissing;
import com.zarbosoft.wv.exception.MissingCache;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.valuebase.ConstValue;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class LocalModuleSubContext extends ModuleSubContext {
  public final ConcurrentHashMap<LocalModuleCacheAddr, CompletableFuture<Pair<ConstValue, Hash>>>
      localDir;
  public final ConcurrentHashMap<Path, Object> localDependencies = new ConcurrentHashMap<>();

  public LocalModuleSubContext(
      ModuleAddr address,
      ConcurrentHashMap<LocalModuleCacheAddr, CompletableFuture<Pair<ConstValue, Hash>>> localDir) {
    super(address);
    this.localDir = localDir;
  }

  public static ConstValue importCachedLocalPart(Context context, LocalModuleCacheAddr addr) {
    Path cacheDir = context.supercontext.workingDir.resolve(Global.ALLIGATORUS_LOCAL_CACHE);
    Path cacheFile = addr.computeCachePath(cacheDir);
    if (!Files.exists(cacheFile)) throw new MissingCache(addr);
    ConstValue value = DeserializeCache.deserialize(context, cacheFile);
    return value;
  }

  @Override
  public CompletableFuture<Pair<ConstValue, Hash>> importLocal(Context context, ModuleAddr addr0) {
    return importLocalPart(context, new LocalModuleCacheAddr((LocalModuleAddr) addr0, 0));
  }

  @Override
  public CacheAddr createCacheAddr(String importContextHash, int part) {
    return new LocalModuleCacheAddr((LocalModuleAddr) addr, part);
  }

  public CompletableFuture<Pair<ConstValue, Hash>> importLocalPart(
      com.zarbosoft.wv.Context context, LocalModuleCacheAddr addr) {
    return localDir.computeIfAbsent(
        addr,
        k -> {
          CompletableFuture<Pair<ConstValue, Hash>> out = new CompletableFuture<>();
          new Thread() {
            @Override
            public void run() {
              try {
                out.complete(new Pair<>(inner(k), null));
              } catch (Exception e) {
                out.completeExceptionally(e);
              }
            }

            public ConstValue inner(LocalModuleCacheAddr k) throws Exception {
              if (k.part != 0) {
                importLocalPart(context, new LocalModuleCacheAddr(k.addr, 0)).get();
              }
              Path cacheDir =
                  context.supercontext.workingDir.resolve(Global.ALLIGATORUS_LOCAL_CACHE);
              Path cacheFile = k.computeCachePath(cacheDir);
              do {
                if (!Files.exists(cacheFile)) break;
                ConstValue value = DeserializeCache.deserialize(context, cacheFile);
                if (value == null) break;
                return value;
              } while (false);
              if (k.part == 0) {
                LocalModuleSubContext module =
                    new LocalModuleSubContext(k.addr, new ConcurrentHashMap<>());
                Expr expr;
                try (InputStream is = Files.newInputStream(k.addr.path)) {
                  expr = Expr.deserialize(module).parse(is);
                }
                ConstValue result = Importing.importEvaluate(context, module, expr);
                result.serializeCache(context, cacheDir);
                return result;
              } else {
                throw new ModuleResultPartMissing(k);
              }
            }
          }.start();
          return out;
        });
  }
}

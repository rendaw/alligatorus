package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.wv.help.Serialize;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocalModuleAddr extends ModuleAddr {
  public static Node deserializeSimpleUntyped =
      new Operator<StackStore>(
        new Serialize.DeserializeRecordBuilder().addString("path").build()) {
        @Override
        protected StackStore process(StackStore store) {
          Map map = (Map) store.top();
          store = store.popStack();
          return store.pushStack(
            new LocalModuleAddr(Paths.get((String) map.get("path"))));
        }
      };
  public static Node deserializeSimple =
    Serialize.type(
      "remote",
      deserializeSimpleUntyped);
  /** Absolute, normalized */
  public final Path path;

  public LocalModuleAddr(Path path) {
    this.path = path;
  }

  @Override
  public LocalModuleAddr resolveLocal(String path) {
    return new LocalModuleAddr(this.path.resolve(path).normalize());
  }

  @Override
  public void serializeSimple(Writer writer) {
    writer.type("local").primitive(path.toString());
  }

  @Override
  public Comparable toSurveyAddrTuple() {
    return new Tuple("local", path.toString());
  }

  @Override
  public Comparable pieces() {
    return new Tuple("localModuleAddress", path.toString());
  }
  @Override
  public String toStringKey() {
    return Stream.of("lm", path.toString()).map(x -> x.toString()).collect(
      Collectors.joining("$"));
  }
}

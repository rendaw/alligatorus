package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.rendaw.common.Assertion;
import org.pcollections.PVector;

import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Collection;

public class IntrinsicCacheAddr extends CacheAddr {
  public final PVector<String> path;

  public IntrinsicCacheAddr(PVector<String> path) {
    this.path = path;
  }

  @Override
  public void serializeSimple(Writer writer) {
    writer.type("intrinsic").arrayBegin();
    for (String s : path) {
      writer.primitive(s);
    }
    writer.arrayEnd();
  }

  @Override
  public OutputStream createOutputStream(Path cacheBase) {
    throw new Assertion();
  }
}

package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.wv.Key;

public abstract class ModuleAddr implements Key {
  public abstract ModuleAddr resolveLocal(String path);

  public abstract void serializeSimple(Writer writer);

  Node deserializeSimple = new Union().add(RemoteModuleAddr.deserializeSimple);

  /**
   * @return Key component piece
   */
  public abstract Comparable toSurveyAddrTuple();
}

package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.wv.error.RemoteURIFragmentInvalid;
import com.zarbosoft.wv.error.RemoteURIQueryUnsupported;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.help.Serialize;

import java.net.IDN;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class RemoteModuleAddr extends ModuleAddr {
  public static Node deserializeSimpleUntyped =
      new Operator<StackStore>(
          new Serialize.DeserializeRecordBuilder().addString("uri").addString("hash").build()) {
        @Override
        protected StackStore process(StackStore store) {
          Map map = (Map) store.top();
          store = store.popStack();
          return store.pushStack(
              new RemoteModuleAddr((String) map.get("uri"), (String) map.get("hash")));
        }
      };
  public static Node deserializeSimple = Serialize.type("remote", deserializeSimpleUntyped);
  public final String pathless; // URI, no path
  public final String rootPath;
  public final Hash rootHash;
  public final Path path;

  public RemoteModuleAddr(String uri0, String hash) {
    URI uri = uncheck(() -> new URI(uri0));
    if (uri.getQuery() != null) throw new RemoteURIQueryUnsupported();
    if (uri.getFragment() != null) throw new RemoteURIFragmentInvalid();
    this.pathless =
        uncheck(() -> new URI(uri.getScheme(), uri.getAuthority(), null, null, null).toString());
    {
      String tempRoot = uri.getPath();
      if (tempRoot == null) tempRoot = "";
      if (tempRoot.isEmpty()) tempRoot = "/";
      if (tempRoot.endsWith("/")) tempRoot = tempRoot + "index.alligatorus";
      this.rootPath = tempRoot;
      this.path = Paths.get(tempRoot);
    }
    this.rootHash = Hash.deserialize(hash);
  }

  public RemoteModuleAddr(String pathless, String rootPath, Hash rootHash, Path path) {
    this.pathless = pathless;
    this.rootPath = rootPath;
    this.rootHash = rootHash;
    this.path = path;
  }

  @Override
  public RemoteModuleAddr resolveLocal(String path) {
    return new RemoteModuleAddr(pathless, rootPath, rootHash, this.path.resolve(path).normalize());
  }

  @Override
  public void serializeSimple(Writer writer) {
    writer.type("remote").recordBegin();
    writer.key("uri").primitive(toRequestUri());
    writer.key("hash").primitive(rootHash.serialize());
    writer.recordEnd();
  }

  @Override
  public Comparable toSurveyAddrTuple() {
    return new Tuple(
        "remote", String.format("%s/%s", pathless, rootPath), rootHash.toAscii(), path.toString());
  }

  public String toRequestUri() {
    return uncheck(
        () -> {
          URI temp = new URI(pathless);
          return new URI(temp.getScheme(), temp.getAuthority(), path.toString(), null).toString();
        });
  }

  @Override
  public String toStringKey() {
    return Stream.of(
            "rm", String.format("%s/%s", pathless, rootPath), rootHash.toAscii(), path.toString())
        .map(x -> x.toString())
        .collect(Collectors.joining("$"));
  }

  @Override
  public Comparable pieces() {
    return new Tuple("remoteModuleAddress", pathless, rootPath, rootHash.pieces(), path.toString());
  }

  public String slug() {
    return IDN.toASCII(toRequestUri().toString()) + "/" + rootHash.toSlug();
  }
}

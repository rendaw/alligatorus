package com.zarbosoft.wv.importing;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LTypeEvent;
import com.zarbosoft.luxem.read.Parse;
import com.zarbosoft.pidgoon.Grammar;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Color;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Global;
import com.zarbosoft.wv.error.UnknownIntrinsic;
import com.zarbosoft.wv.exception.MissingCache;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.value.IntValue;
import com.zarbosoft.wv.value.StringValue;
import com.zarbosoft.wv.valuebase.ConstValue;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class DeserializeCache {
  public static final Grammar grammar;
  private static Union deserializeNode;

  static {
    final Node intrinsicRefNode =
        new Sequence()
            .add(new Color("intrinsic", new MatchingEventTerminal(new LTypeEvent("intrinsic"))))
            .add(
                new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                  @Override
                  protected StackStore process(StackStore store) {
                    String id = ((LPrimitiveEvent) store.top()).value;
                    ConstValue found = Global.intrinsicLookup.get(id);
                    if (found == null) throw new UnknownIntrinsic(id);
                    return store.pushStack(found.realizeAccessFork(null, ImmutableList.of()));
                  }
                });
    final Node remoteRefNode =
        new Sequence()
            .add(new Color("remote", new MatchingEventTerminal(new LTypeEvent("remote"))))
            .add(RemoteModuleCacheAddr.deserializeSimpleUntyped);
    final Node localRefNode =
        new Sequence()
            .add(new Color("local", new MatchingEventTerminal(new LTypeEvent("local"))))
            .add(LocalModuleCacheAddr.deserializeSimpleUntyped);
    final Node intNode =
        new Sequence()
            .add(new Color("int", new MatchingEventTerminal(new LTypeEvent("int"))))
            .add(
                new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                  @Override
                  protected StackStore process(final StackStore store) {
                    return store.pushStack(
                        new IntValue(Integer.parseInt(((LPrimitiveEvent) store.top()).value)));
                  }
                });
    final Node stringNode =
        new Sequence()
            .add(new Color("string", new MatchingEventTerminal(new LTypeEvent("string"))))
            .add(
                new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                  @Override
                  protected StackStore process(final StackStore store) {
                    return store.pushStack(new StringValue(((LPrimitiveEvent) store.top()).value));
                  }
                });
    /*
    // TODO
    final Node customNode =
        new Sequence()
            .add(new Color("remote type", new MatchingEventTerminal(new LTypeEvent("remote"))))
            .add(new ValEqTerminal(LArrayOpenEvent.instance))
            .add(remotePartNode)
            .add(
                new Dynamic<StackStore>() {
                  @Override
                  protected Pair<StackStore, Node> generate(StackStore store) {
                    Value got = store.stackTop();
                    if (!(got instanceof Type)) throw new ReferencedRemotePartNotType();
                    return new Pair<>(store.popStack(), ((Type) got).generateGrammar());
                  }
                })
            .add(new ValEqTerminal(LArrayCloseEvent.instance));
     */
    deserializeNode =
        new Union()
            .add(intrinsicRefNode)
            .add(remoteRefNode)
            .add(localRefNode)
            .add(intNode)
            .add(stringNode)
            .add(Expr.moduleCacheNode)
    /*.add(customNode)*/
    ;
    grammar = new Grammar().add("root", deserializeNode);
  }

  /**
   * May throw UncheckedFileNotFoundException
   *
   * @param path
   * @return
   */
  public static ConstValue deserialize(Context context, final Path path) {
    return uncheck(
        () -> {
          try (final InputStream is = Files.newInputStream(path)) {
            return new Parse<ConstValue>()
                .grammar(grammar)
                .store(new StackStore(ImmutableMap.builder().put("context", context).build()))
                .parse(is);
          } catch (MissingCache e) {
            return null;
          }
        });
  }
}

package com.zarbosoft.wv.importing;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.hash.StructuredHash;
import com.zarbosoft.wv.hash.StructuredSHA256;
import com.zarbosoft.wv.valuebase.ConstValue;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class RemoteModuleSubContext extends ModuleSubContext {
  public final ConcurrentHashMap<RemoteModuleAddr, CompletableFuture<Pair<ConstValue, Hash>>> localDir;
  public final StructuredHash hash = new StructuredSHA256();
  public int partIds = 0;

  public RemoteModuleSubContext(
    ModuleAddr address, ConcurrentHashMap<RemoteModuleAddr, CompletableFuture<Pair<ConstValue, Hash>>> localDir
  ) {
    super(address);
    this.localDir = localDir;
  }

  @Override
  public CompletableFuture<Pair<ConstValue, Hash>> importLocal(Context context, ModuleAddr addr) {
    return localDir.computeIfAbsent(
      (RemoteModuleAddr)addr,
      k -> {
        CompletableFuture<Pair<ConstValue, Hash>> out = new CompletableFuture<>();
        new Thread() {
          @Override
          public void run() {
            try {
              out.complete(inner(k));
            } catch (Exception e) {
              out.completeExceptionally(e);
            }
          }

          public Pair<ConstValue, Hash> inner(RemoteModuleAddr k) throws Exception {
            RemoteModuleSubContext module = new RemoteModuleSubContext(addr, localDir);
            Expr expr =
              Importing.download(
                module.hash,
                ((RemoteModuleAddr) addr).toRequestUri(),
                is -> Expr.deserialize(module).parse(is));
            ConstValue out = Importing.importEvaluate(context, module, expr);
            return new Pair<>(out, module.hash);
          }
        }.start();
        return out;
        });
    }

  @Override
  public CacheAddr createCacheAddr(String importContextHash, int part) {
    return new RemoteModuleCacheAddr(importContextHash, (RemoteModuleAddr) addr, part);
  }
}

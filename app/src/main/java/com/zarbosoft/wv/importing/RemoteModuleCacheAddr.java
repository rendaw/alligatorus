package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LRecordCloseEvent;
import com.zarbosoft.luxem.events.LRecordOpenEvent;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.wv.Context;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class RemoteModuleCacheAddr extends CacheAddr {
  public static final Node deserializeSimpleUntyped =
      new Sequence()
          .add(new ValEqTerminal(LRecordOpenEvent.instance))
          .add(
              new Union()
                  .add(
                      new Sequence()
                          .add(new MatchingEventTerminal(new LKeyEvent("context")))
                          .add(
                              new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                                @Override
                                protected StackStore process(final StackStore store) {
                                  return store.pushStack(((LPrimitiveEvent) store.top()).value);
                                }
                              }))
                  .add(
                      new Sequence()
                          .add(new MatchingEventTerminal(new LKeyEvent("module")))
                          .add(RemoteModuleAddr.deserializeSimpleUntyped))
                  .add(
                      new Sequence()
                          .add(new MatchingEventTerminal(new LKeyEvent("part")))
                          .add(
                              new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                                @Override
                                protected StackStore process(final StackStore store) {
                                  return store.pushStack(
                                      Integer.parseInt(((LPrimitiveEvent) store.top()).value));
                                }
                              })))
          .add(new ValEqTerminal(LRecordCloseEvent.instance))
          .add(
              new Operator<StackStore>() {
                @Override
                protected StackStore process(StackStore store) {
                  int part = Integer.parseInt(store.stackTop());
                  store = store.popStack();
                  String importContextHash = store.stackTop();
                  store = store.popStack();
                  RemoteModuleAddr remoteModuleAddr = store.stackTop();
                  store = store.popStack();
                  Context context = store.getEnv("context");
                  return store.pushStack(
                      uncheck(
                          () ->
                              Importing.importCachedRemotePart(
                                  context,
                                  new RemoteModuleCacheAddr(
                                      importContextHash, remoteModuleAddr, part))));
                }
              });
  public final String importContextHash;
  public final RemoteModuleAddr addr;
  public final int part;

  public RemoteModuleCacheAddr(String importContextHash, RemoteModuleAddr addr, int part) {
    this.importContextHash = importContextHash;
    this.addr = addr;
    this.part = part;
  }

  @Override
  public void serializeSimple(Writer writer) {
    writer.type("remote");
    writer.recordBegin();
    writer.key("context").primitive(importContextHash);
    writer.key("module");
    addr.serializeSimple(writer);
    writer.key("part").primitive(Integer.toString(part));
    writer.recordEnd();
  }

  @Override
  public OutputStream createOutputStream(Path cacheBase) {
    return uncheck(() -> Files.newOutputStream(computeCachePath(cacheBase)));
  }

  public Path computeCachePath(Path cacheBase) {
    return cacheBase
        .resolve("modules")
        .resolve(importContextHash)
        .resolve(addr.slug())
        .resolve("output")
        .resolve(Integer.toString(part));
  }
}

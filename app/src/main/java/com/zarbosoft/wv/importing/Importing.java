package com.zarbosoft.wv.importing;

import com.google.common.collect.ImmutableMap;
import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Evaluate;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.error.EvaluationFailed;
import com.zarbosoft.wv.error.ImportLoop;
import com.zarbosoft.wv.error.ImportReplacementLoop;
import com.zarbosoft.wv.error.ModuleResultPartMissing;
import com.zarbosoft.wv.error.NotConst;
import com.zarbosoft.wv.error.RemoteModuleHashMismatch;
import com.zarbosoft.wv.exception.MissingCache;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.hash.StructuredHash;
import com.zarbosoft.wv.valuebase.ConstValue;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.pcollections.TreePVector;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static com.zarbosoft.wv.Global.remoteModuleCachePath;
import static com.zarbosoft.wv.Global.warn;
import static com.zarbosoft.wv.valuebase.ErrorValue.err;

public class Importing {
  public static final ConcurrentHashMap<
          RemoteModuleCacheAddr, CompletableFuture<Pair<ConstValue, Hash>>>
      remoteDir = new ConcurrentHashMap<>();
  public static final List<ImportCredentials> importCredentials = new ArrayList<>();
  private static final OkHttpClient httpClient = new OkHttpClient();

  static ConstValue importEvaluate(Context context, ModuleSubContext module, Expr expr) {
    Context context1 =
        Context.createSeparateModule(
            context, module, context.importLoopDetect.plus(module.addr));
    Value res = Evaluate.instance.evaluate(context1, expr).realize(context);
    for (ModuleSubContext.Incomplete incomplete : module.incomplete) {
      if (!incomplete.complete(context)) res = err;
    }
    if (res == err) throw new EvaluationFailed();
    if (!res.isConst()) throw new NotConst(TreePVector.empty(), res.errorDesc());
    return (ConstValue) res;
  }

  protected static <T> T download(
      /** Hash of the module being downloaded */
      StructuredHash hash, String uri, Function<InputStream, T> loader) {
    return uncheck(
        () -> {
          Request.Builder req = new Request.Builder().get().url(uri);
          for (ImportCredentials c : importCredentials) {
            if (!c.pattern.matcher(uri).find()) continue;
            req.addHeader(
                "Authorization", Credentials.basic(c.user, c.password, StandardCharsets.UTF_8));
            break;
          }
          T out;
          try (Response response = httpClient.newCall(req.build()).execute()) {
            Hash.HashStreamWrapper wrapper = hash.wrapStream(response.body().byteStream());
            out = loader.apply(wrapper.is());
            hash.put(wrapper.get());
          }
          return out;
        });
  }

  public static CompletableFuture<Pair<ConstValue, Hash>> importRemoteEntry(
      com.zarbosoft.wv.Context context, RemoteModuleAddr addr) {
    // Respect import replacements
    RemoteModuleAddr replacedAddr = addr;
    List<RemoteModuleAddr> replacements = new ArrayList<>();
    replacements.add(addr);
    while (true) {
      RemoteModuleAddr replacement;
      if ((replacement =
              context
                  .moduleImportOverride
                  .getOrDefault(context.module.addr, ImmutableMap.of())
                  .get(replacedAddr))
          != null) {
        replacedAddr = replacement;
      } else if ((replacement = context.generalImportOverride.get(replacedAddr)) != null) {
        replacedAddr = replacement;
      }
      if (replacedAddr.equals(addr)) {
        addr = replacedAddr;
        break;
      }
      if (replacements.contains(replacedAddr))
        throw new ImportReplacementLoop(replacements, replacedAddr);
      replacements.add(replacedAddr);
    }

    // Check for loops
    if (context.importLoopDetect.contains(addr)) {
      return CompletableFuture.failedFuture(new ImportLoop());
    }

    // Load
    return importRemotePart(context, new RemoteModuleCacheAddr(context.importContextHash, addr, 0));
  }

  public static ConstValue importCachedRemotePart(Context context, RemoteModuleCacheAddr addr) {
    Path modulePath = addr.computeCachePath(remoteModuleCachePath);
    if (!Files.exists(modulePath)) throw new MissingCache(addr);
    ConstValue value = DeserializeCache.deserialize(context, modulePath);
    return value;
  }

  public static CompletableFuture<Pair<ConstValue, Hash>> importRemotePart(
      com.zarbosoft.wv.Context context, RemoteModuleCacheAddr addr) {
    return remoteDir.computeIfAbsent(
        addr,
        k -> {
          CompletableFuture<Pair<ConstValue, Hash>> out = new CompletableFuture<>();
          new Thread() {
            @Override
            public void run() {
              try {
                out.complete(inner(k));
              } catch (Exception e) {
                out.completeExceptionally(e);
              }
            }

            public Pair<ConstValue, Hash> inner(RemoteModuleCacheAddr k) throws Exception {
              if (k.part != 0) {
                importRemotePart(context, new RemoteModuleCacheAddr(k.importContextHash, k.addr, 0))
                    .get();
              }
              Path modulePath = k.computeCachePath(remoteModuleCachePath);
              do {
                if (!Files.exists(modulePath)) break;
                ConstValue value =
                    DeserializeCache.deserialize(
                        context, modulePath.resolve(Integer.toString(k.part)));
                if (value == null) break;
                Hash hash;
                if (k.part == 0) {
                  Path hashPath = modulePath.resolve("hash");
                  try {
                    hash = Hash.deserialize(Files.readString(hashPath));
                  } catch (NoSuchFileException e) {
                    break;
                  } catch (Exception e) {
                    warn(e, "Failed to read cached module hash at %s", hashPath);
                    break;
                  }
                } else {
                  hash = null;
                }
                return new Pair<>(value, hash);
              } while (false);
              if (k.part == 0) {
                RemoteModuleSubContext module =
                    new RemoteModuleSubContext(k.addr, new ConcurrentHashMap<>());
                Expr expr =
                    download(
                        module.hash,
                        k.addr.toRequestUri(),
                        is -> Expr.deserialize(module).parse(is));
                ConstValue result = importEvaluate(context, module, expr);
                if (!module.hash.equals_(addr.addr.rootHash))
                  throw new RemoteModuleHashMismatch(addr, module.hash);
                result.serializeCache(context, modulePath);
                return new Pair<>(result, module.hash);
              } else {
                throw new ModuleResultPartMissing(k);
              }
            }
          }.start();
          return out;
        });
  }

  public static class ImportCredentials {
    public final Pattern pattern;
    public final String user;
    public final String password;

    public ImportCredentials(Pattern pattern, String user, String password) {
      this.pattern = pattern;
      this.user = user;
      this.password = password;
    }
  }
}

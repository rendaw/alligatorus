package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LRecordCloseEvent;
import com.zarbosoft.luxem.events.LRecordOpenEvent;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.pidgoon.nodes.Union;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.Key;

import java.io.OutputStream;
import java.net.IDN;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.zarbosoft.rendaw.common.Common.uncheck;
import static com.zarbosoft.wv.importing.LocalModuleSubContext.importCachedLocalPart;

public class LocalModuleCacheAddr extends CacheAddr {
  public static final Node deserializeSimpleUntyped =
      new Sequence()
          .add(new ValEqTerminal(LRecordOpenEvent.instance))
          .add(
              new Union()
                  .add(
                      new Sequence()
                          .add(new MatchingEventTerminal(new LKeyEvent("module")))
                          .add(LocalModuleAddr.deserializeSimple))
                  .add(
                      new Sequence()
                          .add(new MatchingEventTerminal(new LKeyEvent("part")))
                          .add(
                              new Operator<StackStore>(new ClassEqTerminal(LPrimitiveEvent.class)) {
                                @Override
                                protected StackStore process(final StackStore store) {
                                  return store.pushStack(
                                      Integer.parseInt(((LPrimitiveEvent) store.top()).value));
                                }
                              })))
          .add(new ValEqTerminal(LRecordCloseEvent.instance))
          .add(
              new Operator<StackStore>() {
                @Override
                protected StackStore process(StackStore store) {
                  int part = Integer.parseInt(store.stackTop());
                  store = store.popStack();
                  LocalModuleAddr localModuleAddr = store.stackTop();
                  store = store.popStack();
                  Context context = store.getEnv("context");
                  return store.pushStack(
                      uncheck(
                          () ->
                              importCachedLocalPart(context,
                                  new LocalModuleCacheAddr(localModuleAddr, part))));
                }
              });
  public final LocalModuleAddr addr;
  public final int part;

  public LocalModuleCacheAddr(LocalModuleAddr addr, int part) {
    this.addr = addr;
    this.part = part;
  }

  @Override
  public void serializeSimple(Writer writer) {
    writer.type("local");
    writer.recordBegin();
    writer.key("module");
    addr.serializeSimple(writer);
    writer.key("part").primitive(Integer.toString(part));
    writer.recordEnd();
  }

  @Override
  public OutputStream createOutputStream(Path cacheBase) {
    return uncheck(() -> Files.newOutputStream(computeCachePath(cacheBase)));
  }

  public Path computeCachePath(Path cacheBase) {
    return cacheBase
        .resolve("modules")
        .resolve(IDN.toASCII(cacheBase.relativize(addr.path).toString()))
        .resolve("output")
        .resolve(Integer.toString(part));
  }
}

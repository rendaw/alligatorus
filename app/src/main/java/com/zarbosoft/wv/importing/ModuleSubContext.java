package com.zarbosoft.wv.importing;

import com.zarbosoft.rendaw.common.Pair;
import com.zarbosoft.wv.Context;
import com.zarbosoft.wv.SourceMap;
import com.zarbosoft.wv.Value;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.jvm.JCodeHelper;
import com.zarbosoft.wv.valuebase.ConstValue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public abstract class ModuleSubContext {
  public final ModuleAddr addr;
  public int symbolIds = 0;
  public Class constants;
  public final List<Value> constantsArray = new ArrayList<>();
  public JCodeHelper.JVMStaticField jvmModule;
  public SourceMap sourceMap = new SourceMap();
  public List<Incomplete> incomplete = new ArrayList<>();

  public ModuleSubContext(ModuleAddr addr) {
    this.addr = addr;
  }

  public abstract CompletableFuture<Pair<ConstValue, Hash>> importLocal(
      Context context, ModuleAddr addr);

  public abstract CacheAddr createCacheAddr(String importContextHash, int part);

  public interface Incomplete {
    public boolean complete(Context context);
  }
}

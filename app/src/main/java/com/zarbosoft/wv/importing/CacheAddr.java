package com.zarbosoft.wv.importing;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.importing.RemoteModuleAddr;

import java.io.OutputStream;
import java.nio.file.Path;

public abstract class CacheAddr {
  public abstract void serializeSimple(Writer writer);

  public abstract OutputStream createOutputStream(Path cacheBase);
}

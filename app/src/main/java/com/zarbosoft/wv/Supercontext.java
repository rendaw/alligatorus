package com.zarbosoft.wv;

import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.rendaw.common.Triple;
import com.zarbosoft.wv.hash.Hash;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.Instant;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.zarbosoft.rendaw.common.Common.uncheck;

/**
 * Represents a "compilation execution" If the compiler is run once against one or more root
 * modules, this is effectively global But if run multiple times in same jvm process this is not
 * effectively global therefore it's not just static values
 */
public class Supercontext {
  public final Path workingDir;
  public final ConcurrentHashMap<Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>>
      localDependencies;
  public final Set<Path> localDirty;
  private final Connection dbThreadConnection;
  private final ScheduledThreadPoolExecutor dbThread = new ScheduledThreadPoolExecutor(1);

  public Supercontext(
      Path workingDir,
      ConcurrentHashMap<Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>> localDependencies,
      Set<Path> localDirty) {
    this.workingDir = workingDir;
    this.localDependencies = localDependencies;
    this.localDirty = localDirty;
    this.dbThreadConnection =
        uncheck(() -> DriverManager.getConnection(String.format("jdbc:sqlite:%s", metaDbPath())));
    uncheck(() -> this.dbThreadConnection.setAutoCommit(false));
  }

  public void close() {
    uncheck(
        () -> {
          dbThread.shutdown();
          dbThread.awaitTermination(300, TimeUnit.SECONDS);
          dbThreadConnection.close();
        });
  }

  public Path metaDbPath() {
    return workingDir.resolve(Global.ALLIGATORUS_META);
  }

  public void doInDb(Common.UncheckedConsumer<Connection> consumer) {
    dbThread.execute(() -> uncheck(() -> consumer.run(dbThreadConnection)));
  }
}

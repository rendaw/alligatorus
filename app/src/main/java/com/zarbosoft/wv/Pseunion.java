package com.zarbosoft.wv;

public class Pseunion<T extends Enum<T>, V> {
  private final T tag;
  private final V value;

  protected Pseunion(T tag, V value) {
    this.tag = tag;
    this.value = value;
  }

  public T code() {
    return tag;
  }

  protected  <U extends V> U get() {
    return (U) value;
  }
}

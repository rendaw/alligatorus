package com.zarbosoft.wv;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Node;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.wv.help.Serialize;
import com.zarbosoft.wv.importing.ModuleAddr;
import com.zarbosoft.wv.importing.RemoteModuleAddr;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExprAddr implements Key {
  public final ModuleAddr module;
  public final int line;
  public static final Node deserializeSimple =
      new Operator<StackStore>(
          new Serialize.DeserializeRecordBuilder()
              .add("module", RemoteModuleAddr.deserializeSimple)
              .addInt("line")
              .build()) {
        @Override
        protected StackStore process(StackStore store) {
          Map map = store.stackTop();
          store = store.popStack();
          return store.pushStack(
              new ExprAddr((ModuleAddr) map.get("module"), (Integer) map.get("line")));
        }
      };

  public ExprAddr(ModuleAddr module, int line) {
    this.module = module;
    this.line = line;
  }

  public void serializeSimple(Writer writer) {
    writer.recordBegin();
    writer.key("module");
    module.serializeSimple(writer);
    writer.key("line").primitive(Integer.toString(line));
    writer.recordEnd();
  }

  @Override
  public String toString() {
    return String.format("<%s %s>", module, line);
  }

  @Override
  public Comparable pieces() {
    return new Tuple(module.pieces(), line);
  }

  public String toSurveyAddr() {
    return Key.recursiveFlattenKeyPieces(Stream.of(module.toSurveyAddrTuple(), line))
        .map(x -> x.toString())
        .collect(Collectors.joining("$"));
  }

  @Override
  public String toStringKey() {
    return Key.recursiveFlattenKeyPieces(Stream.of("expr", module.toSurveyAddrTuple(), line))
        .map(x -> x.toString())
        .collect(Collectors.joining("$"));
  }
}

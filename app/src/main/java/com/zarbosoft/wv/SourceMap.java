package com.zarbosoft.wv;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.expr.Expr;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public class SourceMap {
  public final List<ExprAddr> sourceMap;
  public final Map<ExprAddr, Integer> reverseSourceMap;

  public SourceMap() {
    this.sourceMap = new ArrayList<>();
    this.reverseSourceMap = new HashMap<>();
  }

  public void write(String path) {
    uncheck(
        () -> {
          try (OutputStream s = Files.newOutputStream(Paths.get(path))) {
            Writer writer = new Writer(s, (byte) ' ', 4);
            for (ExprAddr exprAddr : sourceMap) {
              writer.arrayBegin();
              exprAddr.module.serializeSimple(writer);
              writer.primitive(Integer.toString(exprAddr.line)).arrayEnd();
            }
          }
        });
  }

  public int line(Expr expr) {
    Integer found = reverseSourceMap.get(expr.id);
    if (found == null) {
      found = sourceMap.size();
      sourceMap.add(expr.id);
      reverseSourceMap.put(expr.id, found);
    }
    return found;
  }
}

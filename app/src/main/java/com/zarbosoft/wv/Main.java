package com.zarbosoft.wv;

import com.google.common.collect.ImmutableList;
import com.zarbosoft.luxem.events.LArrayCloseEvent;
import com.zarbosoft.luxem.events.LArrayOpenEvent;
import com.zarbosoft.luxem.events.LKeyEvent;
import com.zarbosoft.luxem.events.LPrimitiveEvent;
import com.zarbosoft.luxem.events.LRecordCloseEvent;
import com.zarbosoft.luxem.events.LRecordOpenEvent;
import com.zarbosoft.luxem.read.Parse;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.pidgoon.Grammar;
import com.zarbosoft.pidgoon.events.nodes.ClassEqTerminal;
import com.zarbosoft.pidgoon.events.nodes.MatchingEventTerminal;
import com.zarbosoft.pidgoon.events.nodes.ValEqTerminal;
import com.zarbosoft.pidgoon.events.stores.StackStore;
import com.zarbosoft.pidgoon.nodes.Color;
import com.zarbosoft.pidgoon.nodes.Operator;
import com.zarbosoft.pidgoon.nodes.Repeat;
import com.zarbosoft.pidgoon.nodes.Sequence;
import com.zarbosoft.rendaw.common.ChainComparator;
import com.zarbosoft.rendaw.common.Common;
import com.zarbosoft.rendaw.common.Triple;
import com.zarbosoft.wv.error.ExprError;
import com.zarbosoft.wv.expr.Expr;
import com.zarbosoft.wv.hash.Hash;
import com.zarbosoft.wv.importing.Importing;
import com.zarbosoft.wv.importing.LocalModuleAddr;
import com.zarbosoft.wv.importing.LocalModuleSubContext;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.pidgoon.events.stores.StackStore.prepVarStack;
import static com.zarbosoft.rendaw.common.Common.iterable;
import static com.zarbosoft.rendaw.common.Common.uncheck;
import static com.zarbosoft.rendaw.common.Common.workingDir;

public class Main {
  public static final Grammar credientialGrammar =
      new Grammar()
          .add(
              "root",
              new Repeat(
                  new Operator<StackStore>(
                      new Sequence()
                          .add(new ValEqTerminal(LArrayOpenEvent.instance))
                          .add(
                              new Color(
                                  "pattern",
                                  new Operator<StackStore>(
                                      new MatchingEventTerminal(new LPrimitiveEvent())) {
                                    @Override
                                    protected StackStore process(StackStore store) {
                                      return store.pushStack(
                                          Pattern.compile(((LPrimitiveEvent) store.top()).value));
                                    }
                                  }))
                          .add(
                              new Color(
                                  "user",
                                  new Operator<StackStore>(
                                      new MatchingEventTerminal(new LPrimitiveEvent())) {
                                    @Override
                                    protected StackStore process(StackStore store) {
                                      return store.pushStack(((LPrimitiveEvent) store.top()).value);
                                    }
                                  }))
                          .add(
                              new Color(
                                  "password",
                                  new Operator<StackStore>(
                                      new MatchingEventTerminal(new LPrimitiveEvent())) {
                                    @Override
                                    protected StackStore process(StackStore store) {
                                      return store.pushStack(((LPrimitiveEvent) store.top()).value);
                                    }
                                  }))
                          .add(new ValEqTerminal(LArrayCloseEvent.instance))) {
                    @Override
                    protected StackStore process(StackStore store) {
                      String password = store.stackTop();
                      store = store.popStack();
                      String user = store.stackTop();
                      store = store.popStack();
                      Pattern pattern = store.stackTop();
                      store = store.popStack();
                      return store.pushStack(
                          new Importing.ImportCredentials(pattern, user, password));
                    }
                  }));
  private static final String hashesSerialVersion = "h0.0.0";
  private static final Grammar hashesGrammar =
      new Grammar()
          .add(
              "root",
              new Sequence()
                  .add(new MatchingEventTerminal(new LPrimitiveEvent(hashesSerialVersion)))
                  .add(new ValEqTerminal(LRecordOpenEvent.instance))
                  .add(prepVarStack)
                  .add(
                      new Operator<StackStore>(
                          new Repeat(
                              new Operator<StackStore>(
                                  new Sequence()
                                      .add(
                                          new Operator<StackStore>(
                                              new ClassEqTerminal(LKeyEvent.class)) {
                                            @Override
                                            protected StackStore process(StackStore store) {
                                              return store.pushStack(
                                                  Paths.get(((LPrimitiveEvent) store.top()).value));
                                            }
                                          })
                                      .add(new ValEqTerminal(LArrayOpenEvent.instance))
                                      .add(
                                          new Operator<StackStore>(
                                              new ClassEqTerminal(LPrimitiveEvent.class)) {
                                            @Override
                                            protected StackStore process(StackStore store) {
                                              return store.pushStack(
                                                  Instant.parse(
                                                      ((LPrimitiveEvent) store.top()).value));
                                            }
                                          })
                                      .add(
                                          new Operator<StackStore>(
                                              new ClassEqTerminal(LPrimitiveEvent.class)) {
                                            @Override
                                            protected StackStore process(StackStore store) {
                                              return store.pushStack(
                                                  Hash.deserialize(
                                                      ((LPrimitiveEvent) store.top()).value));
                                            }
                                          })
                                      .add(
                                          new Sequence()
                                              .add(new ValEqTerminal(LArrayOpenEvent.instance))
                                              .add(
                                                  new Operator<StackStore>() {
                                                    @Override
                                                    protected StackStore process(StackStore store) {
                                                      return store.pushStack(0);
                                                    }
                                                  })
                                              .add(
                                                  new Repeat(
                                                      new Operator<StackStore>(
                                                          new ClassEqTerminal(
                                                              LPrimitiveEvent.class)) {
                                                        @Override
                                                        protected StackStore process(
                                                            StackStore store) {
                                                          return store.stackSingleElement(
                                                              Paths.get(
                                                                  ((LPrimitiveEvent) store.top())
                                                                      .value));
                                                        }
                                                      }))
                                              .add(new ValEqTerminal(LArrayCloseEvent.instance)))
                                      .add(
                                          new Operator<StackStore>() {
                                            @Override
                                            protected StackStore process(StackStore store) {
                                              List<Path> dependencies = new ArrayList<>();
                                              store = store.popVarSingleList(dependencies);
                                              Hash hash = store.stackTop();
                                              store = store.popStack();
                                              Instant modified = store.stackTop();
                                              store = store.popStack();
                                              return store.pushStack(
                                                  new Triple<>(modified, hash, dependencies));
                                            }
                                          })
                                      .add(new ValEqTerminal(LArrayCloseEvent.instance))) {
                                @Override
                                protected StackStore process(StackStore store) {
                                  return store.stackVarDoubleElement();
                                }
                              })) {
                        @Override
                        protected StackStore process(StackStore store) {
                          ConcurrentHashMap<
                                  Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>>
                              out = new ConcurrentHashMap<>();
                          return store.popVarMap(store, out).pushStack(out);
                        }
                      })
                  .add(new ValEqTerminal(LRecordCloseEvent.instance)));

  public static boolean compile(
      boolean hashVerifyLocalContext, LocalModuleSubContext module, Expr root) {
    return uncheck(
        () -> {
          final Path working = Common.workingDir();
          Files.createDirectories(working.resolve(Global.ALLIGATORUS));

          // Refresh fetch creds
          uncheck(
              () -> {
                Path at = working;
                while (at != null) {
                  for (Path p :
                      iterable(
                          Files.list(at)
                              .filter(
                                  p -> p.toString().endsWith(Global.ALLIGATORUS_CREDENTIALS)))) {
                    if (Files.isExecutable(p)) {
                      Process proc = new ProcessBuilder(p.toString()).inheritIO().start();
                      Importing.importCredentials.addAll(
                          new Parse<Importing.ImportCredentials>()
                              .grammar(credientialGrammar)
                              .parseByElement(proc.getInputStream())
                              .collect(Collectors.toList()));
                    } else {
                      try (InputStream s = Files.newInputStream(p)) {
                        Importing.importCredentials.addAll(
                            new Parse<Importing.ImportCredentials>()
                                .grammar(credientialGrammar)
                                .parseByElement(s)
                                .collect(Collectors.toList()));
                      }
                    }
                  }
                  at = at.getParent();
                }
              });

          // Prep cache, check dirty files
          Path localCacheSummaryFile = working.resolve(Global.ALLIGATORUS_LOCAL_CACHE_STATE);
          uncheck(() -> Files.createDirectories(localCacheSummaryFile.getParent()));
          ConcurrentHashMap<Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>>
              localDependencies;
          try (InputStream is = Files.newInputStream(localCacheSummaryFile)) {
            localDependencies =
                new Parse<
                        ConcurrentHashMap<
                            Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>>>()
                    .grammar(hashesGrammar)
                    .parse(is);
          } catch (NoSuchFileException e) {
            localDependencies = new ConcurrentHashMap<>();
          }
          Set<Path> localDirty =
              localDependencies
                  .entrySet()
                  .parallelStream()
                  .filter(
                      t -> {
                        if (hashVerifyLocalContext) {
                          Hash hash;
                          try {
                            hash = t.getValue().second.hashOtherFile(t.getKey());
                          } catch (Common.UncheckedFileNotFoundException e) {
                            return true;
                          }
                          return !t.getValue().second.equals(hash);
                        } else {
                          try {
                            return !uncheck(() -> Files.getLastModifiedTime(t.getKey()).toInstant())
                                .equals(t.getValue().first);
                          } catch (Common.UncheckedFileNotFoundException e) {
                            return true;
                          }
                        }
                      })
                  .flatMap(t -> Stream.concat(Stream.of(t.getKey()), t.getValue().third.stream()))
                  .collect(Collectors.toSet());
          for (Path path : localDirty) {
            localDependencies.remove(path);
            Common.deleteTree(path);
          }
          for (Triple<Instant, Hash, ConcurrentLinkedDeque<Path>> e : localDependencies.values()) {
            e.third.removeIf(localDirty::contains);
          }

          // Compile
          ConcurrentLinkedQueue<ExprError> errors = new ConcurrentLinkedQueue<>();
          Context context =
              Context.createStart(
                  new Supercontext(working, localDependencies, localDirty), module, errors);
          Path metaPath = context.supercontext.metaDbPath();
          uncheck(() -> Files.deleteIfExists(metaPath));
          context.supercontext.doInDb(
              metaDb -> {
                try (Statement s = metaDb.createStatement()) {
                  s.execute(
                      "create table survey(id integer primary key autoincrement, addr text, data json)");
                  s.execute("create index survey_addr on survey(addr)");
                }
                metaDb.setAutoCommit(true);
                metaDb.setAutoCommit(false);
              });

          try {
            Evaluate evaluate = Evaluate.instance;
            //noinspection ResultOfMethodCallIgnored
            evaluate.drop(context, evaluate.evaluate(context, root));
            try (OutputStream os = Files.newOutputStream(localCacheSummaryFile)) {
              Writer writer = new Writer(os);
              writer.primitive(hashesSerialVersion);
              writer.recordBegin();
              for (Map.Entry<Path, Triple<Instant, Hash, ConcurrentLinkedDeque<Path>>> e :
                  localDependencies.entrySet()) {
                writer
                    .key(e.getKey().toString())
                    .arrayBegin()
                    .primitive(e.getValue().first.toString())
                    .primitive(e.getValue().second.serialize())
                    .arrayBegin();
                for (Path d : e.getValue().third) {
                  writer.primitive(d.toString());
                }
                writer.arrayEnd();
                writer.arrayEnd();
              }
              writer.recordEnd();
            }

            if (!errors.isEmpty()) {
              final Writer writer = new Writer(System.err, (byte) ' ', 4);
              for (ExprError e :
                  iterable(
                      errors.stream()
                          .sorted(
                              new ChainComparator<ExprError>()
                                  .lesserFirst(f -> f.source.id.pieces())
                                  .build()))) {
                e.serialize(writer);
              }
              System.err.flush();
              return false;
            }

            return true;
          } finally {
            context.supercontext.close();
          }
        });
  }

  public static void main(String[] args) {
    new TestDoc1();
  }

  public static class TestDoc1 {
    int id = 0;
    LocalModuleAddr module = new LocalModuleAddr(Paths.get("abc.alligatorus"));

    private ExprAddr i() {
      return new ExprAddr(module, id++);
    }

    private Expr s(String text) {
      return Expr.stringLiteral(i(), text);
    }

    private Expr ia(String... keys) {
      Expr out = Expr.intrinsic(i());
      for (String key : keys) {
        out = Expr.access(i(), out, s(key));
      }
      return out;
    }

    private Expr sa(String... keys) {
      Expr out = Expr.scope(i());
      for (String key : keys) {
        out = Expr.access(i(), out, s(key));
      }
      return out;
    }

    private Expr t(Expr... elements) {
      return Expr.tuple(i(), Arrays.asList(elements));
    }

    private RecordBuilder r() {
      return new RecordBuilder();
    }

    public TestDoc1() {
      Expr doc =
          Expr.sequence(
              i(),
              ImmutableList.of(
                  Expr.bind(
                      i(),
                      s("class"),
                      Expr.call(i(), ia("jbc", "class"), t(s("a.b.c"), s("World")))),
                  Expr.methodCall(
                      i(),
                      Expr.methodCall(
                          i(),
                          sa("class"),
                          s("function"),
                          Expr.tuple(
                              i(),
                              ImmutableList.of(
                                  s("main"),
                                  r().put(
                                          s("in"),
                                          Expr.call(
                                              i(),
                                              ia("jbc", "type", "array"),
                                              ia("jbc", "type", "string")))
                                      .put(s("out"), Expr.voidLiteral(i()))
                                      .build()))),
                      s("implement"),
                      Expr.stage(
                          i(),
                          Expr.call(
                              i(),
                              Expr.call(
                                  i(),
                                  ia("jbc", "externFunction"),
                                  t(
                                      s("java.lang.System"),
                                      s("exit"),
                                      r().put(s("in"), ia("jbc", "type", "int"))
                                          .put(s("out"), Expr.voidLiteral(i()))
                                          .build())),
                              Expr.intLiteral(i(), "13")))),
                  Expr.call(
                      i(),
                      ia("mortar", "write"),
                      t(
                          s("a/b/c/World.class"),
                          Expr.methodCall(i(), sa("class"), s("bytes"), Expr.voidLiteral(i()))))));
      uncheck(
          () -> {
            try (OutputStream os = Files.newOutputStream(workingDir().resolve("test1.at"))) {
              Writer writer = new Writer(os, (byte) ' ', 4);
              doc.serializeModule(writer);
              os.write('\n');
            }
          });
      if (!compile(true, new LocalModuleSubContext(module, new ConcurrentHashMap<>()), doc))
        System.exit(1);
    }

    public class RecordBuilder {
      List<Expr> fields = new ArrayList<>();

      public RecordBuilder put(Expr key, Expr val) {
        fields.add(Expr.recordPair(i(), key, val));
        return this;
      }

      public Expr build() {
        return Expr.record(i(), fields);
      }
    }
  }
}

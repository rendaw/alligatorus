package com.zarbosoft.wv.error;

import com.google.common.base.Throwables;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.errorbase.ExprSubError;

import java.util.List;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.iterable;

public class OtherError extends ExprSubError {
  public final List<String> traceback;

  public OtherError(Exception e) {
    this.traceback = Throwables.getStackTraceAsString(e).lines().collect(Collectors.toList());
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.PVector;

public class NotConst extends SubpathExprSubError {
  public final String errorDesc;

  public NotConst(PVector<Key> path, String errorDesc) {
    super(path);
    this.errorDesc = errorDesc;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.importing.CacheAddr;
import com.zarbosoft.wv.errorbase.ExprSubError;
import com.zarbosoft.wv.hash.Hash;

public class RemoteModuleHashMismatch extends ExprSubError {
  public final CacheAddr addr;
  public final Hash hash;

  public RemoteModuleHashMismatch(CacheAddr addr, Hash hash) {
    this.addr=addr;
    this.hash =hash;
  }
}

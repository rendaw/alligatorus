package com.zarbosoft.wv.error;

public class UnknownIntrinsic extends RuntimeException {
  public final String id;

  public UnknownIntrinsic(String id) {
    this.id = id;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.errorbase.ExprSubError;

import java.util.List;

public class MultiErrorException extends RuntimeException{
  public final List<ExprSubError> inner;

  public MultiErrorException(List<ExprSubError> inner) {
    this.inner = inner;
  }
}

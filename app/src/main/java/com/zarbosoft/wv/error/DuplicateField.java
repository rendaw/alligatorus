package com.zarbosoft.wv.error;

import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.PVector;

public class DuplicateField extends SubpathExprSubError {
  public final Key key;

  public DuplicateField(PVector<Key> path, Key key) {
    super(path);
    this.key = key;
  }
}

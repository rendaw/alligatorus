package com.zarbosoft.wv.error;

import com.zarbosoft.wv.importing.CacheAddr;
import com.zarbosoft.wv.errorbase.ExprSubError;

public class ModuleResultPartMissing extends ExprSubError {
  public final CacheAddr addr;

  public ModuleResultPartMissing(CacheAddr addr) {
    this.addr = addr;
  }
}

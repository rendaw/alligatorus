package com.zarbosoft.wv.error;

import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.PVector;

public class ExtraField extends SubpathExprSubError {
  public ExtraField(PVector<Key> path) {
    super(path);
  }
}

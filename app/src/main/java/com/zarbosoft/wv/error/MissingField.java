package com.zarbosoft.wv.error;

import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.PVector;

public class MissingField extends SubpathExprSubError {
  public final Key key;
  public final String hint;

  public MissingField(PVector<Key> path, Key key) {
    this(path,key,null);
  }
  public MissingField(PVector<Key> path, Key key, String hint) {
    super(path);
    this.key = key;
    this.hint = hint;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.errorbase.ExprSubError;

public class IncompatibleTargetStates extends ExprSubError {
  public final String targetState1;
  public final String targetState2;

  public IncompatibleTargetStates(String targetState1, String targetState2) {
    this.targetState1 = targetState1;
    this.targetState2 = targetState2;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.errorbase.ExprSubError;

public class RecordPairMustBeInRecord extends ExprSubError {
  public static final RecordPairMustBeInRecord instance = new RecordPairMustBeInRecord();
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.TreePVector;

public class LowerUnreachable extends SubpathExprSubError {
  public final int depth;

  public LowerUnreachable(int depth) {
    super(TreePVector.empty());
    this.depth = depth;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.errorbase.SubpathExprSubError;
import org.pcollections.PVector;

public class TypeMismatch extends SubpathExprSubError {
  public final String expected;
  public final String got;

  public TypeMismatch(PVector<Key> path, String expected, String got) {
    super(path);
    this.expected = expected;
    this.got = got;
  }
}

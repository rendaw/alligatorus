package com.zarbosoft.wv.error;

import com.zarbosoft.wv.errorbase.ExprSubError;

public class UnsupportedHashType extends ExprSubError {
  public final String hash;

  public UnsupportedHashType(String hash) {
    this.hash = hash;
  }
}

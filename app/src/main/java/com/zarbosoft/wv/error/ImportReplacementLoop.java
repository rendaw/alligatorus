package com.zarbosoft.wv.error;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.errorbase.ExprSubError;
import com.zarbosoft.wv.importing.RemoteModuleAddr;

import java.util.List;

public class ImportReplacementLoop extends ExprSubError {
  public final List<RemoteModuleAddr> replacements;
  private final RemoteModuleAddr dupe;

  public ImportReplacementLoop(List<RemoteModuleAddr> replacements, RemoteModuleAddr dupe) {
    this.replacements = replacements;
    this.dupe = dupe;
  }
}

package com.zarbosoft.wv.error;

import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.errorbase.ExprSubError;
import com.zarbosoft.wv.expr.Expr;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ExprError {
  public final Expr source;
  public final ConcurrentLinkedQueue<ExprSubError> subErrors;

  public ExprError(Expr source, ConcurrentLinkedQueue<ExprSubError> subErrors) {
    this.source = source;
    this.subErrors = subErrors;
  }

  public ExprError(Expr source, ExprSubError error) {
    this.source = source;
    this.subErrors = new ConcurrentLinkedQueue<>();
    this.subErrors.add(error);
  }

  public void serialize(Writer writer) {
    writer.recordBegin();
    writer.key("id").recordBegin();
    writer.key("addr");
    source.id.module.serializeSimple(writer);
    writer.key("expr").primitive(Integer.toString(source.id.line));
    writer.recordEnd();
    writer.key("errors").arrayBegin();
    for (ExprSubError e : subErrors) {
      e.serialize(writer);
    }
    writer.arrayEnd();
    writer.recordEnd();
  }
}

package com.zarbosoft.wv;

import com.zarbosoft.appdirsj.AppDirs;
import com.zarbosoft.wv.importing.IntrinsicCacheAddr;
import com.zarbosoft.wv.valuebase.ConstValue;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class Global {
  public static final String version = "0";
  public static final AppDirs appDirs =
      new AppDirs().set_appname("alligatorus").set_appauthor("zarbosoft");
  public static final Path remoteModuleCachePath = appDirs.user_cache_dir(true).resolve(version);
  public static final Map<IntrinsicCacheAddr, ConstValue> intrinsicLookup = new HashMap<>();

  // Relative to working directory
  public static final String ALLIGATORUS = ".alligatorus";
  /** Root for storing cached local modules */
  public static final String ALLIGATORUS_LOCAL_CACHE = ALLIGATORUS + "/local_cache";
  /** File, cached value metadata for detecting changes */
  public static final String ALLIGATORUS_LOCAL_CACHE_STATE = ALLIGATORUS + "/local_cache_state";

  public static final String ALLIGATORUS_META = ALLIGATORUS + "/meta.sqlite";
  public static final String ALLIGATORUS_CREDENTIALS = ".alligatorus-credentials";

  public static void warn(String pattern, Object... v) {
    System.err.format(pattern, v);
  }

  public static void warn(Exception e, String pattern, Object... v) {
    System.out.println(ZonedDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    e.printStackTrace(System.err);
    System.err.format(pattern, v);
  }
}

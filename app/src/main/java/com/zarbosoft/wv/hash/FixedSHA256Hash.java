package com.zarbosoft.wv.hash;

import com.google.common.io.BaseEncoding;

public class FixedSHA256Hash extends SHA256Hash {
  private final byte[] data;

  public FixedSHA256Hash(String hash) {
    data = BaseEncoding.base64Url().decode(hash);
  }

  public FixedSHA256Hash(byte[] hash) {
    data = hash;
  }

  @Override
  public byte[] get() {
    return data;
  }
}

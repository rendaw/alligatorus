package com.zarbosoft.wv.hash;

import com.google.common.hash.Hashing;

import java.io.InputStream;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Arrays;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class SHA256Hash implements Hash {
  public static final String type = "SHA256";

  public static SHA256Hash hashFile(Path path) {
    return new FixedSHA256Hash(
        uncheck(
            () ->
                com.google.common.io.Files.asByteSource(path.toFile())
                    .hash(Hashing.sha256())
                    .asBytes()));
  }

  @Override
  public SHA256Hash hashOtherFile(Path path) {
    return hashFile(path);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof SHA256Hash)) return false;
    return Arrays.equals(get(), ((SHA256Hash) obj).get());
  }

  MessageDigest instanceDigest() {
    return uncheck(() -> MessageDigest.getInstance("SHA-256"));
  }

  @Override
  public String type() {
    return type;
  }

  @Override
  public HashStreamWrapper wrapStream(InputStream byteStream) {
    return new HashStreamWrapper() {
      final MessageDigest digest = instanceDigest();

      @Override
      public InputStream is() {
        return new DigestInputStream(byteStream, digest);
      }

      @Override
      public byte[] get() {
        return digest.digest();
      }
    };
  }
}

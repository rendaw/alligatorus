package com.zarbosoft.wv.hash;

import com.google.common.io.BaseEncoding;
import com.zarbosoft.luxem.write.Writer;
import com.zarbosoft.wv.Key;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;

import static com.zarbosoft.rendaw.common.Common.uncheck;

public final class StructuredSHA256 extends SHA256Hash implements StructuredHash {
  private final MessageDigest digest;
  private final Writer writer;

  public StructuredSHA256() {
    digest = uncheck(() -> MessageDigest.getInstance("SHA-256"));
    writer =
        new Writer(
            new OutputStream() {
              @Override
              public void write(int i) throws IOException {
                digest.update((byte) i);
              }

              @Override
              public void write(byte[] b, int off, int len) throws IOException {
                digest.update(b, off, len);
              }
            });
  }

  @Override
  public byte[] get() {
    return digest.digest();
  }

  @Override
  public void put(Comparable value) {
    Key.roughSerializeInternal(writer,value);
  }

  @Override
  public void put(String value) {
    writer.primitive(value);
  }

  @Override
  public void put(int value) {
    writer.primitive(Integer.toString(value));
  }

  @Override
  public void put(long value) {
    writer.primitive(Long.toString(value));
  }

  @Override
  public void put(byte[] value) {
    writer.primitive(BaseEncoding.base64Url().encode(value));
  }
}

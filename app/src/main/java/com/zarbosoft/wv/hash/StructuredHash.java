package com.zarbosoft.wv.hash;

import com.zarbosoft.wv.Key;

import java.io.InputStream;

public interface StructuredHash extends Hash{
  public void put(Comparable value);
  public void put(String value);
  public void put(int value);
  public void put(long value);
  public void put(byte[] value);
}

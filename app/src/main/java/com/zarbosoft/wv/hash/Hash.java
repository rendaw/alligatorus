package com.zarbosoft.wv.hash;

import com.google.common.io.BaseEncoding;
import com.zarbosoft.wv.Key;
import com.zarbosoft.wv.error.InvalidHashPartCount;
import com.zarbosoft.wv.error.UnsupportedHashType;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.Arrays;

public interface Hash {
  public static Hash deserialize(String hash) {
    String[] parts = hash.split(":");
    if (parts.length != 2) throw new InvalidHashPartCount(parts);
    switch (parts[0]) {
      case "SHA256":
        return new FixedSHA256Hash(parts[1]);
      default:
        throw new UnsupportedHashType(parts[0]);
    }
  }

  public interface HashStreamWrapper {
    InputStream is();

    byte[] get();
  }

  HashStreamWrapper wrapStream(InputStream byteStream);

  /**
   * Filesystem safe string rep, one way (but non colliding)
   *
   * @return
   */
  default String toSlug() {
    return serialize();
  }

  public abstract String type();

  /**
   * Get hash bytes
   *
   * @return
   */
  public byte[] get();

  default String toAscii() {
    return BaseEncoding.base64Url().encode(get());
  }

  /**
   * Deserializable human readable string rep
   *
   * @return
   */
  default String serialize() {
    return type() + "." + toAscii();
  }

  /**
   * May throw UncheckedFileNotFoundException
   *
   * @param path
   * @return
   */
  public abstract Hash hashOtherFile(Path path);

  default boolean equals_(Object other) {
    if (!Hash.class.isAssignableFrom(other.getClass())) return false;
    return type().equals(((Hash) other).type()) && Arrays.equals(get(), ((Hash) other).get());
  }

  default Comparable pieces() {
    return new Key.Tuple("hash", type(), toAscii());
  }
}

package com.zarbosoft.wv.exception;

import com.zarbosoft.wv.importing.CacheAddr;
import com.zarbosoft.wv.importing.RemoteModuleCacheAddr;

public class MissingCache extends RuntimeException {

  public final CacheAddr addr;

  public MissingCache(CacheAddr addr) {
    this.addr = addr;
  }
}

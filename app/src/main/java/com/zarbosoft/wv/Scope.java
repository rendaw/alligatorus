package com.zarbosoft.wv;

import com.zarbosoft.wv.help.KeyMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Scope {
  public final Scope parent;
  public final KeyMap<Binding> keyed = new KeyMap<>();
  public final List<Binding> unkeyed = new ArrayList<>();
  public List<Key> parentRemoved = new ArrayList<>();

  public Scope(Scope parent) {
    this.parent = parent;
  }

  public void bind(Binding binding) {
    binding.scope = this;
    unkeyed.add(binding);
  }

  public Binding bind(Key key, Binding binding) {
    binding.scope = this;
    Binding old = keyed.remove(key);
    keyed.put(key, binding);
    return old;
  }

  public Binding unbind(Key key) {
    {
      Binding found = keyed.remove(key);
      if (found != null) return found;
    }
    Scope at = this.parent;
    while (at != null) {
      Binding found = at.keyed.get(key);
      if (found != null) {
        parentRemoved.add(key);
        return found;
      }
      at = at.parent;
    }
    return null;
  }

  public <B extends Binding> B get(Key key) {
    Scope at = this;
    while (at != null) {
      Binding found = at.keyed.get(key);
      if (found != null) return (B) found;
      at = at.parent;
    }
    return null;
  }
}
